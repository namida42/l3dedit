unit IndexedGraphic;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  GR32,
  Misc,
  L3DGraphics,
  Classes, SysUtils;

type

  { TIndexedGraphic }

  TIndexedGraphic = class
    private
      fBaseName: String;
      fSourceWidth: Integer;
      fSourceCompressed: Boolean;

      fImage: TBitmap32;
      fLoadedIndex: Integer;
      fIndex: Integer;

      function GetImage: TBitmap32;
    protected
      procedure LoadFile(aFilename: String);
      property SourceWidth: Integer read fSourceWidth;
      property SourceCompressed: Boolean read fSourceCompressed;
    public
      constructor Create(aBaseName: String; aSourceWidth: Integer; aSourceCompressed: Boolean = false);
      destructor Destroy; override;

      function ChangeIndex(aNewIndex: Integer; aNewWidth: Integer = -1): Boolean;

      property Image: TBitmap32 read GetImage;
      property Index: Integer read fIndex;
  end;

implementation

{ TIndexedGraphic }

function TIndexedGraphic.GetImage: TBitmap32;
var
  Filename: String;
begin
  if (fLoadedIndex <> fIndex) or (fSourceWidth <> fImage.Width) then
  begin
    fLoadedIndex := fIndex;
    Filename := AppPath + 'GFX' + PathDelim + fBaseName + '.' + LeadZeroStr(Index, 3);
    if FileExists(Filename) then
      LoadFile(Filename)
    else
      fImage.Clear($00000000);
  end;

  Result := fImage;
end;

procedure TIndexedGraphic.LoadFile(aFilename: String);
begin
  LoadL3DGraphic(fImage, aFilename, 0, fSourceWidth, -1, fSourceCompressed);
end;

constructor TIndexedGraphic.Create(aBaseName: String; aSourceWidth: Integer; aSourceCompressed: Boolean = false);
begin
  inherited Create;
  fBaseName := aBaseName;
  fImage := TBitmap32.Create;
  fImage.SetSize(64, 64);
  fImage.Clear($00000000);
  fLoadedIndex := -1;
  fSourceWidth := aSourceWidth;
  fSourceCompressed := aSourceCompressed;
end;

destructor TIndexedGraphic.Destroy;
begin
  fImage.Free;
  inherited Destroy;
end;

function TIndexedGraphic.ChangeIndex(aNewIndex: Integer; aNewWidth: Integer = -1): Boolean;
begin
  if aNewWidth < 1 then aNewWidth := fSourceWidth;
  Result := (aNewIndex <> fIndex) or (aNewWidth <> fSourceWidth);
  fIndex := aNewIndex;
  fSourceWidth := aNewWidth;
end;

end.

