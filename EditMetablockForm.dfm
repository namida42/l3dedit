object FEditMetablock: TFEditMetablock
  Left = 153
  Top = 331
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Edit Metablock'
  ClientHeight = 385
  ClientWidth = 564
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    564
    385)
  PixelsPerInch = 96
  TextHeight = 13
  object lblZ31: TLabel
    Left = 4
    Top = 104
    Width = 26
    Height = 13
    Caption = 'Z=31'
    Color = clBtnFace
    ParentColor = False
  end
  object lblZ32: TLabel
    Left = 336
    Top = 104
    Width = 20
    Height = 13
    Caption = 'X=0'
    Color = clBtnFace
    ParentColor = False
  end
  object lblFaceShading: TLabel
    Left = 343
    Top = 168
    Width = 64
    Height = 13
    Caption = 'Face Shading'
    Color = clBtnFace
    ParentColor = False
  end
  object lblBlockProperties: TLabel
    Left = 208
    Top = 217
    Width = 76
    Height = 13
    Caption = 'Block Properties'
  end
  object lblFaceProperties: TLabel
    Left = 343
    Top = 217
    Width = 75
    Height = 13
    Caption = 'Face Properties'
  end
  object lblUnknown1: TLabel
    Left = 208
    Top = 168
    Width = 53
    Height = 13
    Caption = 'Unknown 1'
    FocusControl = ebUnknown1
  end
  object lblUnknown2: TLabel
    Left = 208
    Top = 193
    Width = 53
    Height = 13
    Caption = 'Unknown 2'
    FocusControl = ebUnknown2
  end
  object imgTopFace: TImage32
    Tag = 4
    Left = 185
    Top = 7
    Width = 72
    Height = 72
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Color = clFuchsia
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 0
    OnClick = imgFaceClick
  end
  object imgX31Face: TImage32
    Tag = 2
    Left = 112
    Top = 80
    Width = 72
    Height = 72
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Color = clFuchsia
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 1
    OnClick = imgFaceClick
  end
  object imgZ31Face: TImage32
    Left = 39
    Top = 80
    Width = 72
    Height = 72
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Color = clFuchsia
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 2
    OnClick = imgFaceClick
  end
  object imgZ0Face: TImage32
    Tag = 1
    Left = 185
    Top = 80
    Width = 72
    Height = 72
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Color = clFuchsia
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 3
    OnClick = imgFaceClick
  end
  object imgX0Face: TImage32
    Tag = 3
    Left = 258
    Top = 80
    Width = 72
    Height = 72
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Color = clFuchsia
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 4
    OnClick = imgFaceClick
  end
  object imgBottomFace: TImage32
    Tag = 5
    Left = 39
    Top = 153
    Width = 72
    Height = 72
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Color = clFuchsia
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 5
    OnClick = imgFaceClick
  end
  object sbTextures: TScrollBox
    Left = 477
    Top = 0
    Width = 86
    Height = 385
    HorzScrollBar.Visible = False
    VertScrollBar.Increment = 64
    VertScrollBar.Range = 6464
    VertScrollBar.Tracking = True
    Anchors = [akTop, akRight, akBottom]
    AutoScroll = False
    Color = clFuchsia
    ParentColor = False
    TabOrder = 6
    object imgTextureSet: TImage32
      Left = 0
      Top = 0
      Width = 64
      Height = 6464
      Bitmap.ResamplerClassName = 'TNearestResampler'
      BitmapAlign = baTopLeft
      Scale = 1.000000000000000000
      ScaleMode = smNormal
      TabOrder = 0
      OnMouseDown = imgTextureSetMouseDown
    end
  end
  object btnCancel: TButton
    Left = 400
    Top = 355
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object btnOK: TButton
    Left = 320
    Top = 355
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    TabOrder = 8
    OnClick = btnOKClick
  end
  object btnClearBlock: TButton
    Left = 200
    Top = 355
    Width = 96
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Clear Block'
    TabOrder = 9
    OnClick = btnClearBlockClick
  end
  object tbShading: TTrackBar
    Left = 336
    Top = 184
    Width = 105
    Height = 25
    Max = 8
    TabOrder = 10
    OnChange = tbShadingChange
  end
  object cgBlockProperties: TCheckListBox
    Left = 200
    Top = 236
    Width = 130
    Height = 113
    BorderStyle = bsNone
    Color = clBtnFace
    ItemHeight = 13
    Items.Strings = (
      'Double-Sided'
      'Steel'
      'Liquid (Top)'
      '????'
      'Non-Solid (Lemmings)'
      'Anti-Splat (Top)'
      'Slippery (Top)'
      'Non-Solid (Camera)')
    TabOrder = 11
    OnClick = cgBlockPropertiesItemClick
  end
  object cgFaceProperties: TCheckListBox
    Left = 336
    Top = 236
    Width = 130
    Height = 113
    BorderStyle = bsNone
    Color = clBtnFace
    ItemHeight = 13
    Items.Strings = (
      '????'
      '????'
      '????'
      'Alt. Textures'
      '5-Panel Anim.'
      '4-Panel Anim.'
      'Spec. Anim'
      'Transparent')
    TabOrder = 12
    OnClick = cgFacePropertiesItemClick
  end
  object ebUnknown1: TEdit
    Left = 280
    Top = 165
    Width = 50
    Height = 21
    MaxLength = 4
    TabOrder = 13
    Text = '0000'
    OnChange = ebUnknown1Change
    OnKeyPress = VerifyHexKey
  end
  object ebUnknown2: TEdit
    Left = 280
    Top = 190
    Width = 50
    Height = 21
    MaxLength = 2
    TabOrder = 14
    Text = '00'
    OnChange = ebUnknown2Change
    OnKeyPress = VerifyHexKey
  end
end
