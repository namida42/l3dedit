unit EditMetablockForm;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  L3DLevel, Misc, Classes, SysUtils, Forms, Controls, Graphics,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, GR32, GR32_Image, GR32_Layers,
  StrUtils, Vcl.CheckLst;

type

  { TFEditMetablock }

  TFEditMetablock = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    btnClearBlock: TButton;
    imgTextureSet: TImage32;
    imgTopFace: TImage32;
    imgBottomFace: TImage32;
    imgX31Face: TImage32;
    imgX0Face: TImage32;
    imgZ31Face: TImage32;
    imgZ0Face: TImage32;
    lblFaceShading: TLabel;
    lblZ31: TLabel;
    lblZ32: TLabel;
    sbTextures: TScrollBox;
    tbShading: TTrackBar;
    cgBlockProperties: TCheckListBox;
    cgFaceProperties: TCheckListBox;
    lblBlockProperties: TLabel;
    lblFaceProperties: TLabel;
    lblUnknown1: TLabel;
    ebUnknown1: TEdit;
    lblUnknown2: TLabel;
    ebUnknown2: TEdit;
    procedure btnClearBlockClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure cgBlockPropertiesItemClick(Sender: TObject);
    procedure cgFacePropertiesItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure imgFaceClick(Sender: TObject);
    procedure imgTextureSetMouseDown(Sender: TObject; {%H-}Button: TMouseButton;
      {%H-}Shift: TShiftState; {%H-}X, Y: Integer; {%H-}Layer: TCustomLayer);
    procedure tbShadingChange(Sender: TObject);
    procedure VerifyHexKey(Sender: TObject; var Key: char);
    procedure ebUnknown1Change(Sender: TObject);
    procedure ebUnknown2Change(Sender: TObject);
  private
    FaceImg: array[0..5] of TImage32;
    fLevel: TL3DLevel;
    fBlock: TL3DMetablock;
    fLocalBlock: TL3DMetablock;

    fSelectedFace: Integer;

    procedure Confirm;
    procedure SelectFace(Index: Integer);
    procedure DrawFaces;
  public
    procedure Initialize(aLevel: TL3DLevel; aBlock: TL3DMetablock; aTextureBitmap: TBitmap32);
  end;

implementation

{$R *.dfm}

{ TFEditMetablock }

procedure TFEditMetablock.VerifyHexKey(Sender: TObject; var Key: char);
begin
  if CharInSet(Key, ['a'..'f']) then
    Key := Uppercase(Key)[1]
  else if not (CharInSet(Key, ['A'..'F', '0'..'9']) or (Key < #20)) then
    Key := #0;
end;

procedure TFEditMetablock.Initialize(aLevel: TL3DLevel; aBlock: TL3DMetablock;
  aTextureBitmap: TBitmap32);
var
  i: Integer;
begin
  fLevel := aLevel;
  fBlock := aBlock;
  if aTextureBitmap <> nil then
  begin
    imgTextureSet.Bitmap.Assign(aTextureBitmap);
    imgTextureSet.Bitmap.DrawMode := dmTransparent;
  end;

  if rfZapInsteadOfDrown in fLevel.RenderFlags then
    cgBlockProperties.Items[2] := 'Electrified (Top)';

  fLocalBlock.Flags := aBlock.Flags;
  fLocalBlock._Unknown0000[0] := aBlock._Unknown0000[0];
  fLocalBlock._Unknown0000[1] := aBlock._Unknown0000[1];
  fLocalBlock._Unknown0003 := aBlock._Unknown0003;
  for i := 0 to 5 do
  begin
    fLocalBlock.Faces[i].TextureIndex := aBlock.Faces[i].TextureIndex;
    fLocalBlock.Faces[i].Modifiers := aBlock.Faces[i].Modifiers;
    fLocalBlock.Faces[i].Shade := aBlock.Faces[i].Shade;
  end;

  cgBlockProperties.OnClick := nil;
  try
    for i := 0 to 7 do
      cgBlockProperties.Checked[i] := TL3DBlockFlag(i) in fLocalBlock.Flags;
  finally
    cgBlockProperties.OnClick := cgBlockPropertiesItemClick;
  end;

  ebUnknown1.Text := IntToHex(fLocalBlock._Unknown0000[0], 2) + IntToHex(fLocalBlock._Unknown0000[1], 2);
  ebUnknown2.Text := IntToHex(fLocalBlock._Unknown0003, 2);

  imgTextureSet.Changed;

  DrawFaces;
  SelectFace(FACE_Z31);
end;

procedure TFEditMetablock.btnOKClick(Sender: TObject);
begin
  Confirm;
  ModalResult := mrOk;
end;

procedure TFEditMetablock.btnClearBlockClick(Sender: TObject);
var
  i: Integer;
  OldBlock: TL3DMetaBlock;
begin
  fLocalBlock.Flags := [];
  for i := 0 to 5 do
  begin
    fLocalBlock.Faces[i].TextureIndex := 255;
    fLocalBlock.Faces[i].Modifiers := [];
    fLocalBlock.Faces[i].Shade := 0;
  end;

  OldBlock := fBlock;
  Initialize(fLevel, fLocalBlock, nil);
  fBlock := OldBlock;
end;

procedure TFEditMetablock.cgBlockPropertiesItemClick(Sender: TObject);
var
  Flag: TL3DBlockFlag;
begin
  Flag := TL3DBlockFlag(cgBlockProperties.ItemIndex);

  if Flag in fLocalBlock.Flags then
    Exclude(fLocalBlock.Flags, Flag)
  else
    Include(fLocalBlock.Flags, Flag);

  for Flag := Low(TL3DBlockFlag) to High(TL3DBlockFlag) do
    cgBlockProperties.Checked[Integer(Flag)] := Flag in fLocalBlock.Flags;
end;

procedure TFEditMetablock.cgFacePropertiesItemClick(Sender: TObject);
var
  Flag: TL3DMetaBlockFlag;
begin
  Flag := TL3DMetaBlockFlag(cgFaceProperties.ItemIndex);

  if Flag in fLocalBlock.Faces[fSelectedFace].Modifiers then
    Exclude(fLocalBlock.Faces[fSelectedFace].Modifiers, Flag)
  else
    Include(fLocalBlock.Faces[fSelectedFace].Modifiers, Flag);

  for Flag := Low(TL3DMetaBlockFlag) to High(TL3DMetaBlockFlag) do
    cgFaceProperties.Checked[Integer(Flag)] := Flag in fLocalBlock.Faces[fSelectedFace].Modifiers;

  DrawFaces;
end;

procedure TFEditMetablock.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  fLocalBlock := TL3DMetablock.Create;

  FaceImg[FACE_X31] := imgX31Face;
  FaceImg[FACE_Z31] := imgZ31Face;
  FaceImg[FACE_X0] := imgX0Face;
  FaceImg[FACE_Z0] := imgZ0Face;
  FaceImg[FACE_Y0] := imgBottomFace;
  FaceImg[FACE_Y15] := imgTopFace;

  for i := 0 to 5 do
    FaceImg[i].Bitmap.SetSize(64, 64);
end;

procedure TFEditMetablock.FormDestroy(Sender: TObject);
begin
  fLocalBlock.Destroy;
end;

procedure TFEditMetablock.imgFaceClick(Sender: TObject);
begin
  if not (Sender is TImage32) then Exit;
  SelectFace(TImage32(Sender).Tag);
end;

procedure TFEditMetablock.imgTextureSetMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  BlockIndex: Integer;
begin
  BlockIndex := Y div 64;

  if BlockIndex > 99 then
  begin
    BlockIndex := $FF;
  end else if mbfSpecialAnim in fLocalBlock.Faces[fSelectedFace].Modifiers then
  begin
    case BlockIndex of
      12..15: BlockIndex := 0;
      16..19: BlockIndex := 1;
      0..7: BlockIndex := 2;
      28..31: BlockIndex := 3;
      32..35: BlockIndex := 4;
      36..39: BlockIndex := 5;
      else begin
        ShowMessage('This graphic cannot be used when the Special Animation flag is enabled.');
        Exit;
      end;
    end;
  end else if mbfAltTextures in fLocalBlock.Faces[fSelectedFace].Modifiers then
  begin
    case BlockIndex of
      43..54: BlockIndex := BlockIndex - 35;
      else begin
        ShowMessage('This graphic cannot be used with the alt-textures flag.');
        Exit;
      end;
    end;
  end;

  fLocalBlock.Faces[fSelectedFace].TextureIndex := BlockIndex;
  DrawFaces;
end;

procedure TFEditMetablock.tbShadingChange(Sender: TObject);
begin
  fLocalBlock.Faces[fSelectedFace].Shade := tbShading.Position;
  DrawFaces;
end;

procedure TFEditMetablock.Confirm;
var
  i: Integer;
begin
  fBlock._Unknown0000[0] := fLocalBlock._Unknown0000[0];
  fBlock._Unknown0000[1] := fLocalBlock._Unknown0000[1];
  fBlock._Unknown0003 := fLocalBlock._Unknown0003;
  fBlock.Flags := fLocalBlock.Flags;
  for i := 0 to 5 do
  begin
    fBlock.Faces[i].TextureIndex := fLocalBlock.Faces[i].TextureIndex;
    fBlock.Faces[i].Modifiers := fLocalBlock.Faces[i].Modifiers;
    fBlock.Faces[i].Shade := fLocalBlock.Faces[i].Shade;
  end;
end;

procedure TFEditMetablock.SelectFace(Index: Integer);
var
  i: Integer;
begin
  FaceImg[fSelectedFace].Color := clFuchsia;
  fSelectedFace := Index;
  FaceImg[fSelectedFace].Color := clRed;

  tbShading.Position := fLocalBlock.Faces[fSelectedFace].Shade;

  cgFaceProperties.OnClick := nil;
  try
    for i := 0 to 7 do
      cgFaceProperties.Checked[i] := TL3DMetablockFlag(i) in fLocalBlock.Faces[fSelectedFace].Modifiers;
  finally
    cgFaceProperties.OnClick := cgFacePropertiesItemClick;
  end;
end;

procedure TFEditMetablock.DrawFaces;
var
  i: Integer;
begin
  for i := 0 to 5 do
  begin
    FaceImg[i].Bitmap.Clear($00000000);
    if fLocalBlock.Faces[i].DisplayTextureIndex in [0..99] then
      imgTextureSet.Bitmap.DrawTo(FaceImg[i].Bitmap, FaceImg[i].Bitmap.BoundsRect, SizeRect(0, fLocalBlock.Faces[i].DisplayTextureIndex * 64, 64, 64));
    if (mbfAltTextures in fLocalBlock.Faces[i].Modifiers) or
       (mbfTransparent in fLocalBlock.Faces[i].Modifiers) then
      FaceImg[i].Bitmap.DrawMode := dmTransparent
    else
      FaceImg[i].Bitmap.DrawMode := dmOpaque;

    ApplyShade(FaceImg[i].Bitmap, SHADE_VALUES[fLocalBlock.Faces[i].Shade]);

    FaceImg[i].Changed;
  end;
end;

procedure TFEditMetablock.ebUnknown1Change(Sender: TObject);
begin
  fLocalBlock._Unknown0000[0] := StrToIntDef('x' + LeftStr(ebUnknown1.Text, 2), 0);
  fLocalBlock._Unknown0000[1] := StrToIntDef('x' + RightStr(ebUnknown1.Text, 2), 0);
end;

procedure TFEditMetablock.ebUnknown2Change(Sender: TObject);
begin
  fLocalBlock._Unknown0003 := StrToIntDef('x' + ebUnknown2.Text, 0);
end;

end.

