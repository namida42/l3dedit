unit L3DLevel;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  L3DLevelData,
  L3DDecompressor,
  L3DGraphics,
  Misc,
  GR32,
  Generics.Collections,
  Classes, SysUtils, StrUtils, Math;

const
  FACE_Z31 = 0;
  FACE_Z0  = 1;
  FACE_X31 = 2;
  FACE_X0  = 3;
  FACE_Y15 = 4;
  FACE_Y0  = 5;

type

  { Metablock }

  TL3DMetaBlockFlag = (mbfUnknown1, mbfUnknown2, mbfUnknown3, mbfAltTextures,
                       mbfFourPanelAnimA, mbfFourPanelAnimB, mbfSpecialAnim, mbfTransparent);
  TL3DMetaBlockFlags = set of TL3DMetaBlockFlag;

  { TL3DMetaBlockFace }

  TL3DMetaBlockFace = record
    TextureIndex: Integer;
    Modifiers: TL3DMetaBlockFlags;
    Shade: Integer;

    Texture: TBitmap32;

    function DisplayTextureIndex: Integer;
  end;

  TL3DBlockFlag = (bfRenderDoubleSided, bfSteel, bfTopLiquid, bfUnknown,
                   bfNonSolid, bfNoSplat, bfSlippery, bfNonSolidToCamera);

  TL3DBlockFlags = set of TL3DBlockFlag;

  TL3DMetaBlock = class
    public
      Faces: array[0..5] of TL3DMetaBlockFace;
      Flags: TL3DBlockFlags;
      _Unknown0000: array[0..1] of Byte;
      _Unknown0003: Byte;

      constructor Create;
      destructor Destroy; override;
      procedure Clear;
  end;

  { Level }

  TL3DSkill = (skBlocker, skTurner, skBomber, skBuilder, skBasher, skMiner,
               skDigger, skClimber, skFloater);

  { T3DPoint }

  T3DPoint = record
    X: Integer;
    Y: Integer;
    Z: Integer;

    function IsInside(X1, X2, Y1, Y2, Z1, Z2: Integer): Boolean; inline;
    class operator Equal(A, B: T3DPoint): Boolean;
  end;

  function Make3DPoint(X, Y, Z: Integer): T3DPoint;

type
  TInitialCamera = record
    Position: T3DPoint;
    Orientation: Integer;
  end;

  TL3DLevelRenderFlag = (rfSolidSea, rfSolidSky, rfHideLand, rfDisableQuickJump, rfDontAnimateSea,
                         rfLargeSeaTexture, rfFixedPreviewCamera, rSolidBottom, rfSingleLandTexture,
                         rfSurroundSky, rfReplaceWallsWithScreen, rfHalfSizeLand, rfDisableMinimap,
                         rfUnknown0x2000, rfUnknown0x4000, rfZapInsteadOfDrown,
                         rfSlipperyBottom, rfSlipperyLand, rfSlippery0x04, rfSlippery0x08,
                         rfSlippery0x10, rfSlippery0x20, rfSlippery0x40, rfSlippery0x80); // These last eight are NOT in the main render flags byte in actual level file data.
  TL3DLevelRenderFlags = set of TL3DLevelRenderFlag;

  TL3DBlockShape = (bsStandard, bsPyramidUpTypeA, bsPyramidDownTypeA, bsPyramidUpTypeB,
                    bsPyramidDownTypeB, bsSlope45Above, bsSlope45Below, bsDeflector,
                    bsSlope225Above, bsSlope225Below, bsSlopeCornerAbove,
                    bsSlopeCornerBelow, bsSlopeDiagonalAbove, bsSlopeDiagonalBelow);

  TLandVertexPoint = record
    X: Integer;
    Z: Integer;
    class operator Equal(A, B: TLandVertexPoint): Boolean;
  end;

  { TL3DLandPolygon }

  TL3DLandPolygon = class(TList<TLandVertexPoint>)
    public
      FloorGraphic: Integer;
      FloorShade: Integer;
      procedure Clear;
      procedure EnsureAntiClockwise;
  end;
  TL3DLandPolygons = TObjectList<TL3DLandPolygon>;

  { TL3DBlock }

  TL3DBlock = record
    Graphic: Integer;
    Shape: TL3DBlockShape;
    Rotation: Integer;
    Segments: array[0..3] of Boolean; // bottom to top

    strict private
      procedure SetRawData(aValue: Word);
      function GetRawData: Word;
      function GetEmpty: Boolean;
      function GetFull: Boolean;
    public
      property RawData: Word read GetRawData write SetRawData;
      property Empty: Boolean read GetEmpty;
      property Full: Boolean read GetFull;
  end;

  TL3DObjectType = (otStandard, otSign, otAnimation, otInteractive, otWall, otNothing);
  TL3DObjectSplit = (osWhole, osLeft, osRight);

  { TL3DObject }

  TL3DObject = record
    RawByte1: Byte; // This is the only one known to actually do anything.
    RawByte2: Byte;

    strict private
      function GetGraphicIndex: Integer;
      function GetHalfHeight: Boolean;
      function GetIsBlueTrampoline: Boolean;
      function GetIsReceiver: Boolean;
      function GetObjectType: TL3DObjectType;
      function GetOrientation: Integer;
      function GetBidirectionalPairing: Integer;
      function GetSinglePairing: Integer;
      function GetRawData: Word;
      function GetSplit: TL3DObjectSplit;
      procedure SetGraphicIndex(aValue: Integer);
      procedure SetHalfHeight(aValue: Boolean);
      procedure SetIsBlueTrampoline(aValue: Boolean);
      procedure SetIsReceiver(aValue: Boolean);
      procedure SetObjectType(aValue: TL3DObjectType);
      procedure SetOrientation(aValue: Integer);
      procedure SetBidirectionalPairing(aValue: Integer);
      procedure SetSinglePairing(aValue: Integer);
      procedure SetRawData(aValue: Word);
      procedure SetSplit(aValue: TL3DObjectSplit);

    public
      property RawData: Word read GetRawData write SetRawData;

      property ObjectType: TL3DObjectType read GetObjectType write SetObjectType;
      property GraphicIndex: Integer read GetGraphicIndex write SetGraphicIndex;
      property Orientation: Integer read GetOrientation write SetOrientation;
      property Split: TL3DObjectSplit read GetSplit write SetSplit;
      property IsReceiver: Boolean read GetIsReceiver write SetIsReceiver;
      property IsBlueTrampoline: Boolean read GetIsBlueTrampoline write SetIsBlueTrampoline;
      property BidirectionalPairing: Integer read GetBidirectionalPairing write SetBidirectionalPairing;
      property SinglePairing: Integer read GetSinglePairing write SetSinglePairing;
      property HalfHeight: Boolean read GetHalfHeight write SetHalfHeight;
      property _Unknown0002: Byte read RawByte2 write RawByte2;
  end;

  TL3DLevelHeaderUnknowns = packed record
    _UnknownCamera0002: TL3DLevelFileCameraPosition;
    _Always0x00_001C: array[0..5] of Byte;
    _Unknown00EA: array[0..3] of Byte;
    _Always0x00_00EE: Byte;
    _Unknown00EF: Byte;
    _Unknown0116: Byte;
    _Unknown011A: Byte;
    _Unknown013D: array[0..1] of Byte;
    _Unknown0141: array[0..1] of Byte;
    _Unknown0144: array[0..2] of Byte;
    _Always0x00_0147: Byte;
    _Unknown0148: array[0..7] of Byte;
    _Always0x00_0150: Byte;
    _Always0x00_0152: Byte;
    _Always0x00_0158: Byte;
    _Unknown017D: array[0..9] of Byte;
    _Always0x00_0187: array[0..6] of Byte;
    _Unknown018E: Byte;
    _Always0x00_018F: Byte;
    _Unknown0190: Byte;
    _Always0x00_0191: array[0..110] of Byte;
  end;

  { TL3DLevel }

  TL3DLevel = class
    private
      procedure SetTimeLimitSeconds(const aValue: Integer);
      procedure SetTimeLimitMinutes(const aValue: Integer);
      function GetTimeLimitSeconds: Integer;
      function GetTimeLimitMinutes: Integer;

      procedure LoadLevelDataFromStream(aStream: TStream);
      procedure LoadLevelDataFromRecord(Rec: TL3DLevelFile);
      procedure LoadBlockDataFromStream(aStream: TStream);
      procedure SaveLevelDataToStream(aStream: TStream);
      procedure SaveBlockDataToStream(aStream: TStream);
    public
      HeaderUnknowns: TL3DLevelHeaderUnknowns;

      Title: String;
      Comment: String;

      LemmingCount: Integer;
      SaveRequirement: Integer;
      TimeLimit: Integer;
      ReleaseRate: Integer;

      SkillCounts: array[TL3DSkill] of Integer;

      Style: Integer;
      Music: Integer;

      RenderFlags: TL3DLevelRenderFlags;
      FaceRenderLimit: Integer;

      TextureSet: Integer;
      Land: Integer;
      ObjectSet: Integer;
      Sign: Integer;
      Sea: Integer;
      AnimatedObject: Integer;
      InteractiveObject: Integer;
      Sky: Integer;
      Walls: Integer;

      WaterSpeedX: Integer;
      WaterSpeedZ: Integer;

      PreviewCameraPivot: T3DPoint;

      InitialCameraPositions: array[0..3] of TInitialCamera;

      LandPolygons: TL3DLandPolygons;

      BorderKills: array[0..4] of Integer; // Not used for FACE_Y0... at least as far as currently known

      Blocks: array[0..31, 0..15, 0..31] of TL3DBlock;
      Objects: array[0..31, 0..15, 0..31] of TL3DObject;

      MetaBlocks: array[0..63] of TL3DMetaBlock;

      constructor Create;
      destructor Destroy; override;

      procedure LoadFromFile(aFilename: String);
      procedure LoadFromStreams(aLevelStream: TStream; aBlockStream: TStream);

      procedure SaveToFile(aFilename: String);
      procedure SaveToStreams(aLevelStream: TStream; aBlockStream: TStream);

      procedure Clear;

      procedure PurgeBlockFaceGraphics;

      property TimeLimitMinutes: Integer read GetTimeLimitMinutes write SetTimeLimitMinutes;
      property TimeLimitSeconds: Integer read GetTimeLimitSeconds write SetTimeLimitSeconds;
  end;

implementation

function Make3DPoint(X, Y, Z: Integer): T3DPoint;
begin
  Result.X := X;
  Result.Y := Y;
  Result.Z := Z;
end;

{ TL3DMetaBlockFace }

function TL3DMetaBlockFace.DisplayTextureIndex: Integer;
begin
  Result := TextureIndex;

  if mbfAltTextures in Modifiers then
    case Result of
      8..19: Result := Result + 35; // there's extra considerations for double-sided blocks too, but I'll worry about that later
      else Result := -1;
    end;

  if mbfSpecialAnim in Modifiers then
    case Result of
      0: Result := 12;
      1: Result := 16;
      2: Result := 0;
      3: Result := 28;
      4: Result := 32;
      5: Result := 36;
      6: Result := 0;
      else Result := -1;
    end;
end;

{ TL3DLandPolygon }

procedure TL3DLandPolygon.Clear;
begin
  inherited Clear;
  FloorGraphic := 0;
  FloorShade := 0;
end;

procedure TL3DLandPolygon.EnsureAntiClockwise;
var
  OrigPoints: array of TLandVertexPoint;
  Area: Single;
  i: Integer;
  P1, P2: TLandVertexPoint;
begin
  if Count < 3 then Exit;

  Area := 0;

  for i := 0 to 2 do
  begin
    P1 := Items[i];
    P2 := Items[(i + 1) mod 3];
    Area := Area + ((P2.X - P1.X) * (P2.Z + P1.Z));
  end;

  if Area < 0 then
  begin
    SetLength(OrigPoints, Count);
    for i := 0 to Count-1 do
      OrigPoints[i] := Items[i];
    Clear;
    for i := Length(OrigPoints)-1 downto 0 do
      Add(OrigPoints[i]);
  end;
end;

{ T3DPoint }

function T3DPoint.IsInside(X1, X2, Y1, Y2, Z1, Z2: Integer): Boolean;
begin
  Result := (X >= X1) and (X < X2) and (Y >= Y1) and (Y < Y2) and
            (Z >= Z1) and (Z < Z2);
end;

class operator T3DPoint.Equal(A, B: T3DPoint): Boolean;
begin
  Result := (A.X = B.X) and (A.Y = B.Y) and (A.Z = B.Z);
end;

{ TL3DMetaBlock }

constructor TL3DMetaBlock.Create;
var
  i: Integer;
begin
  inherited;
  for i := 0 to 5 do
  begin
    Faces[i].Texture := TBitmap32.Create;
    Faces[i].Texture.Clear($FF000000);
  end;
end;

destructor TL3DMetaBlock.Destroy;
begin
  inherited Destroy;
end;

procedure TL3DMetaBlock.Clear;
var
  i: Integer;
begin
  for i := 0 to 5 do
  begin
    if Faces[i].Texture <> nil then
      Faces[i].Texture.Clear($FF000000);
    Faces[i].TextureIndex := -1;
    Faces[i].Modifiers := [];
    Faces[i].Shade := 0;
  end;

  Flags := [];
  _Unknown0000[0] := 0;
  _Unknown0000[1] := 0;
  _Unknown0003 := 0;
end;

{ TL3DObject }

// It's much more convenient to store the raw byte, and operate on it. But this
// makes the code harder to understand. If you're trying to understand how the
// data itself works, I suggest looking instead at "LEVEL.xxx.txt" in the "notes"
// folder of this repo. It'll probably help with understanding this code too.

function TL3DObject.GetGraphicIndex: Integer;
begin
  case ObjectType of
    otStandard: if RawByte1 <= 20 then Result := (RawByte1 and $1F) - 1 else Result := 0;
    otSign: Result := ((RawByte1 shr 2) and $07) xor $04;
    otAnimation: if (RawByte1 = $51) then Result := 1 else Result := 0;
    otInteractive: Result := -1; // TL3DObject alone can't provide a logical answer to this.
    otWall: Result := RawByte1 and $0F;
    else Result := -1;
  end;
end;

procedure TL3DObject.SetGraphicIndex(aValue: Integer);
begin
  if aValue < 0 then aValue := 0;

  case ObjectType of
    otStandard: if aValue <= 20 then RawByte1 := aValue - 1 else RawByte1 := $01;
    otSign: RawByte1 := (RawByte1 and $E3) or ((aValue and $07) shr 2);

  end;
end;

function TL3DObject.GetHalfHeight: Boolean;
begin
  Result := (RawByte1 >= $D0) and (RawByte1 <= $EF);
end;

procedure TL3DObject.SetHalfHeight(aValue: Boolean);
begin
  case RawByte1 of
    $70..$7F, $90..$9F: if aValue then RawByte1 := (RawByte1 and $0F) or $D0;
    $80..$8F, $A0..$AF: if aValue then RawByte1 := (RawByte1 and $0F) or $E0;
    $D0..$DF: if not aValue then RawByte1 := (RawByte1 and $0F) or $70;
    $E0..$EF: if not aValue then RawByte1 := (RawByte1 and $0F) or $80;
  end;
end;

function TL3DObject.GetIsBlueTrampoline: Boolean;
begin
  Result := (ObjectType = otInteractive) and ((RawByte1 and $04) <> 0);
end;

procedure TL3DObject.SetIsBlueTrampoline(aValue: Boolean);
begin
  if ObjectType = otInteractive then
    if aValue then
      RawByte1 := RawByte1 or $04
    else
      RawByte1 := RawByte1 and not $04;
end;

function TL3DObject.GetIsReceiver: Boolean;
begin
  if ObjectType = otInteractive then
    Result := (RawByte1 and $01) = 1
  else
    Result := false;
end;

procedure TL3DObject.SetIsReceiver(aValue: Boolean);
begin
  if ObjectType = otInteractive then
  begin
    RawByte1 := RawByte1 and $FE;
    if aValue then
      RawByte1 := RawByte1 or $01;
  end;
end;

function TL3DObject.GetObjectType: TL3DObjectType;
begin
  case RawByte1 of
    $01..$1F: Result := otStandard;
    $20..$3F: Result := otSign;
    $50..$5B: Result := otAnimation;
    $60..$67: Result := otInteractive;
    $70..$EF: Result := otWall;
    else Result := otNothing;
  end;
end;

procedure TL3DObject.SetObjectType(aValue: TL3DObjectType);
begin
  // It's simply not practical to try and logically convert between types, so we just
  // nuke EVERYTHING in the object data when changing the type.
  case aValue of
    otStandard: RawByte1 := $01;
    otSign: RawByte1 := $20;
    otAnimation: RawByte1 := $50;
    otInteractive: RawByte1 := $60;
    otWall: RawByte1 := $70;
    else RawByte1 := $00;
  end;
end;

function TL3DObject.GetOrientation: Integer;
begin
  case ObjectType of
    otSign: Result := RawByte1 and $03;
    otAnimation: if (RawByte1 = $51) then Result := 0 else Result := RawByte1 and $03;
    otInteractive: Result := RawByte1 and $03; // Some don't use it, but for those that do, this always works.
    otWall: if RawByte1 < $D0 then Result := (RawByte1 - $70) shr 4 else if RawByte1 < $E0 then Result := 0 else Result := 1;
    else Result := 0;
  end;
end;

procedure TL3DObject.SetOrientation(aValue: Integer);
begin
  if aValue < 0 then aValue := 0;

  case ObjectType of
    otSign: RawByte1 := (RawByte1 and $FC) or (aValue and $03);
    otAnimation: if (RawByte1 <> $51) then RawByte1 := (RawByte1 and $FC) or (aValue and $03);
    otInteractive: RawByte1 := (RawByte1 and $FC) or (aValue and $03); // We just have to trust this won't be called on types it shouldn't. Nothing else we can do.
    otWall: if RawByte1 < $D0 then RawByte1 := RawByte1 - $70 + ($10 * aValue) else RawByte1 := RawByte1 xor $30;
  end;
end;

function TL3DObject.GetBidirectionalPairing: Integer;
begin
  if ObjectType = otInteractive then
    Result := RawByte1 and $07
  else
    Result := -1;
end;

procedure TL3DObject.SetBidirectionalPairing(aValue: Integer);
begin
  if ObjectType = otInteractive then RawByte1 := $60 or (aValue and $07);
end;

function TL3DObject.GetSinglePairing: Integer;
begin
  if ObjectType = otInteractive then
    Result := (RawByte1 shr 1) and $03
  else
    Result := -1;
end;

procedure TL3DObject.SetSinglePairing(aValue: Integer);
begin
  if ObjectType = otInteractive then RawByte1 := (RawByte1 and $F1) or ((aValue and $03) shl 1);
end;

function TL3DObject.GetRawData: Word;
begin
  if ObjectType = otNothing then
    Result := 0
  else
    Result := {(RawByte2 shl 8) or} RawByte1;
end;

procedure TL3DObject.SetRawData(aValue: Word);
begin
  RawByte1 := aValue and $FF;
  {RawByte2 := (aValue shr 8) or $FF;}
end;

function TL3DObject.GetSplit: TL3DObjectSplit;
begin
  case RawByte1 of
    $54..$57: Result := osLeft;
    $58..$5B: Result := osRight;
    else Result := osWhole;
  end;
end;

procedure TL3DObject.SetSplit(aValue: TL3DObjectSplit);
begin
  if (ObjectType = otAnimation) and (RawByte1 <> $51) then
    case aValue of
      osLeft: RawByte1 := (RawByte1 and $03) or $54;
      osRight: RawByte1 := (RawByte1 and $03) or $58;
      osWhole: RawByte1 := $50;
    end;
end;

{ TL3DBlock }

procedure TL3DBlock.SetRawData(aValue: Word);
var
  i: Integer;
begin
  Graphic := (aValue shr 10) and $3F;
  Shape := TL3DBlockShape((aValue shr 6) and $0F);
  Rotation := (aValue shr 4) and $03;

  for i := 0 to 3 do
    Segments[i] := (aValue and ($08 shr i)) <> 0;
end;

function TL3DBlock.GetRawData: Word;
var
  i: Integer;
begin
  Result := ((Graphic and $3F) shl 10) or
            ((Integer(Shape) and $0F) shl 6) or
            ((Rotation and $03) shl 4);

  for i := 0 to 3 do
    if Segments[i] then
      Result := Result or ($08 shr i);
end;

function TL3DBlock.GetEmpty: Boolean;
var
  i: Integer;
begin
  Result := true;
  for i := 0 to 3 do
    if Segments[i] then
    begin
      Result := false;
      Exit;
    end;
end;

function TL3DBlock.GetFull: Boolean;
var
  i: Integer;
begin
  Result := true;
  for i := 0 to 3 do
    if not Segments[i] then
    begin
      Result := false;
      Exit;
    end;
end;

{ TL3DLevel }

procedure TL3DLevel.SetTimeLimitSeconds(const aValue: Integer);
begin
  TimeLimit := ((TimeLimit div 60) * 60) + aValue;
end;

procedure TL3DLevel.SetTimeLimitMinutes(const aValue: Integer);
begin
  TimeLimit := (aValue * 60) + (TimeLimit mod 60);
end;

function TL3DLevel.GetTimeLimitSeconds: Integer;
begin
  Result := TimeLimit mod 60;
end;

function TL3DLevel.GetTimeLimitMinutes: Integer;
begin
  Result := TimeLimit div 60;
end;

constructor TL3DLevel.Create;
var
  i: Integer;
begin
  inherited;
  for i := 0 to 63 do
    MetaBlocks[i] := TL3DMetaBlock.Create;
  LandPolygons := TL3DLandPolygons.Create;
  Clear;
end;

destructor TL3DLevel.Destroy;
var
  i: Integer;
begin
  for i := 0 to 63 do
    MetaBlocks[i].Free;
  LandPolygons.Free;
  inherited;
end;

procedure TL3DLevel.LoadFromFile(aFilename: String);
var
  FilePath: String;
  FileExt: String;
  LevelStream, BlockStream: TFileStream;
  Header: array[0..2] of AnsiChar;
begin
  FilePath := ExtractFilePath(aFilename);
  FileExt := ExtractFileExt(aFilename);
  Header := '   ';

  LevelStream := TFileStream.Create(FilePath + 'LEVEL' + FileExt, fmOpenRead);
  try
    LevelStream.Read(Header, 3);
    if Header <> 'RNC' then
    begin
      LevelStream.Position := 0;
      BlockStream := TFileStream.Create(FilePath + 'BLK' + FileExt, fmOpenRead);
      try
        LoadFromStreams(LevelStream, BlockStream);
        Exit;
      finally
        BlockStream.Free;
      end;
    end;
  finally
    LevelStream.Free;
  end;

  // Note that we will only get to here if the header is 'RNC'.
  // This is a bit confusing, but avoids messiness with the filestreams.
  DecompressFile(FilePath + 'LEVEL' + FileExt);
  LoadFromFile(aFilename);
end;

procedure TL3DLevel.LoadFromStreams(aLevelStream: TStream; aBlockStream: TStream);
begin
  Clear;
  LoadLevelDataFromStream(aLevelStream);
  LoadBlockDataFromStream(aBlockStream);
end;

procedure TL3DLevel.SaveToFile(aFilename: String);
var
  FilePath: String;
  FileExt: String;
  LevelStream, BlockStream: TFileStream;
begin
  FilePath := ExtractFilePath(aFilename);
  FileExt := ExtractFileExt(aFilename);

  LevelStream := TFileStream.Create(FilePath + 'LEVEL' + FileExt, fmCreate);
  try
    BlockStream := TFileStream.Create(FilePath + 'BLK' + FileExt, fmCreate);
    try
      SaveToStreams(LevelStream, BlockStream);
    finally
      BlockStream.Free;
    end;
  finally
    LevelStream.Free;
  end;
end;

procedure TL3DLevel.SaveToStreams(aLevelStream: TStream; aBlockStream: TStream);
begin
  SaveLevelDataToStream(aLevelStream);
  SaveBlockDataToStream(aBlockStream);
end;

procedure TL3DLevel.LoadLevelDataFromStream(aStream: TStream);
var
  LevelRec: TL3DLevelFile;
begin
  FillChar(LevelRec{%H-}, SizeOf(LevelRec), 0); // {&H-} suprresses an incorrect compiler warning. Yes, I'm 100% sure.
  aStream.Read(LevelRec, SizeOf(LevelRec));
  LoadLevelDataFromRecord(LevelRec);
end;

procedure TL3DLevel.SaveLevelDataToStream(aStream: TStream);
var
  Rec: TL3DLevelFile;
  AnsiText: AnsiString;

  Skill: TL3DSkill;

  i, n: Integer;
  X, Y, Z: Integer;
begin
  FillChar(Rec{%H-}, SizeOf(Rec), 0);

  Rec._UnknownCamera0002._Always0xC0_0000 := $C0;
  Rec._UnknownCamera0002._Always0x80_0002 := $80;
  Rec._UnknownCamera0002._Always0x80_0004 := $80;

  AnsiText := AnsiString(Title);
  Move(AnsiText[1], Rec.LevelName[0], Min(Length(AnsiText), 32));

  AnsiText := AnsiString(Comment);
  Move(AnsiText[1], Rec.Comment[0], Min(Length(AnsiText), 32));

  Rec.LemmingCount := LemmingCount;
  Rec.SaveRequirement := SaveRequirement;
  Rec.TimeMinutes := TimeLimitMinutes;
  Rec.TimeSeconds := TimeLimitSeconds;
  Rec.ReleaseRate := ReleaseRate;

  for Skill := Low(TL3DSkill) to High(TL3DSkill) do
  begin
    Rec.Skillset[Integer(Skill)].SkillID := Byte(Skill);
    Rec.Skillset[Integer(Skill)].SkillCount := SkillCounts[Skill];
  end;

  Rec.Style := Style;
  Rec.Music := Music;

  Rec.RenderFlags := Word(Integer(RenderFlags) and $FFFF);
  Rec.SlipperyFlag := Byte((Integer(RenderFlags) shr 16) and $FF);
  if rfDisableMinimap in RenderFlags then
    Rec.RenderFlags := Rec.RenderFlags or $0008;

  Rec.Textures := TextureSet;
  Rec.Land := Land;
  Rec.Objects := ObjectSet;
  Rec.Signs := Sign;
  Rec.Sea := Sea;
  Rec.AnimatedObject := AnimatedObject;
  Rec.InteractiveObject := InteractiveObject;
  Rec.Sky := Sky;
  Rec.Walls := Walls;

  Rec.WaterSpeedX := WaterSpeedX;
  Rec.WaterSpeedZ := WaterSpeedZ;

  Rec.PreviewCameraPivotX := PreviewCameraPivot.X;
  Rec.PreviewCameraPivotY := PreviewCameraPivot.Y;
  Rec.PreviewCameraPivotZ := PreviewCameraPivot.Z;

  Rec.FaceRenderLimit := FaceRenderLimit;

  for i := 0 to 3 do
  begin
    Rec.CameraPositions[i].X := InitialCameraPositions[i].Position.X - 16;
    Rec.CameraPositions[i].Y := InitialCameraPositions[i].Position.Y - 8;
    Rec.CameraPositions[i].Z := InitialCameraPositions[i].Position.Z - 16;
    Rec.CameraPositions[i].Rotation := InitialCameraPositions[i].Orientation;
    Rec.CameraPositions[i]._Always0xC0_0000 := $C0;
    Rec.CameraPositions[i]._Always0x80_0002 := $80;
    Rec.CameraPositions[i]._Always0x80_0004 := $80;
  end;

  Rec.BorderKill[0] := BorderKills[FACE_Z0];
  Rec.BorderKill[1] := BorderKills[FACE_X0];
  Rec.BorderKill[2] := BorderKills[FACE_Z31];
  Rec.BorderKill[3] := BorderKills[FACE_X31];
  Rec.CeilingKill := BorderKills[FACE_Y15];

  for i := 0 to Min(LandPolygons.Count-1, 7) do
  begin
    LandPolygons[i].EnsureAntiClockwise;
    for n := 0 to Min(LandPolygons[i].Count-1, 7) do
    begin
      Rec.LandVertexSets[i][n].LandVertexX := LandPolygons[i][n].X;
      Rec.LandVertexSets[i][n].LandVertexZ := LandPolygons[i][n].Z;
      Rec.LandVertexSets[i][n].FloorGraphic := (LandPolygons[i].FloorGraphic and $07) or ((LandPolygons[i].FloorShade and $07) shl 5); // middle two bits are unknown
    end;
  end;

  n := 0;
  for X := 0 to 31 do
    for Y := 0 to 15 do
      for Z := 0 to 31 do
      begin
        Rec.BlockData[n] := Blocks[X, Y, Z].RawData;
        Rec.ObjectData[n] := Objects[X, Y, Z].RawData or (Objects[X, Y, Z].RawByte2 shl 8);
        Inc(n);
      end;

  Rec._UnknownCamera0002 := HeaderUnknowns._UnknownCamera0002;
  Move(HeaderUnknowns._Always0x00_001C, Rec._Always0x00_001C, SizeOf(HeaderUnknowns._Always0x00_001C));
  Move(HeaderUnknowns._Unknown00EA, Rec._Unknown00EA, SizeOf(HeaderUnknowns._Unknown00EA));
  Move(HeaderUnknowns._Always0x00_00EE, Rec._Always0x00_00EE, SizeOf(HeaderUnknowns._Always0x00_00EE));
  Move(HeaderUnknowns._Unknown00EF, Rec._Unknown00EF, SizeOf(HeaderUnknowns._Unknown00EF));
  Move(HeaderUnknowns._Unknown0116, Rec._Unknown0116, SizeOf(HeaderUnknowns._Unknown0116));
  Move(HeaderUnknowns._Unknown011A, Rec._Unknown011A, SizeOf(HeaderUnknowns._Unknown011A));
  Move(HeaderUnknowns._Unknown013D, Rec._Unknown013D, SizeOf(HeaderUnknowns._Unknown013D));
  Move(HeaderUnknowns._Unknown0141, Rec._Unknown0141, SizeOf(HeaderUnknowns._Unknown0141));
  Move(HeaderUnknowns._Unknown0144, Rec._Unknown0144, SizeOf(HeaderUnknowns._Unknown0144));
  Move(HeaderUnknowns._Always0x00_0147, Rec._Always0x00_0147, SizeOf(HeaderUnknowns._Always0x00_0147));
  Move(HeaderUnknowns._Unknown0148, Rec._Unknown0148, SizeOf(HeaderUnknowns._Unknown0148));
  Move(HeaderUnknowns._Always0x00_0150, Rec._Always0x00_0150, SizeOf(HeaderUnknowns._Always0x00_0150));
  Move(HeaderUnknowns._Always0x00_0152, Rec._Always0x00_0152, SizeOf(HeaderUnknowns._Always0x00_0152));
  Move(HeaderUnknowns._Always0x00_0158, Rec._Always0x00_0158, SizeOf(HeaderUnknowns._Always0x00_0158));
  Move(HeaderUnknowns._Unknown017D, Rec._Unknown017D, SizeOf(HeaderUnknowns._Unknown017D));
  Move(HeaderUnknowns._Always0x00_0187, Rec._Always0x00_0187, SizeOf(HeaderUnknowns._Always0x00_0187));
  Move(HeaderUnknowns._Unknown018E, Rec._Unknown018E, SizeOf(HeaderUnknowns._Unknown018E));
  Move(HeaderUnknowns._Always0x00_018F, Rec._Always0x00_018F, SizeOf(HeaderUnknowns._Always0x00_018F));
  Move(HeaderUnknowns._Unknown0190, Rec._Unknown0190, SizeOf(HeaderUnknowns._Unknown0190));
  Move(HeaderUnknowns._Always0x00_0191, Rec._Always0x00_0191, SizeOf(HeaderUnknowns._Always0x00_0191));

  aStream.Write(Rec, SizeOf(Rec));
end;

procedure TL3DLevel.LoadBlockDataFromStream(aStream: TStream);
var
  i, f: Integer;
  Recs: TL3DBlockFile;
  Rec: TL3DBlockFileEntry;
begin
  aStream.ReadBuffer(Recs{%H-}, SizeOf(Recs));
  for i := 0 to 63 do
  begin
    Rec := Recs[i];
    MetaBlocks[i].Flags := TL3DBlockFlags(Rec.Flags);
    Move(Rec.Unknown1, MetaBlocks[i]._Unknown0000, SizeOf(Rec.Unknown1));
    Move(Rec.Unknown2, MetaBlocks[i]._Unknown0003, SizeOf(Rec.Unknown2));

    for f := 0 to 5 do
    begin
      MetaBlocks[i].Faces[f].TextureIndex := Rec.Faces[f].Index;
      MetaBlocks[i].Faces[f].Modifiers := TL3DMetaBlockFlags(Rec.Faces[f].Modifier);
      MetaBlocks[i].Faces[f].Shade := Rec.Faces[f].Shade;
      FreeAndNil(MetaBlocks[i].Faces[f].Texture);
    end;
  end;
end;

procedure TL3DLevel.SaveBlockDataToStream(aStream: TStream);
var
  i, f: Integer;
  Recs: TL3DBlockFile;
  Rec: TL3DBlockFileEntry;
begin
  for i := 0 to 63 do
  begin
    FillChar(Rec{%H-}, SizeOf(Rec), 0);

    Rec.Flags := Byte(MetaBlocks[i].Flags);
    Move(MetaBlocks[i]._Unknown0000, Rec.Unknown1, SizeOf(Rec.Unknown1));
    Move(MetaBlocks[i]._Unknown0003, Rec.Unknown2, SizeOf(Rec.Unknown2));

    for f := 0 to 5 do
    begin
      Rec.Faces[f].Index := MetaBlocks[i].Faces[f].TextureIndex;
      Rec.Faces[f].Modifier := Byte(MetaBlocks[i].Faces[f].Modifiers);
      Rec.Faces[f].Shade := MetaBlocks[i].Faces[f].Shade;
    end;

    Recs[i] := Rec;
  end;

  aStream.WriteBuffer(Recs, SizeOf(Recs));
end;

procedure TL3DLevel.LoadLevelDataFromRecord(Rec: TL3DLevelFile);
var
  SkillRec: TL3DLevelFileSkillsetEntry;
  i, n: Integer;
  X, Y, Z: Integer;

  ThisPolygonValid: Boolean;
  NewPolygon: TL3DLandPolygon;
  NewPoint: TLandVertexPoint;
begin
  Title := Trim(String(Rec.LevelName));
  Comment := Trim(String(Rec.Comment));

  if LeftStr(Comment, 9) = 'L3DEdit V' then
    Comment := 'L3DEdit V' + VersionString;

  LemmingCount := Rec.LemmingCount;
  SaveRequirement := Rec.SaveRequirement;
  TimeLimit := (Rec.TimeMinutes * 60) + Rec.TimeSeconds;
  ReleaseRate := Rec.ReleaseRate;

  for SkillRec in Rec.Skillset do
    SkillCounts[TL3DSkill(SkillRec.SkillID)] := SkillRec.SkillCount;

  Style := Rec.Style;
  Music := Rec.Music;

  RenderFlags := TL3DLevelRenderFlags(Integer(Rec.RenderFlags)) +
                 TL3DLevelRenderFlags(Integer(Rec.SlipperyFlag) shl 16);

  TextureSet := Rec.Textures;
  Land := Rec.Land;
  ObjectSet := Rec.Objects;
  Sign := Rec.Signs;
  Sea := Rec.Sea;
  AnimatedObject := Rec.AnimatedObject;
  InteractiveObject := Rec.InteractiveObject;
  Sky := Rec.Sky;
  Walls := Rec.Walls;

  PreviewCameraPivot.X := Rec.PreviewCameraPivotX;
  PreviewCameraPivot.Y := Rec.PreviewCameraPivotY;
  PreviewCameraPivot.Z := Rec.PreviewCameraPivotZ;

  for i := 0 to 3 do
  begin
    InitialCameraPositions[i].Position.X := Rec.CameraPositions[i].X + 16;
    InitialCameraPositions[i].Position.Y := Rec.CameraPositions[i].Y + 8;
    InitialCameraPositions[i].Position.Z := Rec.CameraPositions[i].Z + 16;
    InitialCameraPositions[i].Orientation := Rec.CameraPositions[i].Rotation;
  end;

  BorderKills[FACE_Z0] := Rec.BorderKill[0];
  BorderKills[FACE_X0] := Rec.BorderKill[1];
  BorderKills[FACE_Z31] := Rec.BorderKill[2];
  BorderKills[FACE_X31] := Rec.BorderKill[3];
  BorderKills[FACE_Y15] := Rec.CeilingKill;

  WaterSpeedX := Rec.WaterSpeedX;
  WaterSpeedZ := Rec.WaterSpeedZ;

  FaceRenderLimit := Rec.FaceRenderLimit;

  for i := 0 to 7 do
  begin
    ThisPolygonValid := false;
    for n := 0 to 2 do
      if n = 2 then
        ThisPolygonValid := true
      else if (Rec.LandVertexSets[i][n].LandVertexX = 0) or (Rec.LandVertexSets[i][n].LandVertexZ = 0) then
        Break;
    if not ThisPolygonValid then Continue;

    NewPolygon := TL3DLandPolygon.Create;
    for n := 0 to 7 do
    begin
      NewPoint.X := Rec.LandVertexSets[i][n].LandVertexX;
      NewPoint.Z := Rec.LandVertexSets[i][n].LandVertexZ;

      if (NewPoint.Z = 0) then Break;

      NewPolygon.FloorGraphic := Rec.LandVertexSets[i][n].FloorGraphic and $07; // best way to implement "last vertex has priority"
      NewPolygon.FloorShade := (Rec.LandVertexSets[i][n].FloorGraphic and $E0) shr 5;
      NewPolygon.Add(NewPoint);
    end;

    LandPolygons.Add(NewPolygon);
  end;

  n := 0;
  for X := 0 to 31 do
    for Y := 0 to 15 do
      for Z := 0 to 31 do
      begin
        Blocks[X, Y, Z].RawData := Rec.BlockData[n];
        Objects[X, Y, Z].RawData := Rec.ObjectData[n];
        Objects[X, Y, Z]._Unknown0002 := (Rec.ObjectData[n] shr 8) and $FF;
        Inc(n);
      end;

  HeaderUnknowns._UnknownCamera0002 := Rec._UnknownCamera0002;
  Move(Rec._Always0x00_001C, HeaderUnknowns._Always0x00_001C, SizeOf(Rec._Always0x00_001C));
  Move(Rec._Unknown00EA, HeaderUnknowns._Unknown00EA, SizeOf(Rec._Unknown00EA));
  Move(Rec._Always0x00_00EE, HeaderUnknowns._Always0x00_00EE, SizeOf(Rec._Always0x00_00EE));
  Move(Rec._Unknown00EF, HeaderUnknowns._Unknown00EF, SizeOf(Rec._Unknown00EF));
  Move(Rec._Unknown0116, HeaderUnknowns._Unknown0116, SizeOf(Rec._Unknown0116));
  Move(Rec._Unknown011A, HeaderUnknowns._Unknown011A, SizeOf(Rec._Unknown011A));
  Move(Rec._Unknown013D, HeaderUnknowns._Unknown013D, SizeOf(Rec._Unknown013D));
  Move(Rec._Unknown0141, HeaderUnknowns._Unknown0141, SizeOf(Rec._Unknown0141));
  Move(Rec._Unknown0144, HeaderUnknowns._Unknown0144, SizeOf(Rec._Unknown0144));
  Move(Rec._Always0x00_0147, HeaderUnknowns._Always0x00_0147, SizeOf(Rec._Always0x00_0147));
  Move(Rec._Unknown0148, HeaderUnknowns._Unknown0148, SizeOf(Rec._Unknown0148));
  Move(Rec._Always0x00_0150, HeaderUnknowns._Always0x00_0150, SizeOf(Rec._Always0x00_0150));
  Move(Rec._Always0x00_0152, HeaderUnknowns._Always0x00_0152, SizeOf(Rec._Always0x00_0152));
  Move(Rec._Always0x00_0158, HeaderUnknowns._Always0x00_0158, SizeOf(Rec._Always0x00_0158));
  Move(Rec._Unknown017D, HeaderUnknowns._Unknown017D, SizeOf(Rec._Unknown017D));
  Move(Rec._Always0x00_0187, HeaderUnknowns._Always0x00_0187, SizeOf(Rec._Always0x00_0187));
  Move(Rec._Unknown018E, HeaderUnknowns._Unknown018E, SizeOf(Rec._Unknown018E));
  Move(Rec._Always0x00_018F, HeaderUnknowns._Always0x00_018F, SizeOf(Rec._Always0x00_018F));
  Move(Rec._Unknown0190, HeaderUnknowns._Unknown0190, SizeOf(Rec._Unknown0190));
  Move(Rec._Always0x00_0191, HeaderUnknowns._Always0x00_0191, SizeOf(Rec._Always0x00_0191));
end;

procedure TL3DLevel.Clear;
var
  i: Integer;
  Skill: TL3DSkill;
begin
  FillChar(HeaderUnknowns, SizeOf(HeaderUnknowns), 0);
  HeaderUnknowns._UnknownCamera0002._Always0xC0_0000 := $C0;
  HeaderUnknowns._UnknownCamera0002._Always0x80_0002 := $80;
  HeaderUnknowns._UnknownCamera0002._Always0x80_0004 := $80;

  Title := '(Untitled)';
  Comment := 'L3DEdit V' + VersionString;

  LemmingCount := 1;
  SaveRequirement := 1;
  TimeLimit := 300;
  ReleaseRate := 50;

  for Skill := Low(TL3DSkill) to High(TL3DSkill) do
    SkillCounts[Skill] := $10;

  Style := 0;
  Music := 0;

  RenderFlags := [];
  FaceRenderLimit := $0439; // Highest value that doesn't limit lemming count to less than 80

  TextureSet := $FF;
  Land := $FF;
  ObjectSet := $FF;
  Sign := $FF;
  Sea := $FF;
  AnimatedObject := $FF;
  InteractiveObject := $FF;
  Sky := $FF;
  Walls := $FF;

  PreviewCameraPivot.X := 16;
  PreviewCameraPivot.Y := 8;
  PreviewCameraPivot.Z := 16;

  // These values seem a bit strange, but they seem to be the "default" in official levels.
  BorderKills[FACE_X0] := 1;
  BorderKills[FACE_Z0] := 1;
  BorderKills[FACE_X31] := 31;
  BorderKills[FACE_Z31] := 31;
  BorderKills[FACE_Y15] := 15;

  LandPolygons.Clear;

  for i := 0 to 3 do
  begin
    InitialCameraPositions[i].Position := Make3DPoint(16, 8, 16);
    InitialCameraPositions[i].Orientation := 0;
  end;

  FillChar(Blocks, SizeOf(Blocks), 0);
  FillChar(Objects, SizeOf(Objects), 0);

  for i := 0 to 63 do
    MetaBlocks[i].Clear;
end;

procedure TL3DLevel.PurgeBlockFaceGraphics;
var
  i, f: Integer;
begin
  for i := 0 to 63 do
    for f := 0 to 5 do
      FreeAndNil(MetaBlocks[i].Faces[f].Texture);
end;

{ TLandVertexPoint }

class operator TLandVertexPoint.Equal(A, B: TLandVertexPoint): Boolean;
begin
  Result := (A.X = B.X) and (A.Z = B.Z);
end;

end.

