unit Misc;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  GR32,
  Forms,
  Math,
  Classes, SysUtils;

const
  VersionStringBase = '0.51';

  VersionStringSuffix = '-D';
  VersionString = VersionStringBase + VersionStringSuffix;

  SHADE_VALUES: array[0..8] of Byte =
    ( $FF, $FB, $E5, $D7, $CE, $BE, $B6, $A7, $9F );
  // These are not 100% accurate to L3D, but are derived from L3D's values. The difference is that
  // L3D applies different shading to the red channel vs the green and blue channels; I've averaged
  // the shadings here and apply the average to all three channels.

  function AppPath: String;
  function SizeRect(X, Y, Width, Height: Integer): TRect;
  function LeadZeroStr(aValue: Integer; aLength: Integer; aLengthIncludesNegativeSign: Boolean = false): String;

  function FloatPointMid(A, B: TFloatPoint): TFloatPoint;

  procedure ApplyShade(Img: TBitmap32; Shade: Byte);

type

  { TPolygon }

  TPolygon = record
    TopLeft: TFloatPoint;
    TopRight: TFloatPoint;
    BottomRight: TFloatPoint;
    BottomLeft: TFloatPoint;

    function RotatePoints(Rotations: Integer = 1): TPolygon;

    function CheckConcave: Boolean;
    function ShiftToOrigin: TPolygon;
    function Shift(dX, dY: Single): TPolygon;
    function Scale(aScale: Single): TPolygon;
    function ScaleCenterAnchored(aScale: Single): TPolygon;
    function AsFloatPoints: TArrayOfFloatPoint;
    function CenterPoint: TFloatPoint;
    function BoundsRect: TFloatRect;
  end;

  function Polygon(X0, Y0, X1, Y1, X2, Y2, X3, Y3: Single): TPolygon; overload;
  function Polygon(TopLeft, TopRight, BottomRight, BottomLeft: TFloatPoint): TPolygon; overload;

implementation

var
  _AppPath: String;

function AppPath: String;
begin
  if _AppPath = '' then
    _AppPath := IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName));

  Result := _AppPath;
end;

function SizeRect(X, Y, Width, Height: Integer): TRect;
begin
  Result := Rect(X, Y, X + Width, Y + Height);
end;

function LeadZeroStr(aValue: Integer; aLength: Integer; aLengthIncludesNegativeSign: Boolean = false): String;
var
  IsNegative: Boolean;
  PadCount: Integer;
begin
  IsNegative := aValue < 0;
  aValue := Abs(aValue);

  Result := IntToStr(aValue);

  PadCount := aLength - Length(Result);
  if IsNegative and aLengthIncludesNegativeSign then
    PadCount := PadCount - 1;

  if PadCount > 0 then
    Result := StringOfChar('0', PadCount) + Result;

  if IsNegative then
    Result := '-' + Result;
end;

function FloatPointMid(A, B: TFloatPoint): TFloatPoint;
begin
  Result.X := (A.X + B.X) / 2;
  Result.Y := (A.Y + B.Y) / 2;
end;

procedure ApplyShade(Img: TBitmap32; Shade: Byte);
var
  i: Integer;
  ImgPtr: PByte;
begin
  if Shade = $FF then Exit;

  ImgPtr := PByte(Img.PixelPtr[0, 0]);
  for i := 0 to (Img.Width * Img.Height * 3)-1 do
  begin
    ImgPtr^ := ImgPtr^ * Shade div $FF;
    Inc(ImgPtr);
    if i mod 3 = 2 then Inc(ImgPtr); // skip alpha
  end;
end;

function Polygon(X0, Y0, X1, Y1, X2, Y2, X3, Y3: Single): TPolygon;
begin
  Result := Polygon(FloatPoint(X0, Y0), FloatPoint(X1, Y1), FloatPoint(X2, Y2),
                    FloatPoint(X3, Y3));
end;

function Polygon(TopLeft, TopRight, BottomRight, BottomLeft: TFloatPoint): TPolygon;
begin
  Result.TopLeft := TopLeft;
  Result.TopRight := TopRight;
  Result.BottomRight := BottomRight;
  Result.BottomLeft := BottomLeft;
end;

{ TPolygon }

function TPolygon.RotatePoints(Rotations: Integer = 1): TPolygon;
begin
  Rotations := Rotations mod 4;
  if Rotations = 0 then
    Result := self
  else if Rotations < 0 then
    Result := RotatePoints(4 - (Abs(Rotations) mod 4))
  else begin
    Result.TopLeft := BottomLeft;
    Result.TopRight := TopLeft;
    Result.BottomRight := TopRight;
    Result.BottomLeft := BottomRight;

    if Rotations > 1 then
      Result := Result.RotatePoints(Rotations - 1);
  end;
end;

function TPolygon.CheckConcave: Boolean;

  function PerformSubCheck(LineCornerA, LineCornerB: TFloatPoint; TestCornerA, TestCornerB: TFloatPoint): Boolean;
  var
    LineGradient, LineShift: Single;
    DiffA, DiffB: Single;
  begin
    // We check: If we draw a line from LineCornerA to LineCornerB, is exactly one of TestCornerA and TestCornerB on
    //           each side of the line, treating points on the line exactly as not being on either side? If we
    //           perform this check with both ways around of opposite corners, this tells us if the polygon is valid.

    // First, handle a special case:
    if LineCornerA.X = LineCornerB.X then
    begin
      Result := ((TestCornerA.X <> LineCornerA.X) and (TestCornerB.X <> LineCornerA.X)) and
                ((TestCornerA.X < LineCornerA.X) <> (TestCornerB.X < LineCornerA.X));
      Exit;
    end;

    LineGradient := (LineCornerB.Y - LineCornerA.Y) / (LineCornerB.X - LineCornerA.X);
    LineShift := LineCornerA.Y - (LineCornerA.X * LineGradient);

    DiffA := ((TestCornerA.X * LineGradient) + LineShift) - TestCornerA.Y;
    DiffB := ((TestCornerB.X * LineGradient) + LineShift) - TestCornerB.Y;

    Result := ((DiffA < 0) and (DiffB > 0)) or ((DiffA > 0) and (DiffB < 0));
  end;

begin
  Result := PerformSubCheck(TopLeft, BottomRight, TopRight, BottomLeft) and
            PerformSubCheck(TopRight, BottomLeft, TopLeft, BottomRight);
end;

function TPolygon.ShiftToOrigin: TPolygon;
var
  MinX, MinY: Single;
begin
  MinX := Min(Min(TopLeft.X, TopRight.X), Min(BottomLeft.X, BottomRight.X));
  MinY := Min(Min(TopLeft.Y, TopRight.Y), Min(BottomLeft.Y, BottomRight.Y));
  Result := Shift(-MinX, -MinY);
end;

function TPolygon.Shift(dX, dY: Single): TPolygon;
begin
  Result := Polygon(TopLeft.X + dX, TopLeft.Y + dY,
                    TopRight.X + dX, TopRight.Y + dY,
                    BottomRight.X + dX, BottomRight.Y + dY,
                    BottomLeft.X + dX, BottomLeft.Y + dY);
end;

function TPolygon.Scale(aScale: Single): TPolygon;
begin
  Result := Polygon(TopLeft.X * aScale, TopLeft.Y * aScale,
                    TopRight.X * aScale, TopRight.Y * aScale,
                    BottomRight.X * aScale, BottomRight.Y * aScale,
                    BottomLeft.X * aScale, BottomLeft.Y * aScale);
end;

function TPolygon.ScaleCenterAnchored(aScale: Single): TPolygon;
var
  xShift, yShift: Double;
begin
  xShift := TopLeft.X;
  yShift := TopLeft.Y;
  Result := ShiftToOrigin;
  xShift := xShift - Result.TopLeft.X;
  yShift := yShift - Result.TopLeft.Y;
  Result := Result.Scale(aScale).Shift(xShift, yShift);
end;

function TPolygon.AsFloatPoints: TArrayOfFloatPoint;
begin
  SetLength(Result, 4);
  Result[0] := TopLeft;
  Result[1] := TopRight;
  Result[2] := BottomRight;
  Result[3] := BottomLeft;
end;

function TPolygon.CenterPoint: TFloatPoint;
begin
  Result.X := (TopLeft.X + TopRight.X + BottomRight.X + BottomLeft.X) / 4;
  Result.Y := (TopLeft.Y + TopRight.Y + BottomRight.Y + BottomLeft.Y) / 4;
end;

function TPolygon.BoundsRect: TFloatRect;
  procedure ApplyPoint(Point: TFloatPoint);
  begin
    if Result.Left > Point.X then Result.Left := Point.X;
    if Result.Right < Point.X then Result.Right := Point.X;
    if Result.Top > Point.Y then Result.Top := Point.Y;
    if Result.Bottom < Point.Y then Result.Bottom := Point.Y;
  end;

begin
  Result := FloatRect(TopLeft, TopLeft);
  ApplyPoint(TopRight);
  ApplyPoint(BottomRight);
  ApplyPoint(BottomLeft);
end;

end.

