object FReorderLevels: TFReorderLevels
  Left = 372
  Top = 393
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = 'Reorder Levels'
  ClientHeight = 266
  ClientWidth = 330
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    330
    266)
  PixelsPerInch = 96
  TextHeight = 13
  object lbLevels: TListBox
    Left = 8
    Top = 8
    Width = 215
    Height = 250
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 239
    Top = 233
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object btnOK: TButton
    Left = 239
    Top = 202
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    TabOrder = 4
    OnClick = btnOKClick
  end
  object btnMoveUp: TButton
    Left = 239
    Top = 8
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Move Up'
    TabOrder = 1
    OnClick = btnMoveUpClick
  end
  object btnMoveDown: TButton
    Left = 239
    Top = 40
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Move Down'
    TabOrder = 2
    OnClick = btnMoveDownClick
  end
  object btnDelete: TButton
    Left = 239
    Top = 72
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Delete'
    TabOrder = 3
    OnClick = btnDeleteClick
  end
  object btnSwap: TButton
    Left = 240
    Top = 104
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Swap'
    TabOrder = 6
    OnClick = btnSwapClick
  end
  object btnStyleCount: TButton
    Left = 240
    Top = 152
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Style Count'
    TabOrder = 7
    OnClick = btnStyleCountClick
  end
end
