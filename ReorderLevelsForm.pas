unit ReorderLevelsForm;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  L3DLevel, Misc,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Windows, UITypes;

type

  { TFReorderLevels }

  TFReorderLevels = class(TForm)
    btnCancel: TButton;
    btnMoveDown: TButton;
    btnDelete: TButton;
    btnOK: TButton;
    btnMoveUp: TButton;
    btnSwap: TButton;
    btnStyleCount: TButton;
    lbLevels: TListBox;
    procedure btnDeleteClick(Sender: TObject);
    procedure btnMoveDownClick(Sender: TObject);
    procedure btnMoveUpClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnStyleCountClick(Sender: TObject);
    procedure btnSwapClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    fOrigOrderNames: TStringList;
    fHasDeleted: Boolean;

    fStyleList: TStrings;

    procedure RegenerateList;
  public
    procedure SetStyleList(aStrings: TStrings);
  end;

implementation

{$R *.dfm}

{ TFReorderLevels }

procedure TFReorderLevels.FormCreate(Sender: TObject);
var
  i: Integer;
  Level: TL3DLevel;
begin
  fOrigOrderNames := TStringList.Create;

  Level := TL3DLevel.Create;
  try
     for i := 0 to 99 do
     begin
       Level.LoadFromFile(AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(i, 3));
       fOrigOrderNames.Add(Level.Title);
       lbLevels.AddItem('', TObject(i));
     end;
  finally
    Level.Free;
  end;

  fOrigOrderNames.Add('(PLACEHOLDER)');

  RegenerateList;
end;

procedure TFReorderLevels.btnOKClick(Sender: TObject);
var
  i: Integer;
  Level: TL3DLevel;
begin
  if fHasDeleted then
    if MessageDlg('You have chosen to delete some levels. If you continue, these levels will be lost. Continue?', mtCustom, [mbYes, mbNo], 0) = mrNo then
      Exit;

  Level := TL3DLevel.Create;
  try
    Level.Title := '(PLACEHOLDER)';

    for i := 0 to 99 do
    begin
      RenameFile(AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(i, 3), AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(i, 3) + '.TMP');
      RenameFile(AppPath + 'LEVELS' + PathDelim + 'BLK.' + LeadZeroStr(i, 3), AppPath + 'LEVELS' + PathDelim + 'BLK.' + LeadZeroStr(i, 3) + '.TMP');
    end;

    for i := 0 to 99 do
      if Integer(lbLevels.Items.Objects[i]) >= 100 then
        Level.SaveToFile(AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(i, 3))
      else begin
        RenameFile(AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(Integer(lbLevels.Items.Objects[i]), 3) + '.TMP', AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(i, 3));
        RenameFile(AppPath + 'LEVELS' + PathDelim + 'BLK.' + LeadZeroStr(Integer(lbLevels.Items.Objects[i]), 3) + '.TMP', AppPath + 'LEVELS' + PathDelim + 'BLK.' + LeadZeroStr(i, 3));
      end;

    for i := 0 to 99 do
    begin
      if FileExists(AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(i, 3) + '.TMP') then
        DeleteFile(PWideChar(AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(i, 3) + '.TMP'));
      if FileExists(AppPath + 'LEVELS' + PathDelim + 'BLK.' + LeadZeroStr(i, 3) + '.TMP') then
        DeleteFile(PWideChar(AppPath + 'LEVELS' + PathDelim + 'BLK.' + LeadZeroStr(i, 3) + '.TMP'));
    end;
  finally
    Level.Free;
  end;

  ModalResult := mrOk;
end;

procedure TFReorderLevels.btnStyleCountClick(Sender: TObject);
var
  i, n: Integer;
  Counts: array of Integer;
  MusicCounts: array of array of Integer;
  Level: TL3DLevel;
  ThisLabel: String;
  MsgText: String;
begin
  SetLength(Counts, 256); // Using 256 = prepared in case the possibility of adding extra styles arises in the future
  SetLength(MusicCounts, 256, 3); // More than 3 musics is not so likely. We'll deal with it if or when it happens.
  Level := TL3DLevel.Create;
  try
    for i := 0 to 99 do
    begin
      if (lbLevels.SelCount > 0) and not lbLevels.Selected[i] then Continue;

      if lbLevels.Items.Objects[i] = TObject(100) then
        Inc(Counts[0])
      else begin
        Level.LoadFromFile(AppPath + 'LEVELS' + PathDelim + 'LEVEL.' + LeadZeroStr(Integer(lbLevels.Items.Objects[i]), 3));
        Inc(Counts[Level.Style]);
        if Level.Style > 0 then
          Inc(MusicCounts[Level.Style, Level.Music]);
      end;
    end;

    MsgText := '';

    for i := 0 to 255 do
    begin
      if Counts[i] = 0 then Continue;

      ThisLabel := 'Unknown (0x' + IntToHex(i, 2) + ')';
      for n := 0 to fStyleList.Count-1 do
      begin
        if fStyleList.Objects[n] = TObject(i) then
        begin
          ThisLabel := fStyleList[n];
          Break;
        end;
      end;

      MsgText := MsgText + ThisLabel + ': ' + IntToStr(Counts[i]);
      if i > 0 then
      begin
        MsgText := MsgText + ' (';
        MsgText := MsgText + IntToStr(MusicCounts[i, 0]) + ' S, ';
        MsgText := MsgText + IntToStr(MusicCounts[i, 1]) + ' A, ';
        MsgText := MsgText + IntToStr(MusicCounts[i, 2]) + ' R';
        MsgText := MsgText + ')';
      end;
      MsgText := MsgText + #10;
    end;

    ShowMessage(MsgText);
  finally
    Level.Free;
  end;
end;

procedure TFReorderLevels.btnSwapClick(Sender: TObject);
var
  i: Integer;

  Index1, Index2: Integer;
begin
  if lbLevels.SelCount <> 2 then
  begin
    ShowMessage('Please select exactly two levels to swap.');
    Exit;
  end;

  Index1 := -1;
  Index2 := -1;

  for i := 0 to 99 do
    if lbLevels.Selected[i] then
    begin
      Index1 := i;
      Break;
    end;

  for i := Index1+1 to 99 do
    if lbLevels.Selected[i] then
    begin
      Index2 := i;
      Break;
    end;

  if Index2 < 0 then Exit; // should never happen due to above check, but just in case

  lbLevels.Items.Move(Index2, Index1);
  lbLevels.Items.Move(Index1 + 1, Index2);

  RegenerateList;

  lbLevels.Selected[Index1] := true;
  lbLevels.Selected[Index2] := true;
end;

procedure TFReorderLevels.btnMoveUpClick(Sender: TObject);
var
  i: Integer;
  Started: Boolean;
  NewSelected: array[0..99] of Boolean;
begin
  if lbLevels.SelCount = 0 then Exit;
  Started := false;
  FillChar(NewSelected{%H-}, SizeOf(NewSelected), 0);
  for i := 0 to 99 do
    if Started and (lbLevels.Selected[i]) then
    begin
      lbLevels.Items.Move(i, i-1);
      NewSelected[i-1] := true;
    end else if (not Started) and not (lbLevels.Selected[i]) then
      Started := true
    else if (not Started) and (lbLevels.Selected[i]) then
      NewSelected[i] := true;

  RegenerateList;

  for i := 0 to 99 do
    lbLevels.Selected[i] := NewSelected[i];
end;

procedure TFReorderLevels.btnMoveDownClick(Sender: TObject);
var
  i: Integer;
  Started: Boolean;
  NewSelected: array[0..99] of Boolean;
begin
  if lbLevels.SelCount = 0 then Exit;
  Started := false;
  FillChar(NewSelected{%H-}, SizeOf(NewSelected), 0);
  for i := 99 downto 0 do
    if Started and (lbLevels.Selected[i]) then
    begin
      lbLevels.Items.Move(i, i+1);
      NewSelected[i+1] := true;
    end else if (not Started) and not (lbLevels.Selected[i]) then
      Started := true
    else if (not Started) and (lbLevels.Selected[i]) then
      NewSelected[i] := true;

  RegenerateList;

  for i := 0 to 99 do
    lbLevels.Selected[i] := NewSelected[i];
end;

procedure TFReorderLevels.btnDeleteClick(Sender: TObject);
var
  i: Integer;
begin
  if lbLevels.SelCount = 0 then Exit;
  fHasDeleted := true;
  for i := 0 to 99 do
    if lbLevels.Selected[i] then
      lbLevels.Items.Objects[i] := TObject(100);
  RegenerateList;
end;

procedure TFReorderLevels.FormDestroy(Sender: TObject);
begin
  fOrigOrderNames.Free;
end;

procedure TFReorderLevels.RegenerateList;
var
  i: Integer;
begin
  lbLevels.Items.BeginUpdate;
  try
    for i := 0 to 99 do
      lbLevels.Items[i] := LeadZeroStr(i + 1, 2) + ': ' + fOrigOrderNames[Integer(lbLevels.Items.Objects[i])];
  finally
    lbLevels.Items.EndUpdate;
  end;
end;

procedure TFReorderLevels.SetStyleList(aStrings: TStrings);
begin
  fStyleList := aStrings;
end;

end.

