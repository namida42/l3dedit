unit L3DRender;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  IndexedGraphic,
  BitmapCache,
  GR32, GR32_Transforms, GR32_Polygons, GR32_Resamplers,
  L3DLevel,
  Misc,
  Classes, Math, Types, SysUtils;

const
  VIEW_PERSPECTIVES: array[0..8] of Single =
    ( 0.1875, 0.25, 1/3, 0.5, 0.75, 1, 1.25, 1.5, 2 );
  VIEW_SKEWS: array[0..4] of Single =
    ( -0.5, -0.25, 0, 0.25, 0.5 );
  VIEW_RESOLUTIONS: array[0..4] of Integer =
    ( 24, 36, 48, 64, 72 );

type
  TRenderParams = record
    InitialPosition: TPoint;
    TopCornerPosition: TPoint;
    DstWidth: Integer;
    DstHeight: Integer;
    NextXHorzOffset: Integer;
    NextXVertOffset: Integer;
    NextYHorzOffset: Integer;
    NextYVertOffset: Integer;
    NextZHorzOffset: Integer;
    NextZVertOffset: Integer;
    BlockTopPolygon: TPolygon;
    SelectionBottom: TBitmap32;
    SelectionTop: TBitmap32;
    SelectionTopFace: TBitmap32;
    SelectionTopCorners: TBitmap32;
  end;

  TRenderSetting = (rsBackFaces, rsNoInvisibles);
  TRenderSettings = set of TRenderSetting;

  TRenderPieceType = (rptSeaSky, rptLand, rptBlocks, rptObjects, rptCameras);
  TRenderPieceTypes = set of TRenderPieceType;

  TAxis = (AxisX, AxisY, AxisZ);

  // The first eight are guaranteed to remain exactly as is. Any beyond that, will
  // remain the same order within a line, but the lines themself may be reordered.
  TRenderMask = (rmCutSlice0, rmCutSlice1, rmCutSlice2, rmCutSlice3,                                      // Slices
                 rmCutHorzSlice0, rmCutHorzSlice1, rmCutHorzSlice2, rmCutHorzSlice3,                      // Horizontal slices, sometimes used in corner pieces
                 rmCut45TopLeft, rmCut45TopRight, rmCut45BottomRight, rmCut45BottomLeft,                  // 45 degree triangle cuts
                 rmCutToPyramidUp, rmCutToPyramidDown,                                                    // Pyramid shapes
                 rmCut45TopLeft1, rmCut45TopLeft2, rmCut45TopLeft3,                                       // For sliced corner triangles top/bottom faces
                 rmCut45BottomRight1, rmCut45BottomRight2, rmCut45BottomRight3,                           // Same. We don't need the other two orientations
                 rmCut225TopLeft0, rmCut225TopLeft1, rmCut225TopLeft2, rmCut225TopLeft3,                  // 22.5 degree slopes
                 rmCut225TopRight0, rmCut225TopRight1, rmCut225TopRight2, rmCut225TopRight3,              //  "
                 rmCut225BottomRight0, rmCut225BottomRight1, rmCut225BottomRight2, rmCut225BottomRight3,  //  "
                 rmCut225BottomLeft0, rmCut225BottomLeft1, rmCut225BottomLeft2, rmCut225BottomLeft3       //  "
                 );
  TRenderMasks = set of TRenderMask;

  TGridStyle = (gsStandard, gsCorners, gsNone);

  { TL3DRenderer }

  TL3DRenderer = class
    private
      BlockRenderWidth: Integer;
      BlockRenderHeight: Integer;

      Cache: TBitmapCache;
      TextureMasks: array[TRenderMask] of TBitmap32;

      fPairingIcons: TBitmap32;

      fRenderParams: TRenderParams;

      fTextureSet: TIndexedGraphic;
      fSeaGraphic: TIndexedGraphic;
      fSkyGraphic: TIndexedGraphic;
      fLandGraphic: TIndexedGraphic;

      fObjectSet: TIndexedGraphic;
      fSignSet: TIndexedGraphic;
      fAnimObjGraphic: TIndexedGraphic;
      fTrapGraphic: TIndexedGraphic;
      fWallSet: TIndexedGraphic;

      fCameraGraphic: TBitmap32;

      fViewRotation: Integer;
      fViewPerspective: Single;
      fViewSkew: Single;

      fTransform: TProjectiveTransformation;
      fTransformBitmapA, fTransformBitmapB: TBitmap32;

      fRenderRangeStart, fRenderRangeEnd: T3DPoint;

      fInvisibleReveal: TBitmap32;
      fHighlightShade: TColor32;

      procedure CombineMask(F: TColor32; var B: TColor32; {%H-}M: TColor32);
      procedure CombineInverseMask(F: TColor32; var B: TColor32; {%H-}M: TColor32);
      procedure CombineHighlight(F: TColor32; var B: TColor32; {%H-}M: TColor32);

      procedure DrawTransformed(Dst: TBitmap32; Src: TBitmap32; V0, V1, V2, V3: TFloatPoint; Shade: Byte = $FF); overload;
      procedure DrawTransformed(Dst: TBitmap32; Masks: TRenderMasks; Src: TBitmap32; V0, V1, V2, V3: TFloatPoint; Shade: Byte = $FF); overload;
      procedure DrawTransformed(Dst: TBitmap32; Src: TBitmap32; Polygon: TPolygon; Shade: Byte = $FF); overload;
      procedure DrawTransformed(Dst: TBitmap32; Masks: TRenderMasks; Src: TBitmap32; Polygon: TPolygon; Shade: Byte = $FF); overload;

      procedure GenerateMasks;

      procedure EnsureFaceGraphics(MetaBlock: TL3DMetaBlock);
      procedure EnsureTextureSet(Index: Integer; aLevel: TL3DLevel = nil);
      function GetTextureSetBitmap: TBitmap32;
      procedure SetViewRotation(const aValue: Integer);

      function GetViewPerspective: Integer;
      procedure SetViewPerspective(aValue: Integer);
      function GetViewSkew: Integer;
      procedure SetViewSkew(aValue: Integer);
      function GetViewResolution: Integer;
      procedure SetViewResolution(aValue: Integer);

      procedure CalculateRenderParams;
      procedure GenerateInvisibleReveal;
    public
      RenderSettings: TRenderSettings;
      RenderPieceTypes: TRenderPieceTypes;
      GridStyle: TGridStyle;

      constructor Create;
      destructor Destroy; override;

      procedure PurgeCache;

      procedure DrawObjectForInterface(Dst: TBitmap32; Level: TL3DLevel; Obj: TL3DObject);
      function RenderBlock(Dst: TBitmap32; Block: TL3DBlock; Level: TL3DLevel; TopFaceOnly: Boolean; IgnoreViewRotation: Boolean = false): Boolean;
      procedure RenderLevel(Dst: TBitmap32; Level: TL3DLevel); overload;
      procedure RenderLevel(Dst: TBitmap32; Level: TL3DLevel; HighlightAxis: TAxis;
        HighlightRank: Integer; HighlightBlock: T3DPoint; MarkBlock: T3DPoint; CornerMode: Boolean;
        HighlightLandIndex: Integer); overload;

      function CalculateBlockFromImagePos(ImagePos: TPoint; LockAxis: TAxis; LockRank: Integer; SelectingCorner: Boolean): T3DPoint;
      function ViewCoordsToDataCoords(aValue: T3DPoint): T3DPoint;

      property ViewResolution: Integer read GetViewResolution write SetViewResolution;
      property ViewRotation: Integer read fViewRotation write SetViewRotation;
      property ViewPerspective: Integer read GetViewPerspective write SetViewPerspective;
      property ViewSkew: Integer read GetViewSkew write SetViewSkew;

      property RenderRangeStart: T3DPoint read fRenderRangeStart write fRenderRangeStart;
      property RenderRangeEnd: T3DPoint read fRenderRangeEnd write fRenderRangeEnd;

      property TextureSetBitmap: TBitmap32 read GetTextureSetBitmap;
  end;

implementation

{ TL3DRenderer }       

constructor TL3DRenderer.Create;
var
  Mask: TRenderMask;
  MS: TMemoryStream;

  procedure LoadBitmapFromResource(aDst: TBitmap32; ResName, ResType: String; Transparent: Boolean);
  var
    RS: TResourceStream;
    PixPtr: PColor32;
    i: Integer;
  begin
    // I have no idea why, but TBitmap32's don't load correctly from TResourceStream.
    // So instead, we load that to a TMemoryStream, from which the TBitmap32 loads.
    // It's assumed that the TMemoryStream has already been created.
    RS := TResourceStream.Create(HInstance, ResName, PChar(ResType));
    try
      MS.LoadFromStream(RS);
      MS.Position := 0;
      aDst.LoadFromStream(MS);
    finally
      RS.Free;
    end;

    if not Transparent then Exit;

    PixPtr := aDst.PixelPtr[0, 0];
    for i := 0 to (aDst.Width * aDst.Height)-1 do
    begin
      if (PixPtr^ and $FFFFFF) = 0 then
        PixPtr^ := $00000000;
      Inc(PixPtr);
    end;
  end;

begin
  inherited;

  Cache := TBitmapCache.Create;

  fRenderParams.SelectionTop := TBitmap32.Create;
  fRenderParams.SelectionTopFace := TBitmap32.Create;
  fRenderParams.SelectionTopCorners := TBitmap32.Create;
  fRenderParams.SelectionBottom := TBitmap32.Create;

  for Mask := Low(TRenderMask) to High(TRenderMask) do
  begin
    TextureMasks[Mask] := TBitmap32.Create;
    TextureMasks[Mask].SetSize(64, 64);
    TextureMasks[Mask].Clear($00000000);
    TextureMasks[Mask].DrawMode := dmTransparent;
  end;

  GenerateMasks;

  for Mask := Low(TRenderMask) to High(TRenderMask) do
  begin
    TextureMasks[Mask].DrawMode := dmCustom;
    TextureMasks[Mask].OnPixelCombine := CombineMask;
  end;

  fTransform := TProjectiveTransformation.Create;
  fTransformBitmapA := TBitmap32.Create;
  fTransformBitmapA.DrawMode := dmTransparent;
  fTransformBitmapB := TBitmap32.Create;
  fTransformBitmapB.DrawMode := dmTransparent;

  fViewPerspective := 0.5;
  fViewSkew := 0;
  ViewResolution := Min(Length(VIEW_RESOLUTIONS) - 1, 1);

  fTextureSet := TIndexedGraphic.Create('TEXTURE', 64);
  fTextureSet.Image.DrawMode := dmTransparent;

  fSeaGraphic := TIndexedGraphic.Create('SEA', 64);
  fSkyGraphic := TIndexedGraphic.Create('SKY', 1024);
  fLandGraphic := TIndexedGraphic.Create('LAND', 128);

  fObjectSet := TIndexedGraphic.Create('OBJ', 64);
  fSignSet := TIndexedGraphic.Create('SIGNS', 64);
  fAnimObjGraphic := TIndexedGraphic.Create('ANIMOBJ', 64);
  fTrapGraphic := TIndexedGraphic.Create('TRAPS', 64);
  fWallSet := TIndexedGraphic.Create('WALLS', 64);
  fObjectSet.Image.DrawMode := dmTransparent;
  fSignSet.Image.DrawMode := dmTransparent;
  fAnimObjGraphic.Image.DrawMode := dmTransparent;
  fTrapGraphic.Image.DrawMode := dmTransparent;
  fWallSet.Image.DrawMode := dmTransparent;

  fRenderRangeEnd := Make3DPoint(32, 16, 32);

  RenderPieceTypes := [rptSeaSky, rptLand, rptBlocks, rptObjects, rptCameras];

  fPairingIcons := TBitmap32.Create;
  fCameraGraphic := TBitmap32.Create;

  MS := TMemoryStream.Create;
  try
    LoadBitmapFromResource(fPairingIcons, 'UI', 'PAIRING', false);
    LoadBitmapFromResource(fCameraGraphic, 'UI', 'CAMERA', true);
  finally
    MS.Free;
  end;
  fCameraGraphic.DrawMode := dmTransparent;

  fInvisibleReveal := TBitmap32.Create;
  GenerateInvisibleReveal;

  fRenderParams.SelectionTop.DrawMode := dmCustom;
  fRenderParams.SelectionBottom.DrawMode := dmCustom;
  fRenderParams.SelectionTopFace.DrawMode := dmCustom;
  fRenderParams.SelectionTopCorners.DrawMode := dmCustom;
  fRenderParams.SelectionTop.OnPixelCombine := CombineHighlight;
  fRenderParams.SelectionBottom.OnPixelCombine := CombineHighlight;
  fRenderParams.SelectionTopFace.OnPixelCombine := CombineHighlight;
  fRenderParams.SelectionTopCorners.OnPixelCombine := CombineHighlight;
  //CalculateRenderParams; // This already happens when we set the view resolution above.
end;

destructor TL3DRenderer.Destroy;
var
  Mask: TRenderMask;
begin
  fTransform.Free;
  fTransformBitmapA.Free;
  fTransformBitmapB.Free;

  fTextureSet.Free;
  fSeaGraphic.Free;
  fSkyGraphic.Free;
  fLandGraphic.Free;
  fObjectSet.Free;
  fSignSet.Free;
  fAnimObjGraphic.Free;
  fTrapGraphic.Free;
  fWallSet.Free;

  fInvisibleReveal.Free;

  Cache.Free;

  for Mask := Low(TRenderMask) to High(TRenderMask) do
    TextureMasks[Mask].Free;

  fPairingIcons.Free;

  fRenderParams.SelectionBottom.Free;
  fRenderParams.SelectionTop.Free;
  fRenderParams.SelectionTopFace.Free;
  fRenderParams.SelectionTopCorners.Free;

  inherited Destroy;
end;

procedure TL3DRenderer.PurgeCache;
begin
  Cache.Purge;
end;

procedure TL3DRenderer.DrawObjectForInterface(Dst: TBitmap32; Level: TL3DLevel; Obj: TL3DObject);
var
  Src, Temp: TBitmap32;
  SrcRect, DstRect: TRect;
  DstFace: Integer;
  DrawPolygon: TPolygon;
  BottomPolygon: TPolygon;
  i: Integer;
begin
  DstFace := -1;
  DrawPolygon := fRenderParams.BlockTopPolygon;
  BottomPolygon := DrawPolygon.Shift(0, BlockRenderHeight / 2);

  case Obj.ObjectType of
    otStandard: begin
                  fObjectSet.ChangeIndex(Level.ObjectSet);
                  Src := fObjectSet.Image;
                  case Obj.GraphicIndex of
                    0..3: SrcRect := SizeRect((Obj.GraphicIndex mod 2) * 32, (Obj.GraphicIndex div 2) * 64, 32, 64);
                    4..7: SrcRect := SizeRect(0, 128 + ((Obj.GraphicIndex - 4) * 64), 64, 64);
                    8..11: SrcRect := SizeRect((Obj.GraphicIndex mod 2) * 32, (((Obj.GraphicIndex - 8) div 2) * 64) + 384, 32, 64);
                    12..15: SrcRect := SizeRect(0, 512 + ((Obj.GraphicIndex - 12) * 32), 64, 32);
                    16..19: SrcRect := SizeRect((Obj.GraphicIndex mod 2) * 32, (((Obj.GraphicIndex - 16) div 2) * 32) + 640, 32, 32);
                  end;
                end;
    otSign: begin
              fSignSet.ChangeIndex(Level.Sign);
              Src := fSignSet.Image;
              case Obj.GraphicIndex of
                0..1: SrcRect := SizeRect(0, Obj.GraphicIndex * 32, 64, 32);
                2..3: SrcRect := SizeRect(0, Obj.GraphicIndex * 32 + 64, 64, 32);
                4..7: SrcRect := SizeRect(0, Obj.GraphicIndex * 64, 64, 64);
              end;
              case Obj.Orientation of
                0: DstFace := FACE_X31;
                1: DstFace := FACE_Z0;
                2: DstFace := FACE_X0;
                3: DstFace := FACE_Z31;
              end;

              if Obj.Orientation in [1, 2] then
              begin
                if Obj.GraphicIndex >= 4 then
                  SrcRect := SizeRect(0, SrcRect.Top + 256, 64, SrcRect.Height)
                else
                  SrcRect := SizeRect(0, SrcRect.Top + 64, 64, SrcRect.Height);
              end;

              if Obj.GraphicIndex < 4 then
                DrawPolygon := DrawPolygon.Shift(0, BlockRenderHeight / 4);
            end;
    otAnimation: if Obj.GraphicIndex = 1 then
                 begin
                   fObjectSet.ChangeIndex(Level.ObjectSet);
                   Src := fObjectSet.Image;
                   SrcRect := SizeRect(0, 128, 64, 64);
                 end else begin
                   fAnimObjGraphic.ChangeIndex(Level.AnimatedObject);
                   Src := fAnimObjGraphic.Image;
                   case Obj.Split of
                     osWhole: SrcRect := Rect(0, 0, 64, 64);
                     osLeft: SrcRect := Rect(0, 0, 32, 64);
                     osRight: SrcRect := Rect(32, 0, 64, 64);
                   end;

                   if (Obj.Orientation > 1) or (Obj.Split <> osWhole) then
                     case Obj.Orientation of
                       0: DstFace := FACE_X0;
                       1: DstFace := FACE_Z31;
                       2: DstFace := FACE_X31;
                       3: DstFace := FACE_Z0;
                     end;
                 end;
    otInteractive: begin
                     fTrapGraphic.ChangeIndex(Level.InteractiveObject);
                     Src := fTrapGraphic.Image;
                     case Level.InteractiveObject of
                       0: SrcRect := Rect(0, 0, 64, 32); // Flamethrower
                       1, 2, 6: SrcRect := Rect(0, 0, 64, 64); // Squasher, beartrap, wtftrap
                       3: if Obj.IsBlueTrampoline then // Trampoline
                            SrcRect := Rect(0, 256, 64, 320)
                          else
                            SrcRect := Rect(0, 0, 64, 64);
                       4: if Obj.IsReceiver then // Spring
                            SrcRect := Rect(0, 0, 64, 64)
                          else
                            SrcRect := Rect(0, 64, 64, 128);
                       5: SrcRect := Rect(0, 0, 64, 64); // Teleporter
                       7: SrcRect := Rect(0, 128, 64, 160); // Laser trap
                       8: SrcRect := Rect(0, 0, 64, 64); // Rope slide
                     end;

                     if Level.InteractiveObject in [0, 7] then
                     begin
                       DstFace := FACE_Y0;
                       BottomPolygon := BottomPolygon.RotatePoints(-fViewRotation - Obj.Orientation + 1);
                     end;
                   end;
    otWall: begin
              fWallSet.ChangeIndex(Level.Walls);
              Src := fWallSet.Image;
              SrcRect := SizeRect(0, Obj.GraphicIndex * 64, 64, 64);
              case Obj.Orientation of
                0: DstFace := FACE_X31;
                1: DstFace := FACE_Z0;
                2: DstFace := FACE_X0;
                3: DstFace := FACE_Z31;
                4: DstFace := FACE_Y0;
                5: DstFace := FACE_Y15;
              end;
            end;
    else begin
      Dst.Clear($00000000);
      Exit;
    end;
  end;

  if (DstFace >= 0) and not (DstFace in [FACE_Y0, FACE_Y15]) then
    for i := 1 to fViewRotation do
      case DstFace of
        FACE_Z31: DstFace := FACE_X0;
        FACE_X31: DstFace := FACE_Z31;
        FACE_Z0: DstFace := FACE_X31;
        FACE_X0: DstFace := FACE_Z0;
      end;

  if (DstFace < 0) or (Obj.ObjectType = otAnimation) then
  begin
    if (Dst.Width <> 64) or (Dst.Height <> 64) then
      Dst.SetSize(64, 64);
    Dst.Clear($00000000);

    case DstFace of
      FACE_X0: DstRect := SizeRect(24, 0, 16, 32);
      FACE_Z31: DstRect := SizeRect(8, 16, 16, 32);
      FACE_X31: DstRect := SizeRect(24, 32, 16, 32);
      FACE_Z0: DstRect := SizeRect(40, 16, 16, 32);
      else DstRect := SizeRect(16, 0, 32, 64);
    end;

    if (Obj.ObjectType <> otAnimation) or (Obj.Split = osWhole) then
      DstRect := SizeRect(DstRect.Left - (DstRect.Width div 2), DstRect.Top, DstRect.Width * 2, DstRect.Height);

    Src.DrawTo(Dst, DstRect, SrcRect);
  end else begin
    case DstFace of
      FACE_Y0: DrawPolygon := BottomPolygon;
      FACE_Z31: DrawPolygon := Polygon(DrawPolygon.BottomLeft, DrawPolygon.BottomRight, BottomPolygon.BottomRight, BottomPolygon.BottomLeft);
      FACE_X31: DrawPolygon := Polygon(DrawPolygon.BottomRight, DrawPolygon.TopRight, BottomPolygon.TopRight, BottomPolygon.BottomRight);
      FACE_Z0: DrawPolygon := Polygon(DrawPolygon.TopLeft, DrawPolygon.TopRight, BottomPolygon.TopRight, BottomPolygon.TopLeft);
      FACE_X0: DrawPolygon := Polygon(DrawPolygon.BottomLeft, DrawPolygon.TopLeft, BottomPolygon.TopLeft, BottomPolygon.BottomLeft);
    end;

    Temp := TBitmap32.Create;
    try
      Temp.SetSize(SrcRect.Width, SrcRect.Height);
      Dst.SetSize(BlockRenderWidth - 1, BlockRenderHeight);
      Dst.Clear($00000000);
      Src.DrawTo(Temp, Temp.BoundsRect, SrcRect);
      DrawTransformed(Dst, Temp, DrawPolygon.TopLeft, DrawPolygon.TopRight, DrawPolygon.BottomRight, DrawPolygon.BottomLeft);
    finally
      Temp.Free;
    end;
  end;
end;

procedure TL3DRenderer.CombineMask(F: TColor32; var B: TColor32; M: TColor32);
begin
  if F <> 0 then B := 0;
end;

procedure TL3DRenderer.CombineInverseMask(F: TColor32; var B: TColor32;
  M: TColor32);
begin
  if F = 0 then B := 0;
end;

procedure TL3DRenderer.CombineHighlight(F: TColor32; var B: TColor32;
  M: TColor32);
begin
  if (F and $80000000) <> 0 then B := fHighlightShade;
end;

procedure TL3DRenderer.DrawTransformed(Dst: TBitmap32; Src: TBitmap32;
  V0, V1, V2, V3: TFloatPoint; Shade: Byte = $FF);
begin
  DrawTransformed(Dst, [], Src, V0, V1, V2, V3, Shade);
end;

procedure TL3DRenderer.DrawTransformed(Dst: TBitmap32; Masks: TRenderMasks; Src: TBitmap32;
  V0, V1, V2, V3: TFloatPoint; Shade: Byte = $FF);
var
  NewW, NewH: Integer;
  Mask: TRenderMask;
  DrawSrc: TBitmap32;

  OldDrawMode: TDrawMode;
begin

  OldDrawMode := Src.DrawMode;
  try
    Src.DrawMode := dmOpaque;

    fTransform.X0 := V0.X;
    fTransform.X1 := V1.X;
    fTransform.X2 := V2.X;
    fTransform.X3 := V3.X;
    fTransform.Y0 := V0.Y;
    fTransform.Y1 := V1.Y;
    fTransform.Y2 := V2.Y;
    fTransform.Y3 := V3.Y;
    fTransform.SrcRect := FloatRect(0, 0, Src.Width, Src.Height);

    NewW := Ceil(fTransform.X0);
    NewW := Math.Max(Ceil(fTransform.X1), NewW);
    NewW := Math.Max(Ceil(fTransform.X2), NewW);
    NewW := Math.Max(Ceil(fTransform.X3), NewW);

    NewH := Ceil(fTransform.Y0);
    NewH := Math.Max(Ceil(fTransform.Y1), NewH);
    NewH := Math.Max(Ceil(fTransform.Y2), NewH);
    NewH := Math.Max(Ceil(fTransform.Y3), NewH);

    if (NewW = 0) or (NewH = 0) or (Src.Width = 0) or (Src.Height = 0) then Exit;

    if (Masks <> []) or (Shade < $FF) then
    begin
      // Copy Src's pixels to fTransformBitmapA, without copying properties (other than size)
      if (fTransformBitmapA.Width <> Src.Width) or (fTransformBitmapA.Height <> Src.Height) then
        fTransformBitmapA.SetSize(Src.Width, Src.Height);

      Src.DrawTo(fTransformBitmapA, 0, 0);

      if Masks <> [] then
        for Mask := Low(TRenderMask) to High(TRenderMask) do
          if Mask in Masks then
            TextureMasks[Mask].DrawTo(fTransformBitmapA);

      ApplyShade(fTransformBitmapA, Shade);

      DrawSrc := fTransformBitmapA;
    end else
      DrawSrc := Src;

    if (fTransformBitmapB.Width <> NewW) or (fTransformBitmapB.Height <> NewH) then
      fTransformBitmapB.SetSize(NewW, NewH);
    fTransformBitmapB.Clear(0);

    Transform(fTransformBitmapB, DrawSrc, fTransform);

    fTransformBitmapB.DrawTo(Dst, 0, 0);
  finally
    Src.DrawMode := OldDrawMode;
  end;
end;

procedure TL3DRenderer.DrawTransformed(Dst: TBitmap32; Src: TBitmap32;
  Polygon: TPolygon; Shade: Byte = $FF);
begin
  DrawTransformed(Dst, Src, Polygon.TopLeft, Polygon.TopRight, Polygon.BottomRight, Polygon.BottomLeft, Shade);
end;

procedure TL3DRenderer.DrawTransformed(Dst: TBitmap32; Masks: TRenderMasks;
  Src: TBitmap32; Polygon: TPolygon; Shade: Byte = $FF);
begin
  DrawTransformed(Dst, Masks, Src, Polygon.TopLeft, Polygon.TopRight, Polygon.BottomRight, Polygon.BottomLeft, Shade);
end;

procedure TL3DRenderer.GenerateMasks;
var
  x, y: Integer;
  i: Integer;
  TL, TR, BR, BL: TRenderMask;
begin
  TextureMasks[rmCutSlice0].FillRect(0,  0, 64, 16, $FFFFFFFF);
  TextureMasks[rmCutSlice1].FillRect(0, 16, 64, 32, $FFFFFFFF);
  TextureMasks[rmCutSlice2].FillRect(0, 32, 64, 48, $FFFFFFFF);
  TextureMasks[rmCutSlice3].FillRect(0, 48, 64, 64, $FFFFFFFF);

  TextureMasks[rmCutHorzSlice0].FillRect(0,  0, 16, 64, $FFFFFFFF);
  TextureMasks[rmCutHorzSlice1].FillRect(16, 0, 32, 64, $FFFFFFFF);
  TextureMasks[rmCutHorzSlice2].FillRect(32, 0, 48, 64, $FFFFFFFF);
  TextureMasks[rmCutHorzSlice3].FillRect(48, 0, 64, 64, $FFFFFFFF);

  for y := 0 to 63 do
    for x := 0 to 63 do
    begin
      if x < 63 - y then TextureMasks[rmCut45TopLeft][x, y]     := $FFFFFFFF;
      if x > y      then TextureMasks[rmCut45TopRight][x, y]    := $FFFFFFFF;
      if x < y      then TextureMasks[rmCut45BottomLeft][x, y]  := $FFFFFFFF;
      if x > 63 - y then TextureMasks[rmCut45BottomRight][x, y] := $FFFFFFFF;

      if x < (63 - y) div 2 then TextureMasks[rmCutToPyramidUp][x, y] := $FFFFFFFF;
      if x > (y div 2) + 32 then TextureMasks[rmCutToPyramidUp][x, y] := $FFFFFFFF;

      if x < y div 2 then TextureMasks[rmCutToPyramidDown][x, y] := $FFFFFFFF;
      if x > ((63 - y) div 2) + 32 then TextureMasks[rmCutToPyramidDown][x, y] := $FFFFFFFF;

      if x < 63 - (y * 2) then TextureMasks[rmCut225TopLeft0][x, y] := $FFFFFFFF;
    end;

  TextureMasks[rmCut45TopLeft1].FillRect(0, 0, 64, 64, $FFFFFFFF);
  TextureMasks[rmCut45TopLeft1].FillRect(8, 8, 64, 64, $00000000);
  TextureMasks[rmCut45TopLeft].DrawTo(TextureMasks[rmCut45TopLeft1], 8, 8);

  TextureMasks[rmCut45TopLeft2].FillRect(0, 0, 64, 64, $FFFFFFFF);
  TextureMasks[rmCut45TopLeft2].FillRect(16, 16, 64, 64, $00000000);
  TextureMasks[rmCut45TopLeft].DrawTo(TextureMasks[rmCut45TopLeft2], 16, 16);

  TextureMasks[rmCut45TopLeft3].FillRect(0, 0, 64, 64, $FFFFFFFF);
  TextureMasks[rmCut45TopLeft3].FillRect(24, 24, 64, 64, $00000000);
  TextureMasks[rmCut45TopLeft].DrawTo(TextureMasks[rmCut45TopLeft3], 24, 24);

  TextureMasks[rmCut45BottomRight1].FillRect(0, 0, 64, 64, $FFFFFFFF);
  TextureMasks[rmCut45BottomRight1].FillRect(0, 0, 56, 56, $00000000);
  TextureMasks[rmCut45BottomRight].DrawTo(TextureMasks[rmCut45BottomRight1], -8, -8);

  TextureMasks[rmCut45BottomRight2].FillRect(0, 0, 64, 64, $FFFFFFFF);
  TextureMasks[rmCut45BottomRight2].FillRect(0, 0, 48, 48, $00000000);
  TextureMasks[rmCut45BottomRight].DrawTo(TextureMasks[rmCut45BottomRight2], -16, -16);

  TextureMasks[rmCut45BottomRight3].FillRect(0, 0, 64, 64, $FFFFFFFF);
  TextureMasks[rmCut45BottomRight3].FillRect(0, 0, 40, 40, $00000000);
  TextureMasks[rmCut45BottomRight].DrawTo(TextureMasks[rmCut45BottomRight3], -24, -24);

  TextureMasks[rmCut225TopLeft1].FillRect(0, 0, 64, 16, $FFFFFFFF);
  TextureMasks[rmCut225TopLeft0].DrawTo(TextureMasks[rmCut225TopLeft1], 0, 16);

  TextureMasks[rmCut225TopLeft2].FillRect(0, 0, 64, 32, $FFFFFFFF);
  TextureMasks[rmCut225TopLeft0].DrawTo(TextureMasks[rmCut225TopLeft2], 0, 32);

  TextureMasks[rmCut225TopLeft3].FillRect(0, 0, 64, 48, $FFFFFFFF);
  TextureMasks[rmCut225TopLeft0].DrawTo(TextureMasks[rmCut225TopLeft3], 0, 48);

  for i := 0 to 3 do
  begin
    TL := TRenderMask(Integer(rmCut225TopLeft0) + i);
    TR := TRenderMask(Integer(rmCut225TopRight0) + i);
    BR := TRenderMask(Integer(rmCut225BottomRight0) + i);
    BL := TRenderMask(Integer(rmCut225BottomLeft0) + i);

    TextureMasks[TR].Assign(TextureMasks[TL]);
    TextureMasks[TR].FlipHorz;

    TextureMasks[BR].Assign(TextureMasks[TL]);
    TextureMasks[BR].Rotate180;

    TextureMasks[BL].Assign(TextureMasks[TL]);
    TextureMasks[BL].FlipVert;
  end;
end;

procedure TL3DRenderer.EnsureFaceGraphics(MetaBlock: TL3DMetaBlock);
var
  i: Integer;
  SrcRect: TRect;
  LocalTextureIndex: Integer;
begin
  if MetaBlock.Faces[0].Texture <> nil then Exit;

  for i := 0 to 5 do
  begin
    MetaBlock.Faces[i].Texture := TBitmap32.Create;
    MetaBlock.Faces[i].Texture.SetSize(64, 64);

    if ((mbfTransparent in MetaBlock.Faces[i].Modifiers) or (mbfAltTextures in MetaBlock.Faces[i].Modifiers)) then
      MetaBlock.Faces[i].Texture.Clear($00000000)
    else
      MetaBlock.Faces[i].Texture.Clear($FF000000);

    LocalTextureIndex := MetaBlock.Faces[i].DisplayTextureIndex;

    SrcRect := SizeRect(0, LocalTextureIndex * 64, 64, 64);
    if (SrcRect.Top >= 0) and (SrcRect.Bottom <= fTextureSet.Image.Height) then
      fTextureSet.Image.DrawTo(MetaBlock.Faces[i].Texture, 0, 0, SrcRect)
    else if (rsNoInvisibles in RenderSettings) then
      fInvisibleReveal.DrawTo(MetaBlock.Faces[i].Texture, 0, 0);
  end;
end;

procedure TL3DRenderer.EnsureTextureSet(Index: Integer; aLevel: TL3DLevel = nil);
begin
  if not fTextureSet.ChangeIndex(Index) then Exit;

  Cache.Purge;

  if aLevel <> nil then
    aLevel.PurgeBlockFaceGraphics;
end;

function TL3DRenderer.GetTextureSetBitmap: TBitmap32;
begin
  Result := fTextureSet.Image;
end;

procedure TL3DRenderer.SetViewRotation(const aValue: Integer);
begin
  fViewRotation := aValue mod 4;
  if fViewRotation < 0 then
    Inc(fViewRotation, 4);
  CalculateRenderParams;
end;

function TL3DRenderer.GetViewPerspective: Integer;
begin
  for Result := 0 to Length(VIEW_PERSPECTIVES)-1 do
    if VIEW_PERSPECTIVES[Result] = fViewPerspective then Exit;

  Result := -1;
end;

procedure TL3DRenderer.SetViewPerspective(aValue: Integer);
begin
  aValue := Min(Max(0, aValue), Length(VIEW_PERSPECTIVES) - 1);

  if fViewPerspective = VIEW_PERSPECTIVES[aValue] then Exit;

  fViewPerspective := VIEW_PERSPECTIVES[aValue];

  CalculateRenderParams;
end;

function TL3DRenderer.GetViewSkew: Integer;
begin
  for Result := 0 to Length(VIEW_SKEWS)-1 do
    if VIEW_SKEWS[Result] = fViewSkew then Exit;

  Result := -1;
end;

procedure TL3DRenderer.SetViewSkew(aValue: Integer);
begin
  aValue := Min(Max(0, aValue), Length(VIEW_SKEWS) - 1);

  if fViewSkew = VIEW_SKEWS[aValue] then Exit;

  fViewSkew := VIEW_SKEWS[aValue];

  CalculateRenderParams;
end;

function TL3DRenderer.GetViewResolution: Integer;
begin
  for Result := 0 to Length(VIEW_RESOLUTIONS)-1 do
    if VIEW_RESOLUTIONS[Result] = BlockRenderWidth then Exit;

  Result := -1;
end;

procedure TL3DRenderer.SetViewResolution(aValue: Integer);
begin
  aValue := Min(Max(0, aValue), Length(VIEW_RESOLUTIONS) - 1);

  if BlockRenderWidth = VIEW_RESOLUTIONS[aValue] then Exit;

  BlockRenderWidth := VIEW_RESOLUTIONS[aValue];
  BlockRenderHeight := BlockRenderWidth * 4 div 3;

  PurgeCache;
  CalculateRenderParams;
end;

function TL3DRenderer.ViewCoordsToDataCoords(aValue: T3DPoint): T3DPoint;
begin
  Result.Y := aValue.Y;

  case fViewRotation of
    0: begin Result.X := aValue.X; Result.Z := aValue.Z; end;
    1: begin Result.X := aValue.Z; Result.Z := 31 - aValue.X; end;
    2: begin Result.X := 31 - aValue.X; Result.Z := 31 - aValue.Z; end;
    3: begin Result.X := 31 - aValue.Z; Result.Z := aValue.X; end;
  end;
end;

// This procedure is huge and messy. I think I can simplify it a lot, but I don't
// really have the patience to do so right now.
function TL3DRenderer.RenderBlock(Dst: TBitmap32; Block: TL3DBlock;
  Level: TL3DLevel; TopFaceOnly: Boolean; IgnoreViewRotation: Boolean = false): Boolean;
type
  TFaceRenderInfo = record
    DataFace: Integer;
    Polygon: TPolygon;
    Rotation: Integer;
    Masks: TRenderMasks;
    BlankingState: (bsUnset, bsBlank, bsShow);
    ShadeMod: Single;
    // Face can be blanked out two ways:
    //  - Set BlankingState to bsBlank. This will be overridden by "draw both sides" face flag once support is implemented.
    //  - Set DataFace to -1. This will apply no matter what.
  end;

var
  MetaBlock: TL3DMetaBlock;

  FaceRenderInfo: array[0..5] of TFaceRenderInfo; // accessed by RENDER coordinates

  CacheBmp: TBitmap32;

  function FindLowestPoint: Integer;
  var
    i: Integer;
    InternalResult: Single;
  begin
    InternalResult := 0;
    for i := 0 to 5 do
    begin
      if FaceRenderInfo[i].DataFace < 0 then Continue;
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.TopLeft.Y);
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.TopRight.Y);
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.BottomLeft.Y);
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.BottomRight.Y);
    end;

    Result := Ceil(InternalResult);
  end;

  function FindFurthestRightPoint: Integer;
  var
    i: Integer;
    InternalResult: Single;
  begin
    InternalResult := 0;
    for i := 0 to 5 do
    begin
      if FaceRenderInfo[i].DataFace < 0 then Continue;
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.TopLeft.X);
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.TopRight.X);
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.BottomLeft.X);
      InternalResult := Max(InternalResult, FaceRenderInfo[i].Polygon.BottomRight.X);
    end;

    Result := Ceil(InternalResult);
  end;

  procedure CreateFaceRenderInfo;
  var
    i: Integer;
    LocalRotation: Integer;
    BuWidth, BuHeight: Single; // "Block unit"
    HighSliceTop, LowSliceBottom: Integer;

    YFaceSliceInvert: Boolean;

    SliceMasks: TRenderMasks;

    AffectedFace: Integer;

    StandardApplyYFaceHeights: Boolean;

    function GetAffectedFace(BaseFace: Integer): Integer;
    const
      ONE_ROTATION_MAP: array[0..3] of Integer =
        ( FACE_X0, FACE_X31, FACE_Z31, FACE_Z0 );
      TWO_ROTATION_MAP: array[0..3] of Integer =
        ( FACE_Z0, FACE_Z31, FACE_X0, FACE_X31 );
      THREE_ROTATION_MAP: array[0..3] of Integer =
        ( FACE_X31, FACE_X0, FACE_Z0, FACE_Z31 );
    var
      LocalViewRotation: Integer;
    begin
      if BaseFace in [FACE_Y0, FACE_Y15] then
      begin
        Result := BaseFace;
        Exit;
      end;

      if IgnoreViewRotation then
        LocalViewRotation := 0
      else
        LocalViewRotation := fViewRotation;

      case ((LocalViewRotation + 4) - Block.Rotation) mod 4 of
        1: Result := ONE_ROTATION_MAP[BaseFace];
        2: Result := TWO_ROTATION_MAP[BaseFace];
        3: Result := THREE_ROTATION_MAP[BaseFace];
        else Result := BaseFace;
      end;
    end;
    procedure StandardBlankFaces;
    var
      i: Integer;
    begin
      if bfRenderDoubleSided in MetaBlock.Flags then
      begin
        for i := 0 to 5 do
          FaceRenderInfo[i].BlankingState := bsShow;
        Exit;
      end;

      if not (rsBackFaces in RenderSettings) then
      begin
        if FaceRenderInfo[FACE_X0].BlankingState = bsUnset then FaceRenderInfo[FACE_X0].BlankingState := bsBlank;
        if FaceRenderInfo[FACE_Z0].BlankingState = bsUnset then FaceRenderInfo[FACE_Z0].BlankingState := bsBlank;
        if FaceRenderInfo[FACE_Y0].BlankingState = bsUnset then FaceRenderInfo[FACE_Y0].BlankingState := bsBlank;
        if FaceRenderInfo[FACE_X31].BlankingState = bsUnset then FaceRenderInfo[FACE_X31].BlankingState := bsShow;
        if FaceRenderInfo[FACE_Z31].BlankingState = bsUnset then FaceRenderInfo[FACE_Z31].BlankingState := bsShow;
        if FaceRenderInfo[FACE_Y15].BlankingState = bsUnset then FaceRenderInfo[FACE_Y15].BlankingState := bsShow;
      end else begin
        // This is a testing / development feature. I should probably remove it once the editor is stable.
        if FaceRenderInfo[FACE_X0].BlankingState = bsUnset then FaceRenderInfo[FACE_X0].BlankingState := bsShow;
        if FaceRenderInfo[FACE_Z0].BlankingState = bsUnset then FaceRenderInfo[FACE_Z0].BlankingState := bsShow;
        if FaceRenderInfo[FACE_Y0].BlankingState = bsUnset then FaceRenderInfo[FACE_Y0].BlankingState := bsShow;
        if FaceRenderInfo[FACE_X31].BlankingState = bsUnset then FaceRenderInfo[FACE_X31].BlankingState := bsBlank;
        if FaceRenderInfo[FACE_Z31].BlankingState = bsUnset then FaceRenderInfo[FACE_Z31].BlankingState := bsBlank;
        if FaceRenderInfo[FACE_Y15].BlankingState = bsUnset then FaceRenderInfo[FACE_Y15].BlankingState := bsBlank;
      end;

      if TopFaceOnly then
        for i := 0 to 5 do
          if i <> FACE_Y15 then
            FaceRenderInfo[i].DataFace := -1;
    end;
    procedure MoveAllTopPoints(dY: Single);
    begin
      FaceRenderInfo[FACE_Y15].Polygon.TopLeft.Y := FaceRenderInfo[FACE_Y15].Polygon.TopLeft.Y + dY;
      FaceRenderInfo[FACE_Y15].Polygon.TopRight.Y := FaceRenderInfo[FACE_Y15].Polygon.TopRight.Y + dY;
      FaceRenderInfo[FACE_Y15].Polygon.BottomLeft.Y := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft.Y + dY;
      FaceRenderInfo[FACE_Y15].Polygon.BottomRight.Y := FaceRenderInfo[FACE_Y15].Polygon.BottomRight.Y + dY;
    end;
    procedure MoveAllBottomPoints(dY: Single);
    begin
      FaceRenderInfo[FACE_Y0].Polygon.TopLeft.Y := FaceRenderInfo[FACE_Y0].Polygon.TopLeft.Y + dY;
      FaceRenderInfo[FACE_Y0].Polygon.TopRight.Y := FaceRenderInfo[FACE_Y0].Polygon.TopRight.Y + dY;
      FaceRenderInfo[FACE_Y0].Polygon.BottomLeft.Y := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft.Y + dY;
      FaceRenderInfo[FACE_Y0].Polygon.BottomRight.Y := FaceRenderInfo[FACE_Y0].Polygon.BottomRight.Y + dY;
    end;
    procedure AdjustYFaceHeights;
    begin
      MoveAllTopPoints((BuHeight / 2) * HighSliceTop);
      MoveAllBottomPoints(-(BuHeight / 2) * (4 - LowSliceBottom));
    end;
    procedure ApplyPerspectiveAndSkew;
    var
      VerticalShift: Single; // This represents how much the outsides of the sides are shifted by.
      HorizontalShift: Single;
    begin
      VerticalShift := (fViewPerspective - 1) * (BlockRenderHeight / 4);
      HorizontalShift := -fViewSkew * BlockRenderWidth / 2;

      with FaceRenderInfo[FACE_Y15].Polygon do
      begin
        TopLeft.X := TopLeft.X + HorizontalShift;
        TopLeft.Y := TopLeft.Y + (VerticalShift * 2);

        TopRight.Y := TopRight.Y + VerticalShift;

        BottomRight.X := BottomRight.X - HorizontalShift;

        BottomLeft.Y := BottomLeft.Y + VerticalShift;
      end;

      with FaceRenderInfo[FACE_Y0].Polygon do
      begin
        TopLeft.X := TopLeft.X + HorizontalShift;
        TopLeft.Y := TopLeft.Y + (VerticalShift * 2);

        TopRight.Y := TopRight.Y + VerticalShift;

        BottomRight.X := BottomRight.X - HorizontalShift;

        BottomLeft.Y := BottomLeft.Y + VerticalShift;
      end;
    end;
    function GetModifiedYMask(aMask: TRenderMask): TRenderMask;
    var
      aMaskInt, ResultInt: Integer;
    begin
      if YFaceSliceInvert then
      begin
        aMaskInt := Integer(aMask);
        case aMaskInt of
            0..3: ResultInt := 3 - aMaskInt;
            4..7: ResultInt := (3 - (aMaskInt mod 4)) + 4;
           8..11: ResultInt := (3 - ((aMaskInt + 1) mod 4)) + 8;
          12..13: ResultInt := ((aMaskInt + 1) mod 2) + 12;
          14..16: ResultInt := aMaskInt + 3;
          17..19: ResultInt := aMaskInt - 3;
          else ResultInt := aMaskInt;
        end;
        Result := TRenderMask(ResultInt);
      end else
        Result := aMask;
    end;
    procedure ApplyTopMask(aMask: TRenderMask);
    begin
      Include(FaceRenderInfo[FACE_Y15].Masks, TRenderMask(GetModifiedYMask(aMask)));
    end;
    procedure ApplyBottomMask(aMask: TRenderMask);
    begin
      Include(FaceRenderInfo[FACE_Y0].Masks, TRenderMask(GetModifiedYMask(aMask)));
    end;
    procedure CopyLine(SrcA, SrcB: TFloatPoint; DstCenter: TFloatPoint; var DstA, DstB: TFloatPoint);
    var
      SrcCenter: TFloatPoint;
      DiffX, DiffY: Single;
    begin
      // Assumption: DstA is conneted to DstB and SrcA, and correspondingly for all other points.
      SrcCenter := FloatPointMid(SrcA, SrcB);
      DiffX := SrcCenter.X - SrcA.X;
      DiffY := SrcCenter.Y - SrcA.Y;

      DstA := FloatPoint(DstCenter.X - DiffX, DstCenter.Y - DiffY);
      DstB := FloatPoint(DstCenter.X + DiffX, DstCenter.Y + DiffY);
    end;
    procedure NullAll;
    begin
      FaceRenderInfo[FACE_Z31].DataFace := -1;
      FaceRenderInfo[FACE_X31].DataFace := -1;
      FaceRenderInfo[FACE_Z0].DataFace := -1;
      FaceRenderInfo[FACE_X0].DataFace := -1;
      FaceRenderInfo[FACE_Y15].DataFace := -1;
      FaceRenderInfo[FACE_Y0].DataFace := -1;
    end;
  const
    SHADE_BUMP = 0.04;
  type
    TFaceSet = set of Byte;

    procedure BumpShadeUp(aFaces: TFaceSet);
    var
      i: Integer;
    begin
      for i := 0 to 5 do
        if i in aFaces then
          FaceRenderInfo[i].ShadeMod := FaceRenderInfo[i].ShadeMod + SHADE_BUMP;
    end;

    procedure BumpShadeDown(aFaces: TFaceSet);
    var
      i: Integer;
    begin
      for i := 0 to 5 do
        if i in aFaces then
          FaceRenderInfo[i].ShadeMod := FaceRenderInfo[i].ShadeMod - SHADE_BUMP;
    end;
  begin
    // First, we ensure it's clear.
    FillChar(FaceRenderInfo, SizeOf(FaceRenderInfo), 0);

    // And set the default shade mods
    FaceRenderInfo[FACE_Z31].ShadeMod := 1 - (SHADE_BUMP * 2);
    FaceRenderInfo[FACE_X31].ShadeMod := 1 - (SHADE_BUMP * 4);
    FaceRenderInfo[FACE_Z0].ShadeMod := 1 - (SHADE_BUMP * 4);
    FaceRenderInfo[FACE_X0].ShadeMod := 1 - (SHADE_BUMP * 2);
    FaceRenderInfo[FACE_Y15].ShadeMod := 1;
    FaceRenderInfo[FACE_Y0].ShadeMod := 1 - (SHADE_BUMP * 6);

    {%region Face indexes}
    // First, the easy part. Let's figure out what metablock face should correspond
    // to which face from the renderer's point of view.

    // We'll start with the top and bottom faces. The texture is easy enough, but
    // we also need to figure out their rotation. The bottom of the texture, can
    // only point to - level-coordinate wise, not render-wise - either X=0 or Z=0.
    // At least, this is the case for the top - I'm just assuming for the bottom
    // it's the same, I do need to investigate this at some point.
    LocalRotation := 4 - (Block.Rotation mod 2);
    if not IgnoreViewRotation then
      LocalRotation := LocalRotation + fViewRotation;
    LocalRotation := LocalRotation mod 4;
    case LocalRotation of
      1: begin FaceRenderInfo[FACE_Y0].Rotation := 3; FaceRenderInfo[FACE_Y15].Rotation := 3; end;
      2: begin FaceRenderInfo[FACE_Y0].Rotation := 2; FaceRenderInfo[FACE_Y15].Rotation := 2; end;
      3: begin FaceRenderInfo[FACE_Y0].Rotation := 1; FaceRenderInfo[FACE_Y15].Rotation := 1; end;
      else begin FaceRenderInfo[FACE_Y0].Rotation := 0; FaceRenderInfo[FACE_Y15].Rotation := 0; end;
    end;

    FaceRenderInfo[FACE_Y0].DataFace := FACE_Y0;
    FaceRenderInfo[FACE_Y15].DataFace := FACE_Y15;

    // Now, let's figure out the other four. This is where it can get a tad tricky.
    LocalRotation := 4 - Block.Rotation;
    if not IgnoreViewRotation then
      LocalRotation := LocalRotation + fViewRotation;
    LocalRotation := LocalRotation mod 4;
    case LocalRotation of
      1: begin
           FaceRenderInfo[FACE_Z31].DataFace := FACE_X31;
           FaceRenderInfo[FACE_X31].DataFace := FACE_Z0;
           FaceRenderInfo[FACE_Z0].DataFace := FACE_X0;
           FaceRenderInfo[FACE_X0].DataFace := FACE_Z31;
         end;
      2: begin
           FaceRenderInfo[FACE_Z31].DataFace := FACE_Z0;
           FaceRenderInfo[FACE_X31].DataFace := FACE_X0;
           FaceRenderInfo[FACE_Z0].DataFace := FACE_Z31;
           FaceRenderInfo[FACE_X0].DataFace := FACE_X31;
         end;
      3: begin
           FaceRenderInfo[FACE_Z31].DataFace := FACE_X0;
           FaceRenderInfo[FACE_X31].DataFace := FACE_Z31;
           FaceRenderInfo[FACE_Z0].DataFace := FACE_X31;
           FaceRenderInfo[FACE_X0].DataFace := FACE_Z0;
         end;
      else begin
        FaceRenderInfo[FACE_Z31].DataFace := FACE_Z31;
        FaceRenderInfo[FACE_X31].DataFace := FACE_X31;
        FaceRenderInfo[FACE_Z0].DataFace := FACE_Z0;
        FaceRenderInfo[FACE_X0].DataFace := FACE_X0;
      end;
    end;
    {%endregion}
    {%region Standard polygons}
    BuWidth := BlockRenderWidth div 2;
    BuHeight := BlockRenderHeight div 4;

    // First, calculate the standard polygons. Then we'll modify from there.

    // Note that FACE_X0 and FACE_Z0 are back-to-front. This is not completely
    // accurate to L3D, but greatly simplifies rendering variant blocks.

    FaceRenderInfo[FACE_Y15].Polygon := Polygon(FloatPoint(BuWidth - 0.5, BuHeight * 2),
                                                FloatPoint(0, BuHeight),
                                                FloatPoint(BuWidth - 0.5, 0),
                                                FloatPoint(BuWidth * 2 - 1, BuHeight));

    FaceRenderInfo[FACE_Y0].Polygon := Polygon(FloatPoint(BuWidth - 0.5, BuHeight * 4),
                                               FloatPoint(0, BuHeight * 3),
                                               FloatPoint(BuWidth - 0.5, BuHeight * 2),
                                               FloatPoint(BuWidth * 2 - 1, BuHeight * 3));

    ApplyPerspectiveAndSkew;

    FaceRenderInfo[FACE_Z0].Polygon := Polygon(FaceRenderInfo[FACE_Y15].Polygon.BottomLeft,
                                               FaceRenderInfo[FACE_Y15].Polygon.BottomRight,
                                               FaceRenderInfo[FACE_Y0].Polygon.BottomRight,
                                               FaceRenderInfo[FACE_Y0].Polygon.BottomLeft);

    FaceRenderInfo[FACE_X0].Polygon := Polygon(FaceRenderInfo[FACE_Y15].Polygon.BottomRight,
                                               FaceRenderInfo[FACE_Y15].Polygon.TopRight,
                                               FaceRenderInfo[FACE_Y0].Polygon.TopRight,
                                               FaceRenderInfo[FACE_Y0].Polygon.BottomRight);

    FaceRenderInfo[FACE_Z31].Polygon := Polygon(FaceRenderInfo[FACE_Y15].Polygon.TopRight,
                                                FaceRenderInfo[FACE_Y15].Polygon.TopLeft,
                                                FaceRenderInfo[FACE_Y0].Polygon.TopLeft,
                                                FaceRenderInfo[FACE_Y0].Polygon.TopRight);

    FaceRenderInfo[FACE_X31].Polygon := Polygon(FaceRenderInfo[FACE_Y15].Polygon.TopLeft,
                                                FaceRenderInfo[FACE_Y15].Polygon.BottomLeft,
                                                FaceRenderInfo[FACE_Y0].Polygon.BottomLeft,
                                                FaceRenderInfo[FACE_Y0].Polygon.TopLeft);
    {%endregion}
    {%region Segment calculation}
    HighSliceTop := 4;
    LowSliceBottom := 0;

    for i := 0 to 3 do
      if Block.Segments[i] then
      begin
        HighSliceTop := Min(HighSliceTop, i);
        LowSliceBottom := Max(LowSliceBottom, i + 1);
      end;

    // Pyramid shapes have special handling here
    case Block.Shape of
      bsPyramidUpTypeA: if HighSliceTop < 2 then
                        begin
                          NullAll;
                          Exit;
                        end;
      bsPyramidDownTypeA: if LowSliceBottom > 2 then
                          begin
                            NullAll;
                            Exit;
                          end;
      bsPyramidUpTypeB,
      bsPyramidDownTypeB: begin
                            HighSliceTop := (HighSliceTop div 2) * 2;
                            LowSliceBottom := ((LowSliceBottom + 1) div 2) * 2;
                          end;
    end;

    SliceMasks := [];

    for i := 0 to 3 do
      if (i < HighSliceTop) or (i >= LowSliceBottom) then
        Include(SliceMasks, TRenderMask(i));

    FaceRenderInfo[FACE_X0].Masks := SliceMasks;
    FaceRenderInfo[FACE_Z0].Masks := SliceMasks;
    FaceRenderInfo[FACE_X31].Masks := SliceMasks;
    FaceRenderInfo[FACE_Z31].Masks := SliceMasks;
    {%endregion}

    StandardApplyYFaceHeights := true;

    YFaceSliceInvert := Block.Rotation < 2;

    case Block.Shape of
      bsStandard: ; // No special action needed.
      bsPyramidUpTypeA:
        begin
          if HighSliceTop = 3 then
            FaceRenderInfo[FACE_Y15].Masks := FaceRenderInfo[FACE_Y15].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3]
          else if HighSliceTop = 2 then
            FaceRenderInfo[FACE_Y15].DataFace := -1;

          if LowSliceBottom = 3 then
            FaceRenderInfo[FACE_Y0].Masks := FaceRenderInfo[FACE_Y0].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3];

          FaceRenderInfo[FACE_Z31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
          FaceRenderInfo[FACE_Z31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
          FaceRenderInfo[FACE_X31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
          FaceRenderInfo[FACE_X31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
          FaceRenderInfo[FACE_Z0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
          FaceRenderInfo[FACE_Z0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
          FaceRenderInfo[FACE_X0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
          FaceRenderInfo[FACE_X0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;

          Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopLeft);
          Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopLeft);
          Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopLeft);
          Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopLeft);
          Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopRight);
          Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopRight);
          Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopRight);
          Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopRight);

          FaceRenderInfo[FACE_X0].BlankingState := bsShow;
          FaceRenderInfo[FACE_Z0].BlankingState := bsShow;

          BumpShadeUp([FACE_Z31, FACE_X31, FACE_Z0, FACE_X0]);
        end;
      bsPyramidDownTypeA:
        begin
          if HighSliceTop = 1 then
            FaceRenderInfo[FACE_Y15].Masks := FaceRenderInfo[FACE_Y15].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3];

          if LowSliceBottom = 1 then
            FaceRenderInfo[FACE_Y0].Masks := FaceRenderInfo[FACE_Y0].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3]
          else if LowSliceBottom = 2 then
            FaceRenderInfo[FACE_Y0].DataFace := -1;

          FaceRenderInfo[FACE_Z31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
          FaceRenderInfo[FACE_Z31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;
          FaceRenderInfo[FACE_X31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
          FaceRenderInfo[FACE_X31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
          FaceRenderInfo[FACE_Z0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;
          FaceRenderInfo[FACE_Z0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
          FaceRenderInfo[FACE_X0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;
          FaceRenderInfo[FACE_X0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;

          Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomLeft);
          Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomLeft);
          Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomLeft);
          Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomLeft);
          Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomRight);
          Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomRight);
          Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomRight);
          Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomRight);

          BumpShadeDown([FACE_Z31, FACE_X31, FACE_Z0, FACE_X0]);
        end;
      bsPyramidUpTypeB:
        begin
          if HighSliceTop = 2 then
            FaceRenderInfo[FACE_Y15].Masks := FaceRenderInfo[FACE_Y15].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3]
          else if HighSliceTop = 0 then
            FaceRenderInfo[FACE_Y15].DataFace := -1;

          if LowSliceBottom = 2 then
            FaceRenderInfo[FACE_Y0].Masks := FaceRenderInfo[FACE_Y0].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3];

          FaceRenderInfo[FACE_Z31].Polygon.TopLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.BottomRight, FaceRenderInfo[FACE_Y15].Polygon.TopRight);
          FaceRenderInfo[FACE_Z31].Polygon.TopRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.BottomLeft, FaceRenderInfo[FACE_Y15].Polygon.TopLeft);

          FaceRenderInfo[FACE_X31].Polygon.TopLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.TopRight, FaceRenderInfo[FACE_Y15].Polygon.TopLeft);
          FaceRenderInfo[FACE_X31].Polygon.TopRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.BottomRight, FaceRenderInfo[FACE_Y15].Polygon.BottomLeft);

          FaceRenderInfo[FACE_Z0].Polygon.TopLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.TopLeft, FaceRenderInfo[FACE_Y15].Polygon.BottomLeft);
          FaceRenderInfo[FACE_Z0].Polygon.TopRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.TopRight, FaceRenderInfo[FACE_Y15].Polygon.BottomRight);

          FaceRenderInfo[FACE_X0].Polygon.TopLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.BottomLeft, FaceRenderInfo[FACE_Y15].Polygon.BottomRight);
          FaceRenderInfo[FACE_X0].Polygon.TopRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y15].Polygon.TopLeft, FaceRenderInfo[FACE_Y15].Polygon.TopRight);

          Include(FaceRenderInfo[FACE_Z31].Masks, rmCutToPyramidUp);
          Include(FaceRenderInfo[FACE_X31].Masks, rmCutToPyramidUp);
          Include(FaceRenderInfo[FACE_Z0].Masks, rmCutToPyramidUp);
          Include(FaceRenderInfo[FACE_X0].Masks, rmCutToPyramidUp);

          FaceRenderInfo[FACE_X0].BlankingState := bsShow;
          FaceRenderInfo[FACE_Z0].BlankingState := bsShow;

          BumpShadeUp([FACE_Z31, FACE_X31, FACE_Z0, FACE_X0]);
        end;
      bsPyramidDownTypeB:
        begin
          if HighSliceTop = 2 then
            FaceRenderInfo[FACE_Y15].Masks := FaceRenderInfo[FACE_Y15].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3];

          if LowSliceBottom = 2 then
            FaceRenderInfo[FACE_Y0].Masks := FaceRenderInfo[FACE_Y0].Masks +
              [rmCutSlice0, rmCutSlice3, rmCutHorzSlice0, rmCutHorzSlice3]
          else if LowSliceBottom = 4 then
            FaceRenderInfo[FACE_Y0].DataFace := -1;

          FaceRenderInfo[FACE_Z31].Polygon.BottomLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.BottomRight, FaceRenderInfo[FACE_Y0].Polygon.TopRight);
          FaceRenderInfo[FACE_Z31].Polygon.BottomRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.BottomLeft, FaceRenderInfo[FACE_Y0].Polygon.TopLeft);

          FaceRenderInfo[FACE_X31].Polygon.BottomLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.TopRight, FaceRenderInfo[FACE_Y0].Polygon.TopLeft);
          FaceRenderInfo[FACE_X31].Polygon.BottomRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.BottomRight, FaceRenderInfo[FACE_Y0].Polygon.BottomLeft);

          FaceRenderInfo[FACE_Z0].Polygon.BottomLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.TopLeft, FaceRenderInfo[FACE_Y0].Polygon.BottomLeft);
          FaceRenderInfo[FACE_Z0].Polygon.BottomRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.TopRight, FaceRenderInfo[FACE_Y0].Polygon.BottomRight);

          FaceRenderInfo[FACE_X0].Polygon.BottomLeft :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.BottomLeft, FaceRenderInfo[FACE_Y0].Polygon.BottomRight);
          FaceRenderInfo[FACE_X0].Polygon.BottomRight :=
            FloatPointMid(FaceRenderInfo[FACE_Y0].Polygon.TopLeft, FaceRenderInfo[FACE_Y0].Polygon.TopRight);

          Include(FaceRenderInfo[FACE_Z31].Masks, rmCutToPyramidDown);
          Include(FaceRenderInfo[FACE_X31].Masks, rmCutToPyramidDown);
          Include(FaceRenderInfo[FACE_Z0].Masks, rmCutToPyramidDown);
          Include(FaceRenderInfo[FACE_X0].Masks, rmCutToPyramidDown);

          BumpShadeDown([FACE_Z31, FACE_X31, FACE_Z0, FACE_X0]);
        end;
      bsSlope45Above:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        FaceRenderInfo[FACE_Z31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
                        FaceRenderInfo[FACE_Z31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;

                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopRight);
                        Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopLeft);
                      end;
            FACE_X31: begin
                        FaceRenderInfo[FACE_X31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
                        FaceRenderInfo[FACE_X31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;

                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopRight);
                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopLeft);
                      end;
            FACE_Z0: begin
                       FaceRenderInfo[FACE_Z0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
                       FaceRenderInfo[FACE_Z0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopRight;

                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopRight);
                       Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopLeft);
                     end;
            FACE_X0: begin
                       FaceRenderInfo[FACE_X0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
                       FaceRenderInfo[FACE_X0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;

                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopRight);
                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopLeft);
                     end;
          end;

          if HighSliceTop = 0 then
            FaceRenderInfo[FACE_Y15].DataFace := -1
          else for i := HighSliceTop to 3 do
            ApplyTopMask(TRenderMask(i));

          FaceRenderInfo[AffectedFace].BlankingState := bsShow;

          BumpShadeUp([AffectedFace]);
        end;
      bsSlope45Below:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        FaceRenderInfo[FACE_Z31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
                        FaceRenderInfo[FACE_Z31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;

                        Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomLeft);
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomRight);
                      end;
            FACE_X31: begin
                        FaceRenderInfo[FACE_X31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
                        FaceRenderInfo[FACE_X31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;

                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomLeft);
                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomRight);
                      end;
            FACE_Z0: begin
                       FaceRenderInfo[FACE_Z0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;
                       FaceRenderInfo[FACE_Z0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopRight;

                       Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomLeft);
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomRight);
                     end;
            FACE_X0: begin
                       FaceRenderInfo[FACE_X0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
                       FaceRenderInfo[FACE_X0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;

                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomLeft);
                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomRight);
                     end;
          end;

          if LowSliceBottom = 4 then
            FaceRenderInfo[FACE_Y0].DataFace := -1
          else for i := 0 to LowSliceBottom do
            ApplyBottomMask(TRenderMask(i));

          FaceRenderInfo[AffectedFace].BlankingState := bsShow;

          BumpShadeDown([AffectedFace]);
        end;
      bsDeflector:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        FaceRenderInfo[FACE_Z31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
                        FaceRenderInfo[FACE_Z31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;

                        FaceRenderInfo[FACE_X31].DataFace := -1;

                        BumpShadeDown([FACE_Z31]);
                      end;
            FACE_X31: begin
                        FaceRenderInfo[FACE_X31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
                        FaceRenderInfo[FACE_X31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;

                        FaceRenderInfo[FACE_Z0].DataFace := -1;
                      end;
            FACE_Z0: begin
                       FaceRenderInfo[FACE_Z0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
                       FaceRenderInfo[FACE_Z0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopRight;

                       FaceRenderInfo[FACE_X0].DataFace := -1;
                       BumpShadeUp([FACE_Z0]);
                     end;
            FACE_X0: begin
                       FaceRenderInfo[FACE_X0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
                       FaceRenderInfo[FACE_X0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;

                       FaceRenderInfo[FACE_Z31].DataFace := -1;
                     end;
          end;

          case Block.Rotation of
            0, 1: begin
                    Include(FaceRenderInfo[FACE_Y15].Masks, rmCut45TopLeft);
                    Include(FaceRenderInfo[FACE_Y0].Masks, rmCut45TopLeft);
                  end;
            2, 3: begin
                    Include(FaceRenderInfo[FACE_Y15].Masks, rmCut45BottomRight);
                    Include(FaceRenderInfo[FACE_Y0].Masks, rmCut45BottomRight);
                  end;
          end;
        end;
      bsSlope225Above:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        FaceRenderInfo[FACE_Y15].Polygon.TopLeft.Y := FaceRenderInfo[FACE_Y15].Polygon.TopLeft.Y + BuHeight;
                        FaceRenderInfo[FACE_Y15].Polygon.TopRight.Y := FaceRenderInfo[FACE_Y15].Polygon.TopRight.Y + BuHeight;

                        Include(FaceRenderInfo[FACE_X0].Masks, TRenderMask(Integer(rmCut225TopRight0) + HighSliceTop));
                        Include(FaceRenderInfo[FACE_X31].Masks, TRenderMask(Integer(rmCut225TopLeft0) + HighSliceTop));
                      end;
            FACE_X31: begin
                        FaceRenderInfo[FACE_Y15].Polygon.BottomLeft.Y := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft.Y + BuHeight;
                        FaceRenderInfo[FACE_Y15].Polygon.TopLeft.Y := FaceRenderInfo[FACE_Y15].Polygon.TopLeft.Y + BuHeight;

                        Include(FaceRenderInfo[FACE_Z31].Masks, TRenderMask(Integer(rmCut225TopRight0) + HighSliceTop));
                        Include(FaceRenderInfo[FACE_Z0].Masks, TRenderMask(Integer(rmCut225TopLeft0) + HighSliceTop));
                      end;
            FACE_Z0: begin
                       FaceRenderInfo[FACE_Y15].Polygon.BottomRight.Y := FaceRenderInfo[FACE_Y15].Polygon.BottomRight.Y + BuHeight;
                       FaceRenderInfo[FACE_Y15].Polygon.BottomLeft.Y := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft.Y + BuHeight;

                       Include(FaceRenderInfo[FACE_X31].Masks, TRenderMask(Integer(rmCut225TopRight0) + HighSliceTop));
                       Include(FaceRenderInfo[FACE_X0].Masks, TRenderMask(Integer(rmCut225TopLeft0) + HighSliceTop));
                     end;
            FACE_X0: begin
                       FaceRenderInfo[FACE_Y15].Polygon.TopRight.Y := FaceRenderInfo[FACE_Y15].Polygon.TopRight.Y + BuHeight;
                       FaceRenderInfo[FACE_Y15].Polygon.BottomRight.Y := FaceRenderInfo[FACE_Y15].Polygon.BottomRight.Y + BuHeight;

                       Include(FaceRenderInfo[FACE_Z0].Masks, TRenderMask(Integer(rmCut225TopRight0) + HighSliceTop));
                       Include(FaceRenderInfo[FACE_Z31].Masks, TRenderMask(Integer(rmCut225TopLeft0) + HighSliceTop));
                     end;
          end;

          if (LowSliceBottom - HighSliceTop - 1) >= 2 then
          begin
            Include(FaceRenderInfo[AffectedFace].Masks, TRenderMask(Integer(rmCutSlice0) + HighSliceTop));
            Include(FaceRenderInfo[AffectedFace].Masks, TRenderMask(Integer(rmCutSlice0) + HighSliceTop + 1));
          end else
            FaceRenderInfo[AffectedFace].DataFace := -1;

          if (LowSliceBottom - HighSliceTop - 1) = 0 then
          begin
            ApplyTopMask(rmCutSlice2);
            ApplyTopMask(rmCutSlice3);
            ApplyBottomMask(rmCutSlice2);
            ApplyBottomMask(rmCutSlice3);
          end;

          BumpShadeDown([FACE_Y15]);
        end;
      bsSlope225Below:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        FaceRenderInfo[FACE_Y0].Polygon.TopLeft.Y := FaceRenderInfo[FACE_Y0].Polygon.TopLeft.Y - BuHeight;
                        FaceRenderInfo[FACE_Y0].Polygon.TopRight.Y := FaceRenderInfo[FACE_Y0].Polygon.TopRight.Y - BuHeight;

                        Include(FaceRenderInfo[FACE_X0].Masks, TRenderMask(Integer(rmCut225BottomRight0) + (4 - LowSliceBottom)));
                        Include(FaceRenderInfo[FACE_X31].Masks, TRenderMask(Integer(rmCut225BottomLeft0) + (4 - LowSliceBottom)));
                      end;
            FACE_X31: begin
                        FaceRenderInfo[FACE_Y0].Polygon.BottomLeft.Y := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft.Y - BuHeight;
                        FaceRenderInfo[FACE_Y0].Polygon.TopLeft.Y := FaceRenderInfo[FACE_Y0].Polygon.TopLeft.Y - BuHeight;

                        Include(FaceRenderInfo[FACE_Z31].Masks, TRenderMask(Integer(rmCut225BottomRight0) + (4 - LowSliceBottom)));
                        Include(FaceRenderInfo[FACE_Z0].Masks, TRenderMask(Integer(rmCut225BottomLeft0) + (4 - LowSliceBottom)));
                      end;
            FACE_Z0: begin
                       FaceRenderInfo[FACE_Y0].Polygon.BottomRight.Y := FaceRenderInfo[FACE_Y0].Polygon.BottomRight.Y - BuHeight;
                       FaceRenderInfo[FACE_Y0].Polygon.BottomLeft.Y := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft.Y - BuHeight;

                       Include(FaceRenderInfo[FACE_X31].Masks, TRenderMask(Integer(rmCut225BottomRight0) + (4 - LowSliceBottom)));
                       Include(FaceRenderInfo[FACE_X0].Masks, TRenderMask(Integer(rmCut225BottomLeft0) + (4 - LowSliceBottom)));
                     end;
            FACE_X0: begin
                       FaceRenderInfo[FACE_Y0].Polygon.TopRight.Y := FaceRenderInfo[FACE_Y0].Polygon.TopRight.Y - BuHeight;
                       FaceRenderInfo[FACE_Y0].Polygon.BottomRight.Y := FaceRenderInfo[FACE_Y0].Polygon.BottomRight.Y - BuHeight;

                       Include(FaceRenderInfo[FACE_Z0].Masks, TRenderMask(Integer(rmCut225BottomRight0) + (4 - LowSliceBottom)));
                       Include(FaceRenderInfo[FACE_Z31].Masks, TRenderMask(Integer(rmCut225BottomLeft0) + (4 - LowSliceBottom)));
                     end;
          end;

          if (LowSliceBottom - HighSliceTop - 1) >= 2 then
          begin
            Include(FaceRenderInfo[AffectedFace].Masks, TRenderMask(Integer(rmCutSlice0) + LowSliceBottom - 1));
            Include(FaceRenderInfo[AffectedFace].Masks, TRenderMask(Integer(rmCutSlice0) + LowSliceBottom - 2));
          end else
            FaceRenderInfo[AffectedFace].DataFace := -1;

          if (LowSliceBottom - HighSliceTop - 1) = 0 then
          begin
            ApplyTopMask(rmCutSlice2);
            ApplyTopMask(rmCutSlice3);
            ApplyBottomMask(rmCutSlice2);
            ApplyBottomMask(rmCutSlice3);
          end;

          BumpShadeUp([FACE_Y0]);
        end;
      bsSlopeCornerAbove:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        // and FACE_X0
                        FaceRenderInfo[FACE_X0].BlankingState := bsShow;
                        FaceRenderInfo[FACE_X0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
                        FaceRenderInfo[FACE_X0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopRight);

                        FaceRenderInfo[FACE_Z31].BlankingState := bsShow;
                        FaceRenderInfo[FACE_Z31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
                        FaceRenderInfo[FACE_Z31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopLeft);

                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopRight);
                        Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopLeft);

                        FaceRenderInfo[FACE_X0].BlankingState := bsShow;

                        BumpShadeUp([FACE_Z31, FACE_X0]);
                      end;
            FACE_X31: begin
                        // and FACE_Z31
                        FaceRenderInfo[FACE_Z31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
                        FaceRenderInfo[FACE_Z31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopRight);

                        FaceRenderInfo[FACE_X31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
                        FaceRenderInfo[FACE_X31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
                        Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopLeft);

                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopRight);
                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopLeft);

                        BumpShadeUp([FACE_Z31, FACE_X31]);
                      end;
            FACE_Z0: begin
                       // and FACE_X31
                       FaceRenderInfo[FACE_X31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
                       FaceRenderInfo[FACE_X31].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopRight);

                       FaceRenderInfo[FACE_Z0].BlankingState := bsShow;
                       FaceRenderInfo[FACE_Z0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
                       FaceRenderInfo[FACE_Z0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopLeft);

                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopRight);
                       Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopLeft);

                       FaceRenderInfo[FACE_Z0].BlankingState := bsShow;

                       BumpShadeUp([FACE_Z0, FACE_X31]);
                     end;
            FACE_X0: begin
                       // and FACE_Z0
                       FaceRenderInfo[FACE_Z0].BlankingState := bsShow;
                       FaceRenderInfo[FACE_Z0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
                       FaceRenderInfo[FACE_Z0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopRight);

                       FaceRenderInfo[FACE_X0].BlankingState := bsShow;
                       FaceRenderInfo[FACE_X0].BlankingState := bsShow;
                       FaceRenderInfo[FACE_X0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
                       FaceRenderInfo[FACE_X0].Polygon.TopLeft := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
                       Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopLeft);

                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopRight);
                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopLeft);

                       FaceRenderInfo[FACE_Z0].BlankingState := bsShow;
                       FaceRenderInfo[FACE_X0].BlankingState := bsShow;

                       BumpShadeUp([FACE_Z0, FACE_X0]);
                     end;
          end;

          if HighSliceTop = 0 then
            FaceRenderInfo[FACE_Y15].DataFace := -1
          else begin
            for i := 0 to (3 - HighSliceTop) do
            begin
              ApplyTopMask(TRenderMask(3 - i));
              ApplyTopMask(TRenderMask(i + 4));
            end;
          end;
        end;
      bsSlopeCornerBelow:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        // and FACE_X0
                        FaceRenderInfo[FACE_X0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;
                        FaceRenderInfo[FACE_X0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomRight);

                        FaceRenderInfo[FACE_Z31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
                        FaceRenderInfo[FACE_Z31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;
                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomLeft);

                        Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomLeft);
                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomRight);

                        BumpShadeDown([FACE_Z31, FACE_X0]);
                      end;
            FACE_X31: begin
                        // and FACE_Z31
                        FaceRenderInfo[FACE_Z31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
                        FaceRenderInfo[FACE_Z31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;
                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomRight);

                        FaceRenderInfo[FACE_X31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
                        FaceRenderInfo[FACE_X31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
                        Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomLeft);

                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomLeft);
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomRight);

                        BumpShadeDown([FACE_Z0, FACE_X0]);
                      end;
            FACE_Z0: begin
                       // and FACE_X31
                       FaceRenderInfo[FACE_X31].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
                       FaceRenderInfo[FACE_X31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomRight);

                       FaceRenderInfo[FACE_Z0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;
                       FaceRenderInfo[FACE_Z0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomLeft);

                       Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomLeft);
                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomRight);

                       BumpShadeDown([FACE_X31, FACE_Z0]);
                     end;
            FACE_X0: begin
                       // and FACE_Z0
                       FaceRenderInfo[FACE_Z0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;
                       FaceRenderInfo[FACE_Z0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomRight);

                       FaceRenderInfo[FACE_X0].Polygon.BottomLeft := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;
                       FaceRenderInfo[FACE_X0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;
                       Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomLeft);

                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomLeft);
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomRight);

                       BumpShadeDown([FACE_Z0, FACE_X0]);
                     end;
          end;

          if LowSliceBottom = 0 then
            FaceRenderInfo[FACE_Y0].DataFace := -1
          else begin
            for i := 0 to (LowSliceBottom - 1) do
            begin
              ApplyBottomMask(TRenderMask(3 - i));
              ApplyBottomMask(TRenderMask(i + 4));
            end;
          end;
        end;
      bsSlopeDiagonalAbove:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        // and FACE_X31
                        FaceRenderInfo[FACE_X31].DataFace := -1;

                        FaceRenderInfo[FACE_Z31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomLeft;
                        CopyLine(FaceRenderInfo[FACE_Z31].Polygon.BottomLeft, FaceRenderInfo[FACE_Z31].Polygon.BottomRight,
                                 FaceRenderInfo[FACE_Y15].Polygon.BottomRight,
                                 FaceRenderInfo[FACE_Z31].Polygon.TopLeft, FaceRenderInfo[FACE_Z31].Polygon.TopRight);

                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCutToPyramidUp);
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopRight);
                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopLeft);

                        BumpShadeDown([FACE_Z31]);
                      end;
            FACE_X31: begin
                        // and FACE_Z0
                        FaceRenderInfo[FACE_Z0].DataFace := -1;

                        FaceRenderInfo[FACE_X31].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.BottomRight;
                        CopyLine(FaceRenderInfo[FACE_X31].Polygon.BottomLeft, FaceRenderInfo[FACE_X31].Polygon.BottomRight,
                                 FaceRenderInfo[FACE_Y15].Polygon.TopRight,
                                 FaceRenderInfo[FACE_X31].Polygon.TopLeft, FaceRenderInfo[FACE_X31].Polygon.TopRight);

                        Include(FaceRenderInfo[FACE_X31].Masks, rmCutToPyramidUp);
                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopRight);
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45TopLeft);
                      end;
            FACE_Z0: begin
                       // and FACE_X0
                       FaceRenderInfo[FACE_X0].DataFace := -1;

                       FaceRenderInfo[FACE_Z0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopRight;
                       CopyLine(FaceRenderInfo[FACE_Z0].Polygon.BottomLeft, FaceRenderInfo[FACE_Z0].Polygon.BottomRight,
                                FaceRenderInfo[FACE_Y15].Polygon.TopLeft,
                                FaceRenderInfo[FACE_Z0].Polygon.TopLeft, FaceRenderInfo[FACE_Z0].Polygon.TopRight);

                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCutToPyramidUp);
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopRight);
                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45TopLeft);

                       FaceRenderInfo[FACE_Z0].BlankingState := bsShow;

                       BumpShadeUp([FACE_Z0]);
                     end;
            FACE_X0: begin
                       // and FACE_Z31
                       FaceRenderInfo[FACE_Z31].DataFace := -1;

                       FaceRenderInfo[FACE_X0].Polygon.BottomRight := FaceRenderInfo[FACE_Y0].Polygon.TopLeft;
                       CopyLine(FaceRenderInfo[FACE_X0].Polygon.BottomLeft, FaceRenderInfo[FACE_X0].Polygon.BottomRight,
                                FaceRenderInfo[FACE_Y15].Polygon.BottomLeft,
                                FaceRenderInfo[FACE_X0].Polygon.TopLeft, FaceRenderInfo[FACE_X0].Polygon.TopRight);

                       Include(FaceRenderInfo[FACE_X0].Masks, rmCutToPyramidUp);
                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45TopRight);
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45TopLeft);

                       FaceRenderInfo[FACE_X0].BlankingState := bsShow;
                     end;
          end;

          case HighSliceTop of
            0: FaceRenderInfo[FACE_Y15].DataFace := -1;
            1: ApplyTopMask(rmCut45BottomRight3);
            2: ApplyTopMask(rmCut45BottomRight2);
            3: ApplyTopMask(rmCut45BottomRight1);
          end;

          case LowSliceBottom of
            1: ApplyBottomMask(rmCut45BottomRight3);
            2: ApplyBottomMask(rmCut45BottomRight2);
            3: ApplyBottomMask(rmCut45BottomRight1);
            4: ApplyBottomMask(rmCut45BottomRight);
          end;
        end;
      bsSlopeDiagonalBelow:
        begin
          AffectedFace := GetAffectedFace(FACE_Z31);

          case AffectedFace of
            FACE_Z31: begin
                        // and FACE_X31
                        FaceRenderInfo[FACE_X31].DataFace := -1;

                        FaceRenderInfo[FACE_Z31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomLeft;
                        CopyLine(FaceRenderInfo[FACE_Z31].Polygon.TopLeft, FaceRenderInfo[FACE_Z31].Polygon.TopRight,
                                 FaceRenderInfo[FACE_Y0].Polygon.BottomRight,
                                 FaceRenderInfo[FACE_Z31].Polygon.BottomLeft, FaceRenderInfo[FACE_Z31].Polygon.BottomRight);

                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCutToPyramidDown);
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomRight);
                        Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomLeft);

                        BumpShadeDown([FACE_Z31]);
                      end;
            FACE_X31: begin
                        // and FACE_Z0
                        FaceRenderInfo[FACE_Z0].DataFace := -1;

                        FaceRenderInfo[FACE_X31].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.BottomRight;
                        CopyLine(FaceRenderInfo[FACE_X31].Polygon.TopLeft, FaceRenderInfo[FACE_X31].Polygon.TopRight,
                                 FaceRenderInfo[FACE_Y0].Polygon.TopRight,
                                 FaceRenderInfo[FACE_X31].Polygon.BottomLeft, FaceRenderInfo[FACE_X31].Polygon.BottomRight);

                        Include(FaceRenderInfo[FACE_X31].Masks, rmCutToPyramidDown);
                        Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomRight);
                        Include(FaceRenderInfo[FACE_X0].Masks, rmCut45BottomLeft);
                      end;
            FACE_Z0: begin
                       // and FACE_X0
                       FaceRenderInfo[FACE_X0].DataFace := -1;

                       FaceRenderInfo[FACE_Z0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopRight;
                       CopyLine(FaceRenderInfo[FACE_Z0].Polygon.TopLeft, FaceRenderInfo[FACE_Z0].Polygon.TopRight,
                                FaceRenderInfo[FACE_Y0].Polygon.TopLeft,
                                FaceRenderInfo[FACE_Z0].Polygon.BottomLeft, FaceRenderInfo[FACE_Z0].Polygon.BottomRight);

                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCutToPyramidDown);
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomRight);
                       Include(FaceRenderInfo[FACE_Z31].Masks, rmCut45BottomLeft);

                       FaceRenderInfo[FACE_Z0].BlankingState := bsShow;

                       BumpShadeDown([FACE_Z0]);
                     end;
            FACE_X0: begin
                       // and FACE_Z31
                       FaceRenderInfo[FACE_Z31].DataFace := -1;

                       FaceRenderInfo[FACE_X0].Polygon.TopRight := FaceRenderInfo[FACE_Y15].Polygon.TopLeft;
                       CopyLine(FaceRenderInfo[FACE_X0].Polygon.TopLeft, FaceRenderInfo[FACE_X0].Polygon.TopRight,
                                FaceRenderInfo[FACE_Y0].Polygon.BottomLeft,
                                FaceRenderInfo[FACE_X0].Polygon.BottomLeft, FaceRenderInfo[FACE_X0].Polygon.BottomRight);

                       Include(FaceRenderInfo[FACE_X0].Masks, rmCutToPyramidDown);
                       Include(FaceRenderInfo[FACE_Z0].Masks, rmCut45BottomRight);
                       Include(FaceRenderInfo[FACE_X31].Masks, rmCut45BottomLeft);

                       FaceRenderInfo[FACE_X0].BlankingState := bsShow;
                     end;
          end;

          case HighSliceTop of
            0: ApplyTopMask(rmCut45BottomRight);
            1: ApplyTopMask(rmCut45BottomRight1);
            2: ApplyTopMask(rmCut45BottomRight2);
            3: ApplyTopMask(rmCut45BottomRight3);
          end;

          case LowSliceBottom of
            1: ApplyBottomMask(rmCut45BottomRight1);
            2: ApplyBottomMask(rmCut45BottomRight2);
            3: ApplyBottomMask(rmCut45BottomRight3);
            4: FaceRenderInfo[FACE_Y0].DataFace := -1;
          end;
        end;
    end;

    if StandardApplyYFaceHeights then
      AdjustYFaceHeights;

    StandardBlankFaces;
  end;

  procedure DrawFaces;
    procedure RenderThisFace(aIndex: Integer);
    var
      Info: TFaceRenderInfo;
      TexBMP: TBitmap32;
      LocalPolygon: TPolygon;
      MetaFace: TL3DMetaBlockFace;
      Shade: Integer;

      ArrowBMP: TBitmap32;
      LocalViewRotation: Integer;
    begin
      Info := FaceRenderInfo[aIndex];

      if (Info.DataFace >= 0) and (Info.BlankingState <> bsBlank) and Info.Polygon.CheckConcave then
      begin
        MetaFace := MetaBlock.Faces[Info.DataFace];

        if ((MetaFace.TextureIndex < 0) or (MetaFace.TextureIndex > 99)) and
           not (rsNoInvisibles in RenderSettings) then Exit;

        Result := true;

        TexBMP := MetaFace.Texture;
        LocalPolygon := Info.Polygon.RotatePoints(Info.Rotation);

        Shade := Round(SHADE_VALUES[Min(8, MetaFace.Shade)] * Info.ShadeMod);

        DrawTransformed(Dst, Info.Masks, TexBMP, LocalPolygon.TopLeft, LocalPolygon.TopRight,
                        LocalPolygon.BottomRight, LocalPolygon.BottomLeft, Shade);
      end;

      if (aIndex = FACE_Y15) and (Block.Graphic = 0) then
      begin
        if IgnoreViewRotation then
          LocalViewRotation := 0
        else
          LocalViewRotation := fViewRotation;
        LocalPolygon := fRenderParams.BlockTopPolygon.RotatePoints(-Block.Rotation + 2 - LocalViewRotation);
        ArrowBMP := TBitmap32.Create;
        try
          ArrowBMP.SetSize(64, 64);
          ArrowBMP.Clear($00000000);
          fCameraGraphic.DrawTo(ArrowBMP, ArrowBMP.BoundsRect, SizeRect(320, 0, 64, 64));
          DrawTransformed(Dst, ArrowBMP, LocalPolygon.TopLeft, LocalPolygon.TopRight,
                        LocalPolygon.BottomRight, LocalPolygon.BottomLeft);
        finally
          ArrowBMP.Free;
        end;
      end;
    end;
  begin
    // Order is mostly important, although swapping Z0 <--> X0 and/or
    // Z31 <--> X31 will not cause any problems.
    RenderThisFace(FACE_Z0);
    RenderThisFace(FACE_X0);
    RenderThisFace(FACE_Y0);
    RenderThisFace(FACE_X31);
    RenderThisFace(FACE_Z31);
    RenderThisFace(FACE_Y15);
  end;

  function MakeIdentifier: String;
  var
    LocalViewRotation: Integer;
  begin
    if IgnoreViewRotation then
      LocalViewRotation := 0
    else
      LocalViewRotation := fViewRotation;

    Result := IntToHex(Block.RawData, 4) + IntToStr(LocalViewRotation) + FloatToStr(fViewPerspective) + FloatToStr(fViewSkew);
    if TopFaceOnly then Result := Result + '_TFO';
  end;

begin
  Result := false;
  EnsureTextureSet(Level.TextureSet);

  CacheBmp := Cache[MakeIdentifier];

  if Block.Empty then
    Dst.Clear($00000000)
  else if CacheBmp = nil then
  begin
    MetaBlock := Level.MetaBlocks[Block.Graphic];
    EnsureFaceGraphics(MetaBlock);

    CreateFaceRenderInfo;

    if (Dst.Width <> FindFurthestRightPoint) or (Dst.Height <> FindLowestPoint) then
      Dst.SetSize(FindFurthestRightPoint, FindLowestPoint);
    Dst.Clear(0);

    DrawFaces;

    if Result then
      Cache.CacheImage(MakeIdentifier, Dst);
  end else begin
    Dst.Assign(CacheBmp);
    Result := true;
  end;

  Dst.DrawMode := dmTransparent;
end;

procedure TL3DRenderer.CalculateRenderParams;
var
  OffsetMiddleX: Integer;

  function MakeBlockTopPolygon: TPolygon;
  begin
    Result := Polygon(FloatPoint((BlockRenderWidth / 2) - 0.5, 0),
                      FloatPoint((BlockRenderWidth / 2) * 2 - 1, (BlockRenderHeight / 4)),
                      FloatPoint((BlockRenderWidth / 2) - 0.5, (BlockRenderHeight / 4) * 2),
                      FloatPoint(0, (BlockRenderHeight / 4)));
    Result.TopLeft.X := Result.TopLeft.X + (fViewSkew * (BlockRenderWidth / 2));
    Result.BottomRight.X := Result.BottomRight.X - (fViewSkew * (BlockRenderWidth / 2));
    Result.TopRight.Y := Result.TopRight.Y + ((fViewPerspective - 1) * (BlockRenderHeight / 4));
    Result.BottomRight.Y := Result.BottomRight.Y + ((fViewPerspective - 1) * (BlockRenderHeight / 2));
    Result.BottomLeft.Y := Result.BottomLeft.Y + ((fViewPerspective - 1) * (BlockRenderHeight / 4));
  end;

  procedure GenerateSelectionImages;
  var
    Temp: TBitmap32;
    TopPolygon, BottomPolygon: TPolygon;
  begin
    Temp := TBitmap32.Create;
    try
      Temp.SetSize(64, 64);
      Temp.Clear($FFFFFFFF);

      if BlockRenderWidth < 48 then
        Temp.FillRect(6, 6, 58, 58, $00000000)
      else
        Temp.FillRect(4, 4, 60, 60, $00000000);

      fRenderParams.SelectionTop.SetSize(BlockRenderWidth-1, BlockRenderHeight);
      fRenderParams.SelectionTop.Clear($00000000);

      TopPolygon := fRenderParams.BlockTopPolygon;
      BottomPolygon := TopPolygon.Shift(0, BlockRenderHeight div 2);

      DrawTransformed(fRenderParams.SelectionTop, Temp, TopPolygon.TopLeft, TopPolygon.TopRight, TopPolygon.BottomRight, TopPolygon.BottomLeft);
      fRenderParams.SelectionTopFace.Assign(fRenderParams.SelectionTop);
      DrawTransformed(fRenderParams.SelectionTop, Temp, TopPolygon.BottomLeft, TopPolygon.BottomRight, BottomPolygon.BottomRight, BottomPolygon.BottomLeft);
      DrawTransformed(fRenderParams.SelectionTop, Temp, TopPolygon.BottomRight, TopPolygon.TopRight, BottomPolygon.TopRight, BottomPolygon.BottomRight);

      fRenderParams.SelectionBottom.SetSize(BlockRenderWidth-1, BlockRenderHeight);
      fRenderParams.SelectionBottom.Clear($00000000);

      DrawTransformed(fRenderParams.SelectionBottom, Temp, TopPolygon.BottomLeft, TopPolygon.TopLeft, BottomPolygon.TopLeft, BottomPolygon.BottomLeft);
      DrawTransformed(fRenderParams.SelectionBottom, Temp, TopPolygon.TopLeft, TopPolygon.TopRight, BottomPolygon.TopRight, BottomPolygon.TopLeft);
      DrawTransformed(fRenderParams.SelectionBottom, Temp, BottomPolygon.TopLeft, BottomPolygon.TopRight, BottomPolygon.BottomRight, BottomPolygon.BottomLeft);

      Temp.Clear($FFFFFFFF);
      if BlockRenderWidth < 48 then
      begin
        Temp.FillRect(6, 0, 58, 64, $00000000);
        Temp.FillRect(0, 6, 64, 58, $00000000);
      end else begin
        Temp.FillRect(4, 0, 60, 64, $00000000);
        Temp.FillRect(0, 4, 64, 60, $00000000);
      end;

      fRenderParams.SelectionTopCorners.SetSize(BlockRenderWidth-1, BlockRenderHeight);
      fRenderParams.SelectionTopCorners.Clear($00000000);
      DrawTransformed(fRenderParams.SelectionTopCorners, Temp, TopPolygon.TopLeft, TopPolygon.TopRight, TopPolygon.BottomRight, TopPolygon.BottomLeft);
    finally
      Temp.Free;
    end;
  end;

begin
  OffsetMiddleX := Round((BlockRenderWidth div 2) * -fViewSkew);

  with fRenderParams do
  begin
    NextXHorzOffset := (BlockRenderWidth div 2) - 1 + OffsetMiddleX;
    NextXVertOffset := Round((BlockRenderHeight div 4) * fViewPerspective);

    NextYHorzOffset := 0;
    NextYVertOffset := -Round(BlockRenderHeight / 2);

    NextZHorzOffset := (-BlockRenderWidth div 2) + 1 + OffsetMiddleX;
    NextZVertOffset := NextXVertOffset;

    DstWidth  := (Abs(NextZHorzOffset) + NextXHorzOffset) * 65 div 2;
    DstHeight := (Abs(NextYVertOffset) * 17) + (Abs(NextXVertOffset) * 65);

    InitialPosition   := Point((DstWidth div 2) - NextXHorzOffset, (Abs(NextYVertOffset) * 16) + (NextYVertOffset div 2));
    TopCornerPosition := InitialPosition;
    InitialPosition.X := InitialPosition.X - (OffsetMiddleX * 32);

    BlockTopPolygon := MakeBlockTopPolygon;
  end;

  GenerateSelectionImages;
end;

procedure TL3DRenderer.GenerateInvisibleReveal;
var
  x, y: Integer;
  PixPtr: PColor32;
  Modifier: TColor32;
const
  MODIFIER_CHANGE = $836BC2; // arbitrary, just needs to be constant
begin
  fInvisibleReveal.SetSize(64, 64);
  fInvisibleReveal.Clear($00000000);
  fInvisibleReveal.FillRect(1, 1, 63, 63, $FF202020);
  fInvisibleReveal.FillRect(2, 2, 62, 62, $FF606060);
  fInvisibleReveal.FillRect(3, 3, 61, 61, $FFC0C0C0);
  fInvisibleReveal.FillRect(4, 4, 60, 60, $FFFFFFFF);

  Modifier := $3A6D4916; // arbitrary, just needs to be constant
  PixPtr := fInvisibleReveal.PixelPtr[0, 0];
  for y := 0 to 63 do
    for x := 0 to 63 do
    begin
      PixPtr^ := PixPtr^ and not (Modifier and $00FFFFFF);
      Modifier := (((Modifier and $FE000000) shr 25) or ((Modifier and $01FFFFFF) shl 7)) xor MODIFIER_CHANGE;
      Inc(PixPtr);
    end;
end;

procedure TL3DRenderer.RenderLevel(Dst: TBitmap32; Level: TL3DLevel);
begin
  RenderLevel(Dst, Level, AxisX, -1, Make3DPoint(-1, -1, -1), Make3DPoint(-1, -1, -1), false, -1);
end;

procedure TL3DRenderer.RenderLevel(Dst: TBitmap32; Level: TL3DLevel;
  HighlightAxis: TAxis; HighlightRank: Integer; HighlightBlock: T3DPoint;
  MarkBlock: T3DPoint; CornerMode: Boolean; HighlightLandIndex: Integer);
var
  InitialPosition: TPoint;
  NextXHorzOffset, NextXVertOffset: Integer;
  NextYHorzOffset, NextYVertOffset: Integer;
  NextZHorzOffset, NextZVertOffset: Integer;

  procedure CalculateOffsets;
  begin
    // This is a relic of when they were calculated here. It's going to be a bit messy to clean this up,
    // so for now, it just abstracts away the change.

    InitialPosition := fRenderParams.InitialPosition;
    NextXHorzOffset := fRenderParams.NextXHorzOffset;
    NextXVertOffset := fRenderParams.NextXVertOffset;
    NextYHorzOffset := fRenderParams.NextYHorzOffset;
    NextYVertOffset := fRenderParams.NextYVertOffset;
    NextZHorzOffset := fRenderParams.NextZHorzOffset;
    NextZVertOffset := fRenderParams.NextZVertOffset;

    if (Dst.Width <> fRenderParams.DstWidth) or (Dst.Height <> fRenderParams.DstHeight) then
      Dst.SetSize(fRenderParams.DstWidth, fRenderParams.DstHeight);
    Dst.Clear(0);
  end;

  function CheckIfSliceHighlightBlock(X, Y, Z: Integer): Boolean;
  var
    NewPoint: T3DPoint;
  begin
    NewPoint := ViewCoordsToDataCoords(Make3DPoint(X, Y, Z));
    Result := ((HighlightAxis = AxisX) and (NewPoint.X = HighlightRank)) or
              ((HighlightAxis = AxisY) and (NewPoint.Y = HighlightRank)) or
              ((HighlightAxis = AxisZ) and (NewPoint.Z = HighlightRank));
  end;

  procedure RenderBackground(Position: TPoint);
  // We simply cannot do this the same as L3D, because the perspective is entirely
  // different. Instead, we just have to do something that looks alright.
  var
    SeaBMP, SkyBMP: TBitmap32;

    LocalSea: TBitmap32;
    LocalSky: TBitmap32;

    x, y: Integer;
    XStartPos: TPoint;
    LeftSlope, RightSlope: Single;
    LeftHeight, MidHeight, RightHeight: Single;
    TargetPoint: TFloatPoint;

    IsInverse: Boolean;
    DrawPolygon: TPolygon;

    SkyWidth, SeaWidth: Integer;
    SeaScale: Integer;
  begin
    if rfSurroundSky in Level.RenderFlags then
      SkyWidth := 200
    else
      SkyWidth := 1024;

    if rfLargeSeaTexture in Level.RenderFlags then
      SeaWidth := 128
    else
      SeaWidth := 64;

    fSkyGraphic.ChangeIndex(Level.Sky, SkyWidth);
    SkyBMP := fSkyGraphic.Image;

    if SkyBMP.Width = 200 then
    begin
      SkyBMP.Rotate90;
      try
        y := 0;
        IsInverse := false;
        try
          while y < Dst.Height do
          begin
            x := 0;
            while x < Dst.Width do
            begin
              SkyBMP.DrawTo(Dst, x, y);
              x := x + SkyBMP.Width;
            end;

            y := y + SkyBMP.Height;

            SkyBMP.FlipVert; // not accurate to L3D, but what L3D does just wouldn't replicate well here
            IsInverse := not IsInverse;
          end;
        finally
          if IsInverse then
            SkyBMP.FlipVert;
        end;
      finally
        SkyBMP.Rotate270;
      end;
    end else begin
      fSeaGraphic.ChangeIndex(Level.Sea, SeaWidth);

      DrawPolygon := fRenderParams.BlockTopPolygon.Scale(4);

      LocalSea := TBitmap32.Create;
      LocalSky := TBitmap32.Create;
      SeaBMP := TBitmap32.Create;
      try
        LocalSea.SetSize(Ceil(DrawPolygon.TopRight.X), Ceil(DrawPolygon.BottomRight.Y));

        if rfLargeSeaTexture in Level.RenderFlags then
          SeaBMP.Assign(fSeaGraphic.Image)
        else begin
          SeaBMP.SetSize(64, 64);
          SeaBMP.Clear($FF000000);
          fSeaGraphic.Image.DrawTo(SeaBMP, 0, 0, Rect(0, 0, 64, 64));
        end;

        DrawPolygon := DrawPolygon.RotatePoints(fViewRotation);

        LocalSea.Clear($00000000);
        DrawTransformed(LocalSea, SeaBMP, DrawPolygon.TopLeft, DrawPolygon.TopRight, DrawPolygon.BottomRight, DrawPolygon.BottomLeft);

        LocalSea.DrawMode := dmTransparent;

        Position.X := Position.X - ((NextXHorzOffset + NextZHorzOffset) * 8);
        Position.Y := Position.Y - ((NextXVertOffset + NextZVertOffset) * 8);

        // This is for rendering the sky later
        TargetPoint := FloatPoint(Position.X + (DrawPolygon.TopLeft.X / 4), Position.Y);

        Position.X := Position.X - ((NextXHorzOffset + NextZHorzOffset) * 2);
        Position.Y := Position.Y - ((NextXVertOffset + NextZVertOffset) * 2);

        while (Position.Y < Dst.Height) do
        begin
          XStartPos := Position;
          while (Position.Y < Dst.Height) do
          begin
            LocalSea.DrawTo(Dst, Position.X, Position.Y);
            Position.X := Position.X + NextZHorzOffset * 4;
            Position.Y := Position.Y + NextZVertOffset * 4;
          end;
          Position := XStartPos;
          Position.X := Position.X + NextXHorzOffset * 4;
          Position.Y := Position.Y + NextXVertOffset * 4;
        end;

        LeftSlope := (DrawPolygon.TopLeft.Y - DrawPolygon.BottomLeft.Y) / (DrawPolygon.TopLeft.X - DrawPolygon.BottomLeft.X);
        RightSlope := (DrawPolygon.TopRight.Y - DrawPolygon.TopLeft.Y) / (DrawPolygon.TopRight.X - DrawPolygon.TopLeft.X);

        LeftHeight := Abs(TargetPoint.X * LeftSlope) + TargetPoint.Y + 8;
        RightHeight := Abs((Dst.Width - TargetPoint.X) * RightSlope) + TargetPoint.Y + 8;

        MidHeight := (LeftHeight + RightHeight) / 2;

        LocalSky.SetSize(512, 64);

        LocalSky.Clear($00000000);

        if (fViewRotation mod 2 = 0) then
          SkyBMP.DrawTo(LocalSky, 0, 0, Rect(0, 0, 512, 64))
        else
          SkyBMP.DrawTo(LocalSky, 0, 0, Rect(512, 0, 1024, 64));

        DrawPolygon := Polygon(FloatPoint(-4, -4),
                               FloatPoint(TargetPoint.X + 12, TargetPoint.Y - MidHeight + 4),
                               FloatPoint(TargetPoint.X + 12, TargetPoint.Y + 32),
                               FloatPoint(-4, LeftHeight + 32));
        DrawTransformed(Dst, LocalSky, DrawPolygon.TopLeft, DrawPolygon.TopRight, DrawPolygon.BottomRight, DrawPolygon.BottomLeft);

        LocalSky.Clear($00000000);

        if (fViewRotation mod 2 = 0) then
          SkyBMP.DrawTo(LocalSky, 0, 0, Rect(512, 0, 1024, 64))
        else
          SkyBMP.DrawTo(LocalSky, 0, 0, Rect(0, 0, 512, 64));

        DrawPolygon := Polygon(FloatPoint(TargetPoint.X, TargetPoint.Y - MidHeight + 4),
                               FloatPoint(Dst.Width + 12, -4),
                               FloatPoint(Dst.Width + 12, RightHeight + 32),
                               FloatPoint(TargetPoint.X, TargetPoint.Y + 32));
        DrawTransformed(Dst, LocalSky, DrawPolygon.TopLeft, DrawPolygon.TopRight, DrawPolygon.BottomRight, DrawPolygon.BottomLeft);
      finally
        LocalSea.Free;
        LocalSky.Free;
        SeaBMP.Free;
      end;

    end;
  end;

  procedure RenderLand({%H-}Position: TPoint);
  var
    LandMask: TBitmap32;
    LandBMP: TBitmap32;
    TempBMP: TBitmap32;
    LocalLand: TBitmap32;
    i: Integer;

    DrawPolygon: TPolygon;
    XShift: Single;

    function MakeGR32Polygon(aL3DPolygon: TL3DLandPolygon): TArrayOfFloatPoint;
    var
      i: Integer;
    begin
      SetLength(Result, aL3DPolygon.Count);
      for i := aL3DPolygon.Count-1 downto 0 do
      begin
        Result[i].X := aL3DPolygon[i].X * 64;
        Result[i].Y := aL3DPolygon[i].Z * 64;
      end;
    end;

    function MakeLandCacheIdentifier: String;
    var
      i, n: Integer;
    begin
      Result := 'LAND:' + IntToHex(Level.Land, 2);
      for i := 0 to Level.LandPolygons.Count-1 do
      begin
        Result := Result + ':';
        for n := 0 to Level.LandPolygons[i].Count-1 do
          Result := Result + IntToHex(Level.LandPolygons[i][n].X, 2) + IntToHex(Level.LandPolygons[i][n].Z, 2);
        Result := Result + '@' + IntToHex(Level.LandPolygons[i].FloorGraphic, 2) + IntToHex(Level.LandPolygons[i].FloorShade, 2);
      end;
      if HighlightLandIndex >= 0 then
        Result := Result + '@' + IntToStr(HighlightLandIndex);
    end;

    procedure RenderLandSet(Index: Integer);
    var
      Polygon: TL3DLandPolygon;
      LandSrcRect: TRect;
      LandSizeMod: Integer;
      X, Y: Integer;
    begin
      Polygon := Level.LandPolygons[Index];

      if rfSingleLandTexture in Level.RenderFlags then
        LandSrcRect := Rect(0, 0, 128, 128)
      else
        LandSrcRect := SizeRect(0, Polygon.FloorGraphic * 32, 128, 32);

      if Index = HighlightLandIndex then
        TempBMP.Clear($FFFF00FF)
      else begin
        if rfHideLand in Level.RenderFlags then Exit;

        TempBMP.Clear($FF000000);

        if rfHalfSizeLand in Level.RenderFlags then
          LandSizeMod := 1
        else
          LandSizeMod := 2;

        LandBMP.DrawMode := dmTransparent;

        Y := 0;
        while Y < 2048 do
        begin
          X := 0;
          while X < 2048 do
          begin
            LandBMP.DrawTo(TempBMP, Rect(X, Y, X + LandSrcRect.Width * LandSizeMod, Y + LandSrcRect.Height * LandSizeMod), LandSrcRect);
            Inc(X, LandSrcRect.Width * LandSizeMod);
          end;
          Inc(Y, LandSrcRect.Height * LandSizeMod);
        end;

        ApplyShade(TempBMP, SHADE_VALUES[Polygon.FloorShade]);
      end;

      LandMask.SetSize(2048, 2048);
      LandMask.Clear($00000000);

      PolygonFS(LandMask, MakeGR32Polygon(Polygon), $FFFFFFFF);
      LandMask.DrawTo(TempBMP);
      TempBMP.DrawTo(LocalLand);
    end;

  begin
    if Level.LandPolygons.Count = 0 then Exit;
    if RenderRangeStart.Y > 0 then Exit;

    fLandGraphic.ChangeIndex(Level.Land);
    LandBMP := fLandGraphic.Image;

    LandMask := TBitmap32.Create;
    LocalLand := TBitmap32.Create;
    TempBMP := TBitmap32.Create;
    try
      if Cache[MakeLandCacheIdentifier] <> nil then
        LocalLand.Assign(Cache[MakeLandCacheIdentifier])
      else begin
        TempBMP.SetSize(2048, 2048);
        TempBMP.DrawMode := dmTransparent;

        LocalLand.SetSize(2048, 2048);
        LocalLand.Clear($00000000);
        LandMask.DrawMode := dmCustom;
        LandMask.OnPixelCombine := CombineInverseMask;

        LandMask.SetSize(2048, 2048);

        for i := 0 to Min(7, Level.LandPolygons.Count-1) do
          RenderLandSet(i);

        Cache.CacheImage(MakeLandCacheIdentifier, LocalLand);
      end;

      DrawPolygon := fRenderParams.BlockTopPolygon;

      DrawPolygon.BottomLeft.X := DrawPolygon.BottomLeft.X + (NextZHorzOffset * 31);
      DrawPolygon.BottomLeft.Y := DrawPolygon.BottomLeft.Y + (NextZVertOffset * 31);
      DrawPolygon.TopRight.X := DrawPolygon.TopRight.X + (NextXHorzOffset * 31);
      DrawPolygon.TopRight.Y := DrawPolygon.TopRight.Y + (NextXVertOffset * 31);
      DrawPolygon.BottomRight.X := DrawPolygon.BottomRight.X + ((NextXHorzOffset + NextZHorzOffset) * 31);
      DrawPolygon.BottomRight.Y := DrawPolygon.BottomRight.Y + ((NextXVertOffset + NextZVertOffset) * 31);

      DrawPolygon := DrawPolygon.ShiftToOrigin;

      XShift := (InitialPosition.X + (fRenderParams.BlockTopPolygon.TopLeft.X)) - DrawPolygon.TopLeft.X;

      DrawPolygon := DrawPolygon.RotatePoints(-fViewRotation).Shift(XShift, InitialPosition.Y);
      DrawTransformed(Dst, LocalLand, DrawPolygon.TopLeft, DrawPolygon.TopRight, DrawPolygon.BottomRight, DrawPolygon.BottomLeft);
    finally
      LandMask.Free;
      LocalLand.Free;
      TempBMP.Free;
    end;
  end;

  procedure RenderBlocks(Position: TPoint);
  var
    XStartPos, YStartPos: TPoint;
    BlockBMP: TBitmap32;
    X, Y, Z: Integer;
    MarkerCenter: TPoint;
    MarkerDistanceToEdge: Integer;

    function GetBlock(X, Y, Z: Integer): TL3DBlock;
    var
      NewPoint: T3DPoint;
    begin
      NewPoint := ViewCoordsToDataCoords(Make3DPoint(X, Y, Z));

      Result := Level.Blocks[NewPoint.X, NewPoint.Y, NewPoint.Z];
    end;

    procedure DrawObjectStatic(Src: TBitmap32; SrcRect: TRect; DstOffset: TPoint; ShiftToBlock: Boolean; AltShiftMode: Boolean);
    var
      DstRect: TRect;
      YShift: Integer;
      Block: TL3DBlock;
      i: Integer;

      XCut: Integer;
    begin
      // AltShiftMode is the shifting used by interactive objects.

      YShift := Round((fViewPerspective - 1) / 4 * BlockRenderHeight);

      if ShiftToBlock then
      begin
        Block := GetBlock(X, Y, Z);

        if AltShiftMode then
        begin
          YShift := YShift + NextYVertOffset;

          for i := 0 to 3 do
            if Block.Segments[i] then
              Break
            else
              YShift := YShift - (NextYVertOffset div 4);
        end else if not Block.Full then
        begin
          for i := 3 downto 0 do
            if not Block.Segments[i] then
              Break
            else
              YShift := YShift + (NextYVertOffset div 4);
        end;
      end;

      DstRect := SizeRect(Position.X + DstOffset.X + (BlockRenderWidth div 8),
                          Position.Y + DstOffset.Y + (BlockRenderHeight * 5 div 16) + YShift,
                          BlockRenderWidth * 3 div 4,
                          BlockRenderHeight div 2);

      if SrcRect.Width = 32 then
      begin
        XCut := DstRect.Width div 4;
        DstRect.Left := DstRect.Left + XCut;
        DstRect.Right := DstRect.Right - XCut;
      end;

      if SrcRect.Height = 32 then
        DstRect.Top := DstRect.Top + (DstRect.Height div 2);

      Src.DrawTo(Dst, DstRect, SrcRect);
    end;

    procedure DrawObjectToFace(Src: TBitmap32; SrcRect: TRect; DstOffset: TPoint; Face: Integer; Rotation: Integer; Shade: Byte = $FF; ShiftToFace: Boolean = true); overload;
    var
      Block: TL3DBlock;
      DrawPolygon: TPolygon;
      i: Integer;
      TempBMP: TBitmap32;
    begin
      DrawPolygon := fRenderParams.BlockTopPolygon;
      Block := GetBlock(X, Y, Z);

      case Face of
        FACE_Y15: begin
                    if ShiftToFace and (not Block.Empty) then
                      for i := 0 to 3 do
                        if Block.Segments[i] then
                          Break
                        else
                          DrawPolygon := DrawPolygon.Shift(0, -NextYVertOffset div 4);
                    DrawPolygon := DrawPolygon.RotatePoints(Rotation);
                  end;
        FACE_Y0: begin
                   DrawPolygon := DrawPolygon.Shift(0, -NextYVertOffset);
                   DrawPolygon := DrawPolygon.RotatePoints(Rotation);
                 end;
        FACE_Z31: DrawPolygon := Polygon(DrawPolygon.BottomLeft, DrawPolygon.BottomRight,
                                         FloatPoint(DrawPolygon.BottomRight.X, DrawPolygon.BottomRight.Y + (BlockRenderHeight div 2)),
                                         FloatPoint(DrawPolygon.BottomLeft.X, DrawPolygon.BottomLeft.Y + (BlockRenderHeight div 2)));
        FACE_X31: DrawPolygon := Polygon(DrawPolygon.BottomRight, DrawPolygon.TopRight,
                                         FloatPoint(DrawPolygon.TopRight.X, DrawPolygon.TopRight.Y + (BlockRenderHeight div 2)),
                                         FloatPoint(DrawPolygon.BottomRight.X, DrawPolygon.BottomRight.Y + (BlockRenderHeight div 2)));
        FACE_Z0:  DrawPolygon := Polygon(DrawPolygon.TopLeft, DrawPolygon.TopRight,
                                         FloatPoint(DrawPolygon.TopRight.X, DrawPolygon.TopRight.Y + (BlockRenderHeight div 2)),
                                         FloatPoint(DrawPolygon.TopLeft.X, DrawPolygon.TopLeft.Y + (BlockRenderHeight div 2)));
        FACE_X0:  DrawPolygon := Polygon(DrawPolygon.BottomLeft, DrawPolygon.TopLeft,
                                         FloatPoint(DrawPolygon.TopLeft.X, DrawPolygon.TopLeft.Y + (BlockRenderHeight div 2)),
                                         FloatPoint(DrawPolygon.BottomLeft.X, DrawPolygon.BottomLeft.Y + (BlockRenderHeight div 2)));

        // Yes, this means the back faces don't get flipped... which is exactly how L3D behaves here.
      end;

      if (SrcRect = Src.BoundsRect) and (SrcRect.Width = 64) and (SrcRect.Height = 64) then // this probably means it's been pre-generated fully
        DrawTransformed(Dst, Src, DrawPolygon.Shift(Position.X + DstOffset.X, Position.Y + DstOffset.Y), Shade)
      else begin
        TempBMP := TBitmap32.Create;
        try
          TempBMP.SetSize(64, 64);
          TempBMP.Clear(0);
          Src.DrawTo(TempBMP, 32 - (SrcRect.Width div 2), 64 - SrcRect.Height, SrcRect);
          DrawTransformed(Dst, TempBMP, DrawPolygon.Shift(Position.X + DstOffset.X, Position.Y + DstOffset.Y), Shade);
        finally
          TempBMP.Free;
        end;
      end;
    end;

    procedure DrawObjectToFace(Src: TBitmap32; SrcRect: TRect; Face: Integer; Rotation: Integer; Shade: Byte = $FF; ShiftToFace: Boolean = true); overload;
    begin
      DrawObjectToFace(Src, SrcRect, Point(0, 0), Face, Rotation, Shade, ShiftToFace);
    end;

    procedure RenderObjectPreBlock(X, Y, Z: Integer);
    var
      NewPoint: T3DPoint;
      Obj: TL3DObject;

      SrcRect: TRect;

      LocalRotation: Integer;
      FaceIndex: Integer;
    begin
      NewPoint := ViewCoordsToDataCoords(Make3DPoint(X, Y, Z));
      Obj := Level.Objects[NewPoint.X, NewPoint.Y, NewPoint.Z];

      SrcRect := SizeRect(0, 0, 64, 64);

      case Obj.ObjectType of
        otSign: begin
                  LocalRotation :=  (fViewRotation - Obj.Orientation + 4) mod 4;
                  if (LocalRotation mod 2 = 0) then LocalRotation := (LocalRotation + 2) mod 4;
                  if LocalRotation in [1, 2] then Exit;

                  fSignSet.ChangeIndex(Level.Sign);

                  case Obj.GraphicIndex of
                    0..1: SrcRect := SizeRect(0, Obj.GraphicIndex * 32 + 64, 64, 32);
                    2..3: SrcRect := SizeRect(0, Obj.GraphicIndex * 32 + 128, 64, 32);
                    4..7: SrcRect := SizeRect(0, Obj.GraphicIndex * 64 + 256, 64, 64);
                  end;

                  case LocalRotation of
                    1: FaceIndex := FACE_Z31;
                    2: FaceIndex := FACE_X31;
                    3: FaceIndex := FACE_Z0;
                    else FaceIndex := FACE_X0;
                  end;

                  DrawObjectToFace(fSignSet.Image, SrcRect, FaceIndex, 0);
                end;
        otWall: begin
                   if Obj.Orientation = 4 then Exit; // upwards

                   LocalRotation :=  (fViewRotation - Obj.Orientation + 4) mod 4;
                   if (LocalRotation mod 2 = 0) then LocalRotation := (LocalRotation + 2) mod 4;
                   if (LocalRotation in [1, 2]) and (Obj.Orientation < 4) then Exit;

                   fWallSet.ChangeIndex(Level.Walls);

                   SrcRect := SizeRect(0, 64 * Obj.GraphicIndex, 64, 64);

                   if Obj.Orientation = 5 then
                   begin
                     FaceIndex := FACE_Y0;
                   end else begin
                     case LocalRotation of
                       1: FaceIndex := FACE_Z31;
                       2: FaceIndex := FACE_X31;
                       3: FaceIndex := FACE_Z0;
                       else FaceIndex := FACE_X0;
                     end;
                     LocalRotation := 0;
                   end;

                   DrawObjectToFace(fWallSet.Image, SrcRect, FaceIndex, LocalRotation, $FF);
                 end;
      end;
    end;

    procedure RenderObjectPostBlock(X, Y, Z: Integer);
    var
      NewPoint: T3DPoint;
      Obj: TL3DObject;

      Src: TBitmap32;
      SrcRect: TRect;
      DstOffset: TPoint;

      XShiftDir, ZShiftDir, ShiftTemp: Integer;

      LocalRotation: Integer;

      FaceIndex: Integer;

      procedure DrawPairingIcon(aNumber: Integer);
      var
        Center: TFloatPoint;
        SrcRect, DstRect: TRect;
        ModSize: Single;
        i: Integer;
        Block: TL3DBlock;
      begin
        Center := fRenderParams.BlockTopPolygon.Shift(Position.X, Position.Y).CenterPoint;

        Block := GetBlock(X, Y, Z);
        if (Block.Empty) or (Obj.IsReceiver and (Level.InteractiveObject = 8 {Rope Slide})) then
          Center.Y := Center.Y - (NextYVertOffset / 2)
        else begin
          for i := 0 to 3 do
            if not Block.Segments[i] then
              Center.Y := Center.Y - (NextYVertOffset / 4)
            else
              Break;
        end;

        SrcRect := SizeRect(aNumber * 12, 0, 12, 12);
        ModSize := BlockRenderWidth / 6;
        DstRect := Rect(Round(Center.X - ModSize), Round(Center.Y - ModSize), Round(Center.X + ModSize), Round(Center.Y + ModSize));
        fPairingIcons.DrawTo(Dst, DstRect, SrcRect);
      end;

    begin
      NewPoint := ViewCoordsToDataCoords(Make3DPoint(X, Y, Z));
      Obj := Level.Objects[NewPoint.X, NewPoint.Y, NewPoint.Z];

      SrcRect := SizeRect(0, 0, 64, 64);
      DstOffset := Point(0, 0);

      case Obj.ObjectType of
        otInteractive: begin
                         fTrapGraphic.ChangeIndex(Level.InteractiveObject);
                         Src := fTrapGraphic.Image;

                         case Level.InteractiveObject of
                           0, 7: begin
                                   // Flamethrower, laser
                                   SrcRect := SizeRect(0, 128, 64, 32);
                                   LocalRotation :=  (fViewRotation + Obj.Orientation + 1) mod 4;
                                   if (LocalRotation mod 2 = 0) then LocalRotation := (LocalRotation + 2) mod 4;
                                   DrawObjectToFace(Src, SrcRect, Point(0, NextYVertOffset div 2), FACE_Y0, LocalRotation, $FF, false);
                                 end;
                           1, 2, 6: begin
                                      // Squasher, beartrap, wtf trap
                                      SrcRect := SizeRect(0, 0, 64, 64);
                                      DrawObjectStatic(Src, SrcRect, DstOffset, true, true);
                                    end;
                           3, 4, 5: begin
                                      // Trampoline, spring, teleporter
                                      if (Level.InteractiveObject <> 4) or (Obj.IsReceiver) then
                                      begin
                                        if (Level.InteractiveObject = 3) and Obj.IsBlueTrampoline then
                                          SrcRect := SizeRect(0, 256, 64, 64)
                                        else
                                          SrcRect := SizeRect(0, 0, 64, 64);

                                        DrawObjectToFace(Src, SrcRect, FACE_Y15, 0);

                                        if Level.InteractiveObject = 5 then // teleporter
                                          DrawPairingIcon(Obj.BidirectionalPairing)
                                        else if Level.InteractiveObject = 4 then // spring
                                          DrawPairingIcon(Obj.SinglePairing);
                                      end else begin
                                        SrcRect := SizeRect(0, 64, 64, 64);
                                        DrawObjectStatic(Src, SrcRect, DstOffset, true, true);
                                        DrawPairingIcon(Obj.SinglePairing);
                                      end;
                                    end;
                           8: begin
                                // Rope slide
                                SrcRect := SizeRect(0, 0, 64, 64);

                                if Obj.IsReceiver then
                                begin
                                  DstOffset.Y := fRenderParams.NextYVertOffset div 2;
                                  DrawObjectStatic(Src, SrcRect, DstOffset, false, false);
                                end else
                                  DrawObjectStatic(Src, SrcRect, DstOffset, true, true);
                                DrawPairingIcon(Obj.SinglePairing);
                              end;
                         end;
                       end;
        otStandard: begin
                      fObjectSet.ChangeIndex(Level.ObjectSet);
                      Src := fObjectSet.Image;

                      case Obj.GraphicIndex of
                        0..3: SrcRect := SizeRect((Obj.GraphicIndex mod 2) * 32, (Obj.GraphicIndex div 2) * 64, 32, 64);
                        4..7: SrcRect := SizeRect(0, 128 + ((Obj.GraphicIndex - 4) * 64), 64, 64);
                        8..11: SrcRect := SizeRect((Obj.GraphicIndex mod 2) * 32, (((Obj.GraphicIndex - 8) div 2) * 64) + 384, 32, 64);
                        12..15: SrcRect := SizeRect(0, 512 + ((Obj.GraphicIndex - 12) * 32), 64, 32);
                        16..19: SrcRect := SizeRect((Obj.GraphicIndex mod 2) * 32, (((Obj.GraphicIndex - 16) div 2) * 32) + 640, 32, 32);
                      end;

                      DrawObjectStatic(Src, SrcRect, DstOffset, true, false);
                    end;
        otAnimation: begin
                       if Obj.GraphicIndex = 1 then
                       begin
                         fObjectSet.ChangeIndex(Level.ObjectSet);
                         Src := fObjectSet.Image;
                         SrcRect := SizeRect(0, 128, 64, 64);
                       end else begin
                         fAnimObjGraphic.ChangeIndex(Level.AnimatedObject);
                         Src := fAnimObjGraphic.Image;

                         if Obj.Split = osWhole then
                         begin
                           SrcRect := SizeRect(0, 0, 64, 64);
                           case Obj.Orientation of
                             2: begin XShiftDir := 1; ZShiftDir := 1; end;
                             3: begin XShiftDir := 1; ZShiftDir := -1; end;
                             else begin XShiftDir := 0; ZShiftDir := 0; end;
                           end;
                         end else begin
                           if Obj.Split = osLeft then
                             SrcRect := SizeRect(0, 0, 32, 64)
                           else
                             SrcRect := SizeRect(32, 0, 32, 64);

                           case Obj.Orientation mod 4 of
                             1: begin XShiftDir := -1; ZShiftDir := 1; end;
                             2: begin XShiftDir := 1; ZShiftDir := 1; end;
                             3: begin XShiftDir := 1; ZShiftDir := -1; end;
                             else begin XShiftDir := -1; ZShiftDir := -1; end;
                           end;
                         end;

                         LocalRotation := fViewRotation;
                         while LocalRotation > 0 do
                         begin
                           ShiftTemp := ZShiftDir;
                           ZShiftDir := XShiftDir;
                           XShiftDir := -ShiftTemp;
                           Dec(LocalRotation);
                         end;

                         DstOffset := Point((fRenderParams.NextXHorzOffset * XShiftDir div 2) + (fRenderParams.NextZHorzOffset * ZShiftDir div 2),
                                            (fRenderParams.NextXVertOffset * XShiftDir div 2) + (fRenderParams.NextZVertOffset * ZShiftDir div 2));
                       end;

                       DrawObjectStatic(Src, SrcRect, DstOffset, true, false);
                     end;
        otSign: begin
                  LocalRotation :=  (fViewRotation - Obj.Orientation + 4) mod 4;
                  if (LocalRotation mod 2 = 0) then LocalRotation := (LocalRotation + 2) mod 4;
                  if LocalRotation in [0, 3] then Exit;

                  fSignSet.ChangeIndex(Level.Sign);

                  case Obj.GraphicIndex of
                    0..1: SrcRect := SizeRect(0, Obj.GraphicIndex * 32, 64, 32);
                    2..3: SrcRect := SizeRect(0, Obj.GraphicIndex * 32 + 64, 64, 32);
                    4..7: SrcRect := SizeRect(0, Obj.GraphicIndex * 64, 64, 64);
                  end;

                  case LocalRotation of
                    1: FaceIndex := FACE_Z31;
                    2: FaceIndex := FACE_X31;
                    3: FaceIndex := FACE_Z0;
                    else FaceIndex := FACE_X0;
                  end;

                  DrawObjectToFace(fSignSet.Image, SrcRect, FaceIndex, 0);
                end;
        otWall: begin
                   if Obj.Orientation = 5 then Exit; // downwards

                   LocalRotation :=  (fViewRotation - Obj.Orientation + 4) mod 4;
                   if (LocalRotation mod 2 = 0) then LocalRotation := (LocalRotation + 2) mod 4;
                   if (LocalRotation in [0, 3]) and (Obj.Orientation < 4) then Exit;

                   fWallSet.ChangeIndex(Level.Walls);

                   SrcRect := SizeRect(0, 64 * Obj.GraphicIndex, 64, 64);

                   if Obj.Orientation = 4 then
                   begin
                     FaceIndex := FACE_Y15;
                     LocalRotation := LocalRotation + 1;
                   end else begin
                     case LocalRotation of
                       1: FaceIndex := FACE_Z31;
                       2: FaceIndex := FACE_X31;
                       3: FaceIndex := FACE_Z0;
                       else FaceIndex := FACE_X0;
                     end;
                     LocalRotation := 0;
                   end;

                   DrawObjectToFace(fWallSet.Image, SrcRect, FaceIndex, LocalRotation, $FF);
                 end;
      end;
    end;

    procedure RenderCameras(X, Y, Z: Integer);
    var
      i: Integer;
      RealPoint: T3DPoint;
      SrcRect: TRect;
    begin
      RealPoint := ViewCoordsToDataCoords(Make3DPoint(X, Y, Z));

      if Level.PreviewCameraPivot = RealPoint then
      begin
        SrcRect := SizeRect(256, 0, 64, 64);
        DrawObjectStatic(fCameraGraphic, SrcRect, Point(0, 0), false, false);
      end else begin
        SrcRect := default(TRect);
        for i := 0 to 4 do
          if i = 4 then
            Exit
          else if Level.InitialCameraPositions[i].Position = RealPoint then
          begin
            SrcRect := SizeRect(i * 64, 0, 64, 64);
            Break;
          end;
        DrawObjectToFace(fCameraGraphic, SrcRect, FACE_Y15, (4 - Level.InitialCameraPositions[i].Orientation) + (4 - fViewRotation) + 3, $FF, false);
      end;
    end;

    function IsInsideKillBoundaries: Boolean;
    var
      RealPos: T3DPoint;
    begin
      RealPos := ViewCoordsToDataCoords(Make3DPoint(X, Y, Z));
      Result := (RealPos.X >= Level.BorderKills[FACE_X0]) and (RealPos.X < Level.BorderKills[FACE_X31]) and
                (RealPos.Z >= Level.BorderKills[FACE_Z0]) and (RealPos.Z < Level.BorderKills[FACE_Z31]) and
                (RealPos.Y < Level.BorderKills[FACE_Y15]);
    end;

  begin
    BlockBMP := TBitmap32.Create;
    try
      BlockBMP.DrawMode := dmTransparent;

      for X := 0 to 31 do
      begin
        XStartPos := Position;
        for Y := 0 to 15 do
        begin
          YStartPos := Position;
          for Z := 0 to 31 do
          begin
            if (X >= fRenderRangeStart.X) and (X < fRenderRangeEnd.X) and
               (Y >= fRenderRangeStart.Y) and (Y < fRenderRangeEnd.Y) and
               (Z >= fRenderRangeStart.Z) and (Z < fRenderRangeEnd.Z) then
            begin
              if (Y > 0) and (not CornerMode) then
                if (X = HighlightBlock.X) and (Y = HighlightBlock.Y) and (Z = HighlightBlock.Z) then
                begin
                  if IsInsideKillBoundaries then
                    fHighlightShade := $FFA0A0A0
                  else
                    fHighlightShade := $FFA05050;
                  fRenderParams.SelectionBottom.DrawTo(Dst, Position.X, Position.Y);
                end else if (X = MarkBlock.X) and (Y = MarkBlock.Y) and (Z = MarkBlock.Z) then
                begin
                  if IsInsideKillBoundaries then
                    fHighlightShade := $FF20A020
                  else
                    fHighlightShade := $FF205010;
                  fRenderParams.SelectionBottom.DrawTo(Dst, Position.X, Position.Y);
                end;

              if rptObjects in RenderPieceTypes then
                RenderObjectPreBlock(X, Y, Z);

              if rptBlocks in RenderPieceTypes then
              begin
                if RenderBlock(BlockBMP, GetBlock(X, Y, Z), Level, Y = 0) then
                  BlockBMP.DrawTo(Dst, Position.X, Position.Y);
              end;

              if rptObjects in RenderPieceTypes then
                RenderObjectPostBlock(X, Y, Z);

              if rptCameras in RenderPieceTypes then
                RenderCameras(X, Y, Z);

              if (X = HighlightBlock.X) and (Y = HighlightBlock.Y) and (Z = HighlightBlock.Z) and (not CornerMode) then
              begin
                if IsInsideKillBoundaries then
                  fHighlightShade := $FFFFFFFF
                else
                  fHighlightShade := $FFFF8080;

                if Y > 0 then
                  fRenderParams.SelectionTop.DrawTo(Dst, Position.X, Position.Y)
                else
                  fRenderParams.SelectionTopFace.DrawTo(Dst, Position.X, Position.Y);
              end else if (X = MarkBlock.X) and (Y = MarkBlock.Y) and (Z = MarkBlock.Z) and (not CornerMode) then
              begin
                if IsInsideKillBoundaries then
                  fHighlightShade := $FF20FF20
                else
                  fHighlightShade := $FF208010;
                fRenderParams.SelectionTop.DrawTo(Dst, Position.X, Position.Y);
              end else if ((HighlightAxis = AxisX) and (X = HighlightRank)) or
                          ((HighlightAxis = AxisY) and (Y = HighlightRank)) or
                          ((HighlightAxis = AxisZ) and (Z = HighlightRank)) then
                if GridStyle <> gsNone then
                begin
                  if IsInsideKillBoundaries then
                    fHighlightShade := $FF606060
                  else
                    fHighlightShade := $FF603030;

                  case GridStyle of
                    gsStandard: fRenderParams.SelectionTopFace.DrawTo(Dst, Position.X, Position.Y);
                    gsCorners: fRenderParams.SelectionTopCorners.DrawTo(Dst, Position.X, Position.Y);
                  end;
                end;

              if CornerMode and (X = HighlightBlock.X) and (Y = HighlightBlock.Y) and (Z = HighlightBlock.Z) then
              begin
                MarkerCenter := Point(Round(Position.X + fRenderParams.BlockTopPolygon.TopLeft.X), Position.Y);
                MarkerDistanceToEdge := BlockRenderWidth div 8;
                Dst.FillRect(MarkerCenter.X - MarkerDistanceToEdge, MarkerCenter.Y - MarkerDistanceToEdge,
                             MarkerCenter.X + MarkerDistanceToEdge, MarkerCenter.Y + MarkerDistanceToEdge,
                             $FF000000);
                Dst.FillRect(MarkerCenter.X - MarkerDistanceToEdge + 1, MarkerCenter.Y - MarkerDistanceToEdge + 1,
                             MarkerCenter.X + MarkerDistanceToEdge - 1, MarkerCenter.y + MarkerDistanceToEdge - 1,
                             $FFFFFFFF);
              end
            end;

            Position.X := Position.X + NextZHorzOffset;
            Position.Y := Position.Y + NextZVertOffset;
          end;

          Position := YStartPos;
          Position.X := Position.X + NextYHorzOffset;
          Position.Y := Position.Y + NextYVertOffset;
        end;

        Position := XStartPos;
        Position.X := Position.X + NextXHorzOffset;
        Position.Y := Position.Y + NextXVertOffset;
      end;
    finally
      BlockBMP.Free;
    end;
  end;

begin
  CalculateOffsets;
  // Order is important!
  if rptSeaSky in RenderPieceTypes then RenderBackground(InitialPosition{%H-}); // Sky and sea. InitialPosition is initialized in CalculateOffsets but compiler doesn't recognize this.
  if rptLand in RenderPieceTypes then RenderLand(InitialPosition); // Vertex-based land
  RenderBlocks(InitialPosition); // This includes everything with block-based positioning. RenderPieceTypes flags are checked inside RenderBlocks.
end;

function TL3DRenderer.CalculateBlockFromImagePos(ImagePos: TPoint;
  LockAxis: TAxis; LockRank: Integer; SelectingCorner: Boolean): T3DPoint;
  function CalculateBlockBounds(X, Y, Z: Integer): TRect;
  begin
    Result.Left := fRenderParams.InitialPosition.X +
                   (X * fRenderParams.NextXHorzOffset) +
                   (Y * fRenderParams.NextYHorzOffset) +
                   (Z * fRenderParams.NextZHorzOffset);

    Result.Top := fRenderParams.InitialPosition.Y +
                  (X * fRenderParams.NextXVertOffset) +
                  (Y * fRenderParams.NextYVertOffset) +
                  (Z * fRenderParams.NextZVertOffset);

    Result.Left := Result.Left + (BlockRenderWidth div 6);

    if LockAxis <> AxisY then
      Result.Top := Result.Top - (BlockRenderHeight div 6);

    Result.Right := Result.Left + (BlockRenderWidth * 4 div 6) - 1;
    Result.Bottom := Result.Top + (BlockRenderHeight * 4 div 6);

    if SelectingCorner then Result.Offset(Round(BlockRenderWidth * fViewSkew), -BlockRenderHeight * 2 div 8);
  end;
var
  X, Y, Z: Integer;
begin
  Result := Make3DPoint(-1, -1, -1);

  for Y := 0 to 15 do
  begin
    if (LockAxis = AxisY) and (Y <> LockRank) then Continue;

    for Z := 0 to 31 do
    begin
      if (LockAxis = AxisZ) and (Z <> LockRank) then Continue;

      for X := 0 to 31 do
      begin
        if (LockAxis = AxisX) and (X <> LockRank) then Continue;

        if Types.PtInRect(CalculateBlockBounds(X, Y, Z), ImagePos) then
          Result := Make3DPoint(X, Y, Z);
      end;
    end;
  end;
end;

end.

