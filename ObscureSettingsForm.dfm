object FEditUnknowns: TFEditUnknowns
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Obscure / Unknown Settings'
  ClientHeight = 369
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    644
    369)
  PixelsPerInch = 96
  TextHeight = 13
  object lblFaceRenderLimit: TLabel
    Left = 16
    Top = 16
    Width = 85
    Height = 13
    Caption = 'Face Render Limit'
  end
  object ebFaceRenderLimit: TEdit
    Left = 120
    Top = 10
    Width = 49
    Height = 21
    MaxLength = 4
    NumbersOnly = True
    TabOrder = 0
    Text = '0'
    OnExit = ebFaceRenderLimitExit
  end
  object gbUncommonFlags: TGroupBox
    Left = 16
    Top = 48
    Width = 121
    Height = 121
    Caption = 'Uncommon Flags'
    TabOrder = 1
    object cbSolidSea: TCheckBox
      Left = 16
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Solid Color Sea'
      TabOrder = 0
    end
    object cbSolidSky: TCheckBox
      Tag = 1
      Left = 16
      Top = 35
      Width = 97
      Height = 17
      Caption = 'Solid Color Sky'
      TabOrder = 1
    end
    object cbInvisibleLand: TCheckBox
      Tag = 2
      Left = 16
      Top = 54
      Width = 97
      Height = 17
      Caption = 'Invisible Land'
      TabOrder = 2
    end
    object cbFun8Effect: TCheckBox
      Tag = 10
      Left = 16
      Top = 73
      Width = 97
      Height = 17
      Caption = '"Fun 8" Walls'
      TabOrder = 3
    end
    object cbShrinkLand: TCheckBox
      Tag = 11
      Left = 16
      Top = 92
      Width = 97
      Height = 17
      Caption = 'Shrink Land'
      TabOrder = 4
    end
  end
  object gbUnknownFlags: TGroupBox
    Left = 16
    Top = 184
    Width = 121
    Height = 177
    Caption = 'Unknown Flags'
    TabOrder = 2
    object cbUnknown011Cx20: TCheckBox
      Tag = 13
      Left = 16
      Top = 16
      Width = 97
      Height = 17
      Caption = '0x011C: 0x20'
      TabOrder = 0
    end
    object cbUnknown011Cx40: TCheckBox
      Tag = 14
      Left = 16
      Top = 35
      Width = 97
      Height = 17
      Caption = '0x011C: 0x40'
      TabOrder = 1
    end
    object cbUnknown0157x04: TCheckBox
      Tag = 18
      Left = 16
      Top = 54
      Width = 97
      Height = 17
      Caption = '0x0157: 0x04'
      TabOrder = 2
    end
    object cbUnknown0157x08: TCheckBox
      Tag = 19
      Left = 16
      Top = 73
      Width = 97
      Height = 17
      Caption = '0x0157: 0x08'
      TabOrder = 3
    end
    object cbUnknown0157x10: TCheckBox
      Tag = 20
      Left = 16
      Top = 92
      Width = 97
      Height = 17
      Caption = '0x0157: 0x10'
      TabOrder = 4
    end
    object cbUnknown0157x20: TCheckBox
      Tag = 21
      Left = 16
      Top = 111
      Width = 97
      Height = 17
      Caption = '0x0157: 0x20'
      TabOrder = 5
    end
    object cbUnknown0157x40: TCheckBox
      Tag = 22
      Left = 16
      Top = 130
      Width = 97
      Height = 17
      Caption = '0x0157: 0x40'
      TabOrder = 6
    end
    object cbUnknown0157x80: TCheckBox
      Tag = 23
      Left = 16
      Top = 149
      Width = 97
      Height = 17
      Caption = '0x0157: 0x80'
      TabOrder = 7
    end
  end
  object btnCancel: TButton
    Left = 561
    Top = 336
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    ExplicitTop = 352
  end
  object btnOK: TButton
    Left = 480
    Top = 336
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 4
    OnClick = btnOKClick
    ExplicitTop = 352
  end
end
