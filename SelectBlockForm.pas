unit SelectBlockForm;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  Misc,
  L3DLevel, L3DRender,
  GR32, GR32_Image, GR32_Layers,
  EditMetablockForm,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

const
  BLOCK_IMAGE_SIZE = 48;
  PADDING_SIZE = 8;

type

  { TFSelectBlock }

  TFSelectBlock = class(TForm)
    lblBlockIndex: TLabel;
    lblBlockProperties: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure img32MouseMove(Sender: TObject; {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer; {%H-}Layer: TCustomLayer);
    procedure img32MouseDown(Sender: TObject; Button: TMouseButton; {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer; {%H-}Layer: TCustomLayer);
  private
    fRenderer: TL3DRenderer;
    fLevel: TL3DLevel;
    fTempSL: TStringList;
    fLastMouseoverBlockIndex: Integer;

    img32: array[0..63] of TImage32;

    procedure DrawBlocks;
    procedure SetInfo(aIndex: Integer);
  public
    NewBlockIndex: Integer;
    RequirePurge: Boolean;

    procedure Prepare(aRenderer: TL3DRenderer; aLevel: TL3DLevel; aSelectedIndex: Integer);
  end;

implementation

{$R *.dfm}

{ TFSelectBlock }

procedure TFSelectBlock.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  ClientWidth := ((BLOCK_IMAGE_SIZE + PADDING_SIZE) * 8) + PADDING_SIZE;
  ClientHeight := ClientWidth + 24;
  for i := 0 to 63 do
  begin
    img32[i] := TImage32.Create(self);
    img32[i].Parent := self;
    img32[i].BitmapAlign := baCenter;
    img32[i].Color := clFuchsia;
    img32[i].Tag := i;
    img32[i].BoundsRect := SizeRect((BLOCK_IMAGE_SIZE + PADDING_SIZE) * (i mod 8) + PADDING_SIZE,
                                    (BLOCK_IMAGE_SIZE + PADDING_SIZE) * (i div 8) + PADDING_SIZE,
                                    BLOCK_IMAGE_SIZE, BLOCK_IMAGE_SIZE);
    img32[i].OnMouseDown := img32MouseDown;
    img32[i].OnMouseMove := img32MouseMove;
    img32[i].Bitmap.DrawMode := dmTransparent;
    img32[i].ScaleMode := smScale;
  end;

  fTempSL := TStringList.Create;
  fTempSL.Delimiter := ',';
  fTempSL.StrictDelimiter := true;
end;

procedure TFSelectBlock.FormDestroy(Sender: TObject);
begin
  fTempSL.Free;
end;

procedure TFSelectBlock.img32MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer; Layer: TCustomLayer);
begin
  if not (Sender is TImage32) then Exit;
  SetInfo(TImage32(Sender).Tag);
end;

procedure TFSelectBlock.img32MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  F: TFEditMetablock;
begin
  if not (Sender is TImage32) then Exit;
  if Button = mbLeft then
  begin
    NewBlockIndex := TImage32(Sender).Tag;
    ModalResult := mrOk;
  end else if Button = mbRight then
  begin
    F := TFEditMetablock.Create(self);
    try
      F.Initialize(fLevel, fLevel.MetaBlocks[TImage32(Sender).Tag], fRenderer.TextureSetBitmap);
      if F.ShowModal = mrOk then
      begin
        fLevel.PurgeBlockFaceGraphics;
        fRenderer.PurgeCache;
        Prepare(fRenderer, fLevel, NewBlockIndex);
        RequirePurge := true;
      end;
    finally
      F.Free;
    end;
  end;
end;

procedure TFSelectBlock.DrawBlocks;
var
  i: Integer;
  Block: TL3DBlock;
begin
  Block.RawData := $000F; // all segments on
  for i := 0 to 63 do
  begin
    Block.Graphic := i;
    img32[i].Bitmap.BeginUpdate;
    try
      fRenderer.RenderBlock(img32[i].Bitmap, Block, fLevel, false);
      if img32[i].Bitmap.Width > 0 then
        img32[i].Scale := img32[i].Width / img32[i].Bitmap.Width;
    finally
      img32[i].Bitmap.EndUpdate;
      img32[i].Changed;
    end;
  end;
end;

procedure TFSelectBlock.SetInfo(aIndex: Integer);
var
  S: String;
  Flags: TL3DBlockFlags;
begin
  if fLastMouseoverBlockIndex = aIndex then Exit;

  S := 'Block ' + IntToStr(aIndex);
  case aIndex of
    0: S := S + ' (Entrance)';
    1: S := S + ' (Exit)';
    2: S := S + ' (Splitter)';
    3: S := S + ' (No-Render)';
    4: S := S + ' (Invisible)';
    5, 6, 7, 8: S := S + ' (One Way)';
  end;

  lblBlockIndex.Caption := S;

  fTempSL.Clear;
  if (aIndex >= 0) and (aIndex <= 63) then // just in case
  begin
    Flags := fLevel.MetaBlocks[aIndex].Flags;
    if bfSteel in Flags then fTempSL.Add('Steel');
    if bfTopLiquid in Flags then
    begin
      if rfZapInsteadOfDrown in fLevel.RenderFlags then
        fTempSL.Add('Electrified')
      else
        fTempSL.Add('Liquid');
    end;
    if bfSlippery in Flags then fTempSL.Add('Slippery');
    if bfNoSplat in Flags then fTempSL.Add('Anti-Splat');
    if bfNonSolid in Flags then
    begin
      if bfNonSolidToCamera in Flags then
        fTempSL.Add('Non-Solid')
      else
        fTempSL.Add('Non-Solid (Lemmings only)');
    end else if bfNonSolidToCamera in Flags then
      fTempSL.Add('Non-Solid (Camera only)');
    {$ifdef DEBUG_BUILD}
    if bfUnknown in Flags then
      fTempSL.Add('Unknown Flag');
    {$endif}
  end;

  if fTempSL.Count = 0 then
    lblBlockProperties.Caption := '(No special properties)'
  else
    lblBlockProperties.Caption := fTempSL.DelimitedText;

  fLastMouseoverBlockIndex := aIndex;
end;

procedure TFSelectBlock.Prepare(aRenderer: TL3DRenderer; aLevel: TL3DLevel;
  aSelectedIndex: Integer);
begin
  NewBlockIndex := aSelectedIndex;
  fLastMouseoverBlockIndex := -1;
  fRenderer := aRenderer;
  fLevel := aLevel;
  DrawBlocks;
  SetInfo(NewBlockIndex);
end;

end.

