unit L3DGraphics;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  GR32, Misc,
  Classes, SysUtils;

  procedure LoadL3DGraphic(aDst: TBitmap32; aSrcFilename: String; aOffset: Int64; aWidth, aHeight: Integer; EmptySpaceCompressed: Boolean); overload;
  procedure LoadL3DGraphic(aDst: TBitmap32; aSrcStream: TStream; aWidth, aHeight: Integer; EmptySpaceCompressed: Boolean); overload;
  procedure SaveL3DGraphic(aSrc: TBitmap32; aDstFilename: String; aTransparentColor: TColor32 = $00000000); overload;
  procedure SaveL3DGraphic(aSrc: TBitmap32; aDstStream: TStream; aTransparentColor: TColor32 = $00000000); overload;

  procedure SavePaletteImage(aDstFilename: String); overload;
  procedure SavePaletteImage(aDstStream: TStream); overload;

var
  L3DPalette: array[0..255] of TColor32;

implementation

uses
  L3DDecompressor;

procedure EnsurePalette;
var
  F: TFileStream;
  i: Integer;
  b: Byte;
  C: TColor32;
begin
  if (L3DPalette[1] and $FF000000) <> 0 then Exit;

  F := TFileStream.Create(AppPath + 'GFX' + PathDelim + 'LM3D.PAL', fmOpenRead);
  try
    C := 0;
    b := 0;
    for i := 0 to (256 * 3) - 1 do
    begin
      F.Read(b, 1);
      C := (C shl 8) or (b shl 2);
      if (i mod 3) = 2 then
      begin
        L3DPalette[i div 3] := C or $FF000000;
        C := 0;
      end;
    end;

    // Special case:
    L3DPalette[0] := $00000000;
  finally
    F.Free;
  end;
end;

procedure SavePaletteImage(aDstFilename: String);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aDstFilename, fmCreate);
  try
    SavePaletteImage(F);
  finally
    F.Free;
  end;
end;

procedure SavePaletteImage(aDstStream: TStream);
var
  BMP: TBitmap32;
  n: Integer;
begin
  EnsurePalette;

  BMP := TBitmap32.Create;
  try
    BMP.SetSize(256, 1);

    for n := 0 to 255 do
      BMP[n, 0] := L3DPalette[n];

    BMP.SaveToStream(aDstStream);
  finally
    BMP.Free;
  end;
end;

procedure LoadL3DGraphic(aDst: TBitmap32; aSrcFilename: String; aOffset: Int64;
  aWidth, aHeight: Integer; EmptySpaceCompressed: Boolean);
var
  F: TFileStream;
  Header: array[0..2] of AnsiChar;
begin
  Header := '   ';
  F := TFileStream.Create(aSrcFilename, fmOpenRead);
  try
    F.Read(Header, 3);
    if Header <> 'RNC' then
    begin
      F.Position := aOffset;
      LoadL3DGraphic(aDst, F, aWidth, aHeight, EmptySpaceCompressed);
      Exit;
    end;
  finally
    F.Free;
  end;

  // Note that we will only get to here if the header is 'RNC'.
  // This is a bit confusing, but avoids messiness with the filestream.
  DecompressFile(aSrcFilename);
  LoadL3DGraphic(aDst, aSrcFilename, aOffset, aWidth, aHeight, EmptySpaceCompressed);
end;

procedure SaveL3DGraphic(aSrc: TBitmap32; aDstFilename: String; aTransparentColor: TColor32 = $00000000);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aDstFilename, fmCreate);
  try
    SaveL3DGraphic(aSrc, F, aTransparentColor);
  finally
    F.Free;
  end;
end;

procedure SaveL3DGraphic(aSrc: TBitmap32; aDstStream: TStream; aTransparentColor: TColor32 = $00000000);
var
  x, y: Integer;
  b: Byte;

  function FindNearestColorIndex(C: TColor32): Integer;
  var
    i: Integer;
    Similarity: Integer;
    ClosestSimilarity: Integer;

    function CalculateDifference(aTarget: TColor32): Integer;
    var
      RDiff, GDiff, BDiff: Integer;
    begin
      RDiff := ((aTarget shr 16) and $FF) - ((C shr 16) and $FF);
      GDiff := ((aTarget shr 8) and $FF) - ((C shr 8) and $FF);
      BDiff := (aTarget and $FF) - (C and $FF);
      Result := (RDiff * RDiff) + (GDiff * GDiff) + (BDiff * BDiff);
    end;

  begin
    Result := 0;

    if ((C and $FF000000) <> 0) and (C <> aTransparentColor) then
    begin
      ClosestSimilarity := $7FFFFFFF;
      for i := 1 to 255 do
      begin
        Similarity := CalculateDifference(L3DPalette[i]);
        if Similarity >= ClosestSimilarity then Continue;
        Result := i;
        if Similarity = 0 then Exit;
        ClosestSimilarity := Similarity;
      end;
    end;
  end;

begin
  for y := 0 to aSrc.Height-1 do
    for x := 0 to aSrc.Width-1 do
    begin
      b := FindNearestColorIndex(aSrc[x, y]);
      aDstStream.Write(b, 1);
    end;
end;

procedure LoadL3DEmptySpaceCompressedGraphic(aDst: TBitmap32; aSrcStream: TStream;
  aWidth, aHeight: Integer);
var
  BaseSize: Integer;
  x, y: Integer;
  FinishLineAt: Integer;
  TempBMP: TBitmap32;
  b: Byte;

  procedure ExpandDst;
  begin
    TempBMP.Assign(aDst);
    TempBMP.DrawMode := dmOpaque;
    aDst.SetSize(aDst.Width, aDst.Height + BaseSize);
    aDst.Clear(0);
    TempBMP.DrawTo(aDst, 0, 0);
  end;

begin
  if aHeight < 0 then
  begin
    BaseSize := aSrcStream.Size div aWidth;
    aDst.SetSize(aWidth, BaseSize);
    TempBMP := TBitmap32.Create;
  end else begin
    aDst.SetSize(aWidth, aHeight);
    TempBMP := nil;
  end;

  aDst.BeginUpdate;
  try
    aDst.Clear(0);

    x := 0;
    y := 0;
    b := 0;
    FinishLineAt := 0;

    while aSrcStream.Position < aSrcStream.Size do
    begin
      if x = 0 then
      begin
        aSrcStream.Read(b, 1);
        x := b;
        aSrcStream.Read(b, 1);
        FinishLineAt := aWidth - b;
      end else if x >= FinishLineAt then
      begin
        x := 0;
        Inc(y);
        if (y >= aDst.Height) then
        begin
          if aHeight < 0 then
            ExpandDst
          else
            Exit;
        end;
      end else begin
        aSrcStream.Read(b, 1);
        aDst[x, y] := L3DPalette[b];
        Inc(x);
      end;
    end;

    if aHeight < 0 then
    begin
      TempBMP.Assign(aDst);
      TempBMP.DrawMode := dmOpaque;
      aDst.SetSize(aWidth, Y);
      TempBMP.DrawTo(aDst, 0, 0);
    end;
  finally
    aDst.EndUpdate;
    aDst.Changed;

    if TempBMP <> nil then
      TempBMP.Free;
  end;
end;

procedure LoadL3DGraphic(aDst: TBitmap32; aSrcStream: TStream; aWidth,
  aHeight: Integer; EmptySpaceCompressed: Boolean);
var
  X, Y: Integer;
  b: Byte;
begin
  EnsurePalette;

  if EmptySpaceCompressed then
  begin
    LoadL3DEmptySpaceCompressedGraphic(aDst, aSrcStream, aWidth, aHeight);
    Exit;
  end;

  if (aHeight < 0) then
    aHeight := (aSrcStream.Size - aSrcStream.Position) div aWidth;

  b := 0;
  aDst.SetSize(aWidth, aHeight);
  for Y := 0 to aHeight-1 do
  begin
    for X := 0 to aWidth-1 do
    begin
      aSrcStream.Read(b, 1);
      aDst[X, Y] := L3DPalette[b];
    end;
  end;
end;

end.

