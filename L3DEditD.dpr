program L3DEditD;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

{$R 'resources.res' 'utils\resources.rc'}

uses
  Vcl.Forms,
  L3DEditMain in 'L3DEditMain.pas' {Form1},
  BitmapCache in 'BitmapCache.pas',
  Misc in 'Misc.pas',
  IndexedGraphic in 'IndexedGraphic.pas',
  L3DGraphics in 'L3DGraphics.pas',
  L3DDecompressor in 'L3DDecompressor.pas',
  L3DLevelData in 'L3DLevelData.pas',
  L3DLevel in 'L3DLevel.pas',
  L3DRender in 'L3DRender.pas',
  EditMetablockForm in 'EditMetablockForm.pas' {FEditMetablock},
  GraphicExtractWidthForm in 'GraphicExtractWidthForm.pas' {FExtractGraphicWidth},
  ReorderLevelsForm in 'ReorderLevelsForm.pas' {FReorderLevels},
  SelectBlockForm in 'SelectBlockForm.pas' {FSelectBlock},
  ObscureSettingsForm in 'ObscureSettingsForm.pas' {FEditUnknowns};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFL3DEdit, FL3DEdit);
  Application.Run;
end.
