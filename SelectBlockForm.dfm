object FSelectBlock: TFSelectBlock
  Left = 425
  Top = 142
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  ClientHeight = 109
  ClientWidth = 274
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    274
    109)
  PixelsPerInch = 96
  TextHeight = 13
  object lblBlockIndex: TLabel
    Left = 8
    Top = 88
    Width = 104
    Height = 16
    Anchors = [akLeft, akBottom]
    AutoSize = False
    Caption = 'Block 0'
    Color = clBtnFace
    ParentColor = False
  end
  object lblBlockProperties: TLabel
    Left = 120
    Top = 88
    Width = 144
    Height = 16
    Alignment = taRightJustify
    Anchors = [akLeft, akRight, akBottom]
    AutoSize = False
    Caption = 'Properties'
    Color = clBtnFace
    ParentColor = False
  end
end
