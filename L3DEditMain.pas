unit L3DEditMain;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  L3DLevel,
  L3DLevelData,
  L3DRender,
  GR32,
  GraphicExtractWidthForm, SelectBlockForm, ReorderLevelsForm, ObscureSettingsForm,
  Generics.Collections,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Windows, StrUtils,
  StdCtrls, ExtCtrls, ComCtrls, Menus, GR32_Image, GR32_Layers, Math, Types, UITypes,
  Messages;

const
  MAX_UNDO_HISTORY_STREAMS = 50; // keep in mind - two streams are used for each iteration. So, this MUST be an even number!

  KEYBOARD_CONTROL_KEYS = [VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9, VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3,
                           VK_NUMPAD4, VK_NUMPAD5, VK_NUMPAD6, VK_NUMPAD0, VK_DECIMAL, VK_DIVIDE,
                           VK_MULTIPLY, VK_ADD, VK_SUBTRACT];

type

  TMemoryStreamList = TObjectList<TMemoryStream>;

  { TFL3DEdit }

  TFL3DEdit = class(TForm)
    btnRotateObjectAntiClockwise: TButton;
    btnRotateObjectClockwise: TButton;
    btnRotateCameraClockwise: TButton;
    btnRotateCCW: TButton;
    btnPerspectiveIncrease: TButton;
    btnSelectedSliceMinus: TButton;
    btnRotateBlockClockwise: TButton;
    btnRotateBlockAntiClockwise: TButton;
    btnSkewLeft: TButton;
    btnRotateCW: TButton;
    btnPerspectiveDecrease: TButton;
    btnSkewRight: TButton;
    btnRenderRangeApply: TButton;
    btnSelectedSlicePlus: TButton;
    btnSelectMetablock: TButton;
    btnRotateCameraAnticlockwise: TButton;
    btnDeleteLand: TButton;
    cbBlockSlices1: TCheckBox;
    cbBlockSlices3: TCheckBox;
    cbBlockSlices2: TCheckBox;
    cbDisableMinimap: TCheckBox;
    cbFullHeightSkyTexture: TCheckBox;
    cbSlipperyBottom: TCheckBox;
    cbSlipperyLands: TCheckBox;
    cbSolidBottom: TCheckBox;
    cbLandPreset: TComboBox;
    cbLargeSeaTexture: TCheckBox;
    cbLargeLandTexture: TCheckBox;
    cbNonAnimatingSea: TCheckBox;
    cbObjectPreset: TComboBox;
    cbRenderObjects: TCheckBox;
    cbRenderLand: TCheckBox;
    cbRenderBlocks: TCheckBox;
    cbRenderCameras: TCheckBox;
    cbSignPreset: TComboBox;
    cbSeaPreset: TComboBox;
    cbAnimationPreset: TComboBox;
    cbDisableQuickJump: TCheckBox;
    cbTrapPreset: TComboBox;
    cbSkyPreset: TComboBox;
    cbWallsPreset: TComboBox;
    cbTheme: TComboBox;
    cbMusic: TComboBox;
    cbTextureSetPreset: TComboBox;
    cbDisableTransparency: TCheckBox;
    cbRenderSkySea: TCheckBox;
    cbBlockSlices0: TCheckBox;
    cbBlockShape: TComboBox;
    cbFixedPreviewCamera: TCheckBox;
    cbActiveCamera: TComboBox;
    cbZapLemmings: TCheckBox;
    cbObjectType: TComboBox;
    cbReceiver: TCheckBox;
    cbBoundary: TComboBox;
    cbLand: TComboBox;
    cbDisableMouseInput: TCheckBox;
    ebLandGraphic: TEdit;
    ebObjectGraphic: TEdit;
    ebRangeXHigh: TEdit;
    ebRangeYHigh: TEdit;
    ebRangeZHigh: TEdit;
    ebRangeYLow: TEdit;
    ebRangeZLow: TEdit;
    ebSea: TEdit;
    ebBlockers: TEdit;
    ebSeaMovementX: TEdit;
    ebSeaMovementZ: TEdit;
    ebTextureSet: TEdit;
    ebObjects: TEdit;
    ebSign: TEdit;
    ebSky: TEdit;
    ebInteractive: TEdit;
    ebWalls: TEdit;
    ebAnimation: TEdit;
    ebTurners: TEdit;
    ebBombers: TEdit;
    ebBuilders: TEdit;
    ebBashers: TEdit;
    ebMiners: TEdit;
    ebDiggers: TEdit;
    ebClimbers: TEdit;
    ebFloaters: TEdit;
    ebSaveRequirement: TEdit;
    ebTimeMinutes: TEdit;
    ebReleaseRate: TEdit;
    ebTimeSeconds: TEdit;
    ebLevelTitle: TEdit;
    ebComment: TEdit;
    ebLemmings: TEdit;
    ebLand: TEdit;
    ebRangeXLow: TEdit;
    ebMetablock: TEdit;
    imgBlock: TImage32;
    imgObject: TImage32;
    lblLandShade: TLabel;
    lblLandGraphic: TLabel;
    lblLandInstance: TLabel;
    lblBoundary: TLabel;
    lblRenderPieces: TLabel;
    lblObjectGraphic: TLabel;
    lblCamera: TLabel;
    lblMetablock: TLabel;
    lblSeaMovement: TLabel;
    lblSeaMovementX: TLabel;
    lblSeaMovementZ: TLabel;
    lblSelectedSliceIndex: TLabel;
    lblRenderRange: TLabel;
    lblCoords: TLabel;
    lblRenderRangeX: TLabel;
    lblRenderRangeY: TLabel;
    lblRenderRangeZ: TLabel;
    lblRenderRangeXTo: TLabel;
    lblRenderRangeYTo: TLabel;
    lblRenderRangeZTo: TLabel;
    levelImage: TImage32;
    lblSea: TLabel;
    lblTextureSet: TLabel;
    lblObjects: TLabel;
    lblSign: TLabel;
    lblSky: TLabel;
    lblInteractive: TLabel;
    lblWalls: TLabel;
    lblAnimation: TLabel;
    lblTheme: TLabel;
    lblBlockers: TLabel;
    lblMusic: TLabel;
    lblTurners: TLabel;
    lblBombers: TLabel;
    lblBuilders: TLabel;
    lblBashers: TLabel;
    lblMiners: TLabel;
    lblDiggers: TLabel;
    lblClimbers: TLabel;
    lblFloaters: TLabel;
    lblLemmings: TLabel;
    lblComment: TLabel;
    lblSaveRequirement: TLabel;
    lblTimeLimit: TLabel;
    lblReleaseRate: TLabel;
    lblTitle: TLabel;
    lblLand: TLabel;
    MainMenu1: TMainMenu;
    miGraphicComparison: TMenuItem;
    miRedo: TMenuItem;
    miUndo: TMenuItem;
    miEdit: TMenuItem;
    miGridNone: TMenuItem;
    miGridCorners: TMenuItem;
    miGridStandard: TMenuItem;
    miGridStyle: TMenuItem;
    miReorderLevels: TMenuItem;
    miFileSeperator1: TMenuItem;
    miImportBlockData: TMenuItem;
    miImportGraphicFile: TMenuItem;
    miGraphicFiles: TMenuItem;
    miImageBruteForce: TMenuItem;
    miResolutionHighest: TMenuItem;
    miResolutionHigh: TMenuItem;
    miResolutionMedium: TMenuItem;
    miResolutionLow: TMenuItem;
    miResolutionLowest: TMenuItem;
    miResolution: TMenuItem;
    miViewSeperator: TMenuItem;
    miZoomOut: TMenuItem;
    miZoomIn: TMenuItem;
    miView: TMenuItem;
    miCreateTestTextureset: TMenuItem;
    miLevelFlood: TMenuItem;
    miLevelScan: TMenuItem;
    miAboutDebugOptions: TMenuItem;
    miBackFaces: TMenuItem;
    miDebug: TMenuItem;
    miExtractGraphic: TMenuItem;
    miAbout: TMenuItem;
    miHelp: TMenuItem;
    miComFile: TMenuItem;
    miComL3DW: TMenuItem;
    miComL3DD2: TMenuItem;
    miComL3DD1: TMenuItem;
    miDecFile: TMenuItem;
    miComL3D: TMenuItem;
    miDecL3DW: TMenuItem;
    miDecL3DD2: TMenuItem;
    miDecL3DD1: TMenuItem;
    miDecL3D: TMenuItem;
    miCompress: TMenuItem;
    miDecompress: TMenuItem;
    miUtilities: TMenuItem;
    miQuit: TMenuItem;
    miFileSeperator2: TMenuItem;
    miSaveAs: TMenuItem;
    miSave: TMenuItem;
    miOpen: TMenuItem;
    miFile: TMenuItem;
    miNew: TMenuItem;
    pagesLevelComponent: TPageControl;
    pagesLevelInfo: TPageControl;
    pnBottomArea: TPanel;
    panelLevelInfo: TPanel;
    tabGeneral: TTabSheet;
    tabBlocks: TTabSheet;
    tabCameras: TTabSheet;
    tabObjects: TTabSheet;
    tabBoundaries: TTabSheet;
    tabLand: TTabSheet;
    tabStyles: TTabSheet;
    tabSkillset: TTabSheet;
    tbLandShade: TTrackBar;
    rgSelectedSlice: TRadioGroup;
    rgDrawMode: TRadioGroup;
    miEditSeperator: TMenuItem;
    miEditUnknowns: TMenuItem;
    ebObjectUnknown: TEdit;
    lblObjectUnknown: TLabel;
    miPaletteImage: TMenuItem;
    procedure btnDeleteLandClick(Sender: TObject);
    procedure btnPerspectiveDecreaseClick(Sender: TObject);
    procedure btnPerspectiveIncreaseClick(Sender: TObject);
    procedure btnRenderRangeApplyClick(Sender: TObject);
    procedure btnRotateBlockAntiClockwiseClick(Sender: TObject);
    procedure btnRotateBlockClockwiseClick(Sender: TObject);
    procedure btnRotateCameraAnticlockwiseClick(Sender: TObject);
    procedure btnRotateCameraClockwiseClick(Sender: TObject);
    procedure btnRotateCCWClic(Sender: TObject);
    procedure btnRotateCWClick(Sender: TObject);
    procedure btnRotateObjectAntiClockwiseClick(Sender: TObject);
    procedure btnRotateObjectClockwiseClick(Sender: TObject);
    procedure btnSelectedSliceModifierClick(Sender: TObject);
    procedure btnSelectMetablockClick(Sender: TObject);
    procedure btnSkewLeftClick(Sender: TObject);
    procedure btnSkewRightClick(Sender: TObject);
    procedure cbLandChange(Sender: TObject);
    procedure cbObjectTypeChange(Sender: TObject);
    procedure cbReceiverClick(Sender: TObject);
    procedure cbRenderOptionChange(Sender: TObject);
    procedure DrawBlockPropertyChange(Sender: TObject);
    procedure cbDisableTransparencyChange(Sender: TObject);
    procedure cbRenderTypeOptionChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure LandGraphicPropertyChange(Sender: TObject);
    procedure ebObjectGraphicEditingDone(Sender: TObject);
    procedure ebNumbersOnlyAllowNegativeKeyPress(Sender: TObject; var Key: char);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure levelImageKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miGraphicComparisonClick(Sender: TObject);
    procedure miGridOptionClick(Sender: TObject);
    procedure miImportBlockDataClick(Sender: TObject);
    procedure miImportGraphicFileClick(Sender: TObject);
    procedure miRedoClick(Sender: TObject);
    procedure miReorderLevelsClick(Sender: TObject);
    procedure miUndoClick(Sender: TObject);
    procedure pagesLevelComponentChange(Sender: TObject);
    procedure StylePresetChange(Sender: TObject);
    procedure miImageBruteForceClick(Sender: TObject);
    procedure StylePropertyChange(Sender: TObject);
    procedure levelImageMouseDown(Sender: TObject; Button: TMouseButton;
      {%H-}Shift: TShiftState; X, Y: Integer; {%H-}Layer: TCustomLayer);
    procedure levelImageMouseLeave(Sender: TObject);
    procedure levelImageMouseMove(Sender: TObject; {%H-}Shift: TShiftState; X,
      Y: Integer; {%H-}Layer: TCustomLayer);
    procedure levelImageMouseUp(Sender: TObject; Button: TMouseButton;
      {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer; {%H-}Layer: TCustomLayer);
    procedure levelImageMouseWheel(Sender: TObject; {%H-}Shift: TShiftState;
      WheelDelta: Integer; {%H-}MousePos: TPoint; var Handled: Boolean);
    procedure LevelPropertyChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miAboutClick(Sender: TObject);
    procedure miAboutDebugOptionsClick(Sender: TObject);
    procedure miBackFacesClick(Sender: TObject);
    procedure miComFileClick(Sender: TObject);
    procedure miComL3DClick(Sender: TObject);
    procedure miComL3DD1Click(Sender: TObject);
    procedure miComL3DD2Click(Sender: TObject);
    procedure miComL3DWClick(Sender: TObject);
    procedure miCreateTestTexturesetClick(Sender: TObject);
    procedure miDecFileClick(Sender: TObject);
    procedure miDecL3DClick(Sender: TObject);
    procedure miDecL3DD1Click(Sender: TObject);
    procedure miDecL3DD2Click(Sender: TObject);
    procedure miDecL3DWClick(Sender: TObject);
    procedure miExtractGraphicClick(Sender: TObject);
    procedure miLevelFloodClick(Sender: TObject);
    procedure miLevelScanClick(Sender: TObject);
    procedure miNewClick(Sender: TObject);
    procedure miOpenClick(Sender: TObject);
    procedure miQuitClick(Sender: TObject);
    procedure miResolutionOptionClick(Sender: TObject);
    procedure miSaveAsClick(Sender: TObject);
    procedure miSaveClick(Sender: TObject);
    procedure miZoomInClick(Sender: TObject);
    procedure miZoomOutClick(Sender: TObject);
    procedure tbLockAxisChange(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure FormResize(Sender: TObject);
    procedure miEditUnknownsClick(Sender: TObject);
    procedure ebObjectUnknownKeyPress(Sender: TObject; var Key: Char);
    procedure ebObjectUnknownExit(Sender: TObject);
    procedure miPaletteImageClick(Sender: TObject);
  private
    fUndoHistory: TMemoryStreamList;
    fUndoIndex: Integer;

    fDrawBlock: TL3DBlock;
    fDrawObject: TL3DObject;

    fIgnoreBlockEvents: Boolean;
    fIgnoreObjectEvents: Boolean;

    fScrollMode: Boolean;

    fInitialWidth: Integer;
    fInitialHeight: Integer;

    fRenderer: TL3DRenderer;
    fLevel: TL3DLevel;
    fLevelFilename: String;
    fLevelModified: Boolean;

    fLockedAxis: TAxis;
    fSelectedBlock: T3DPoint;
    fMarkedBlock: T3DPoint;

    fCornerMode: Boolean;

    procedure LoadOptions;
    procedure SaveOptions;

    procedure SetFieldsFromLevel;
    procedure SetLevelFromFields;

    procedure HandlePlacement(aPoint: T3DPoint);
    procedure HandlePicker(aPoint: T3DPoint);
    procedure HandleMark(aPoint: T3DPoint);
    procedure HandleRegion(aPoint: T3DPoint);
    procedure HandleShift(aPoint: T3DPoint);

    function PerformSaveCheck: Boolean;
    function HandleSave(SaveAs: Boolean = false): Boolean;

    procedure PrepareNewLevel;
    procedure RecreateLandList;
    procedure MakeNewLandIfNeeded;

    procedure HandleFileCompression(Decompress: Boolean);
    procedure HandleMassCompression(Fileset: String; Decompress: Boolean);

    procedure RedrawLevel;

    procedure LevelFloodDebug({%H-}Block, Level: TMemoryStream; {%H-}Index: Integer);

    procedure ChangeZoom(delta: Integer);

    procedure PrepareStylePresets;

    procedure SetBlockFromOptions;
    procedure SetOptionsFromBlock;
    procedure RedrawDrawBlock;

    procedure SetObjectFromOptions;
    procedure SetOptionsFromObject;
    procedure RedrawDrawObject;

    procedure SetLockedRank(aValue: Integer);
    procedure SetLockedAxis(aAxis: TAxis);

    procedure OpenBlockSelectionWindow;

    procedure MakeBlockCube;

    procedure DoRotateObject(aDelta: Integer; AnimObjOverride: Boolean = false);

    procedure ApplyBoundary;

    procedure ShiftLevelByMark;
    procedure ShiftLevel(dX, dY, dZ: Integer);

    procedure SetLevelModified(const aValue: Boolean);
    procedure SaveUndoHistoryEntry;

    function GetLockedRank: Integer;

    procedure SetCoordInfo;

    property LevelModified: Boolean read fLevelModified write SetLevelModified;
    property LockedRank: Integer read GetLockedRank;
  public

  end;

var
  FL3DEdit: TFL3DEdit;

implementation

uses
  L3DGraphics,
  L3DDecompressor,
  Misc;

{$R *.dfm}

const
  ZOOM_STEP = 0.125;
  ZOOM_MIN = 0.125;
  ZOOM_MAX = 8;

{ TFL3DEdit }

procedure TFL3DEdit.FormCreate(Sender: TObject);
begin
  PrepareStylePresets;

  {$IFNDEF DEBUG_BUILD}
  miDebug.Free;
  {$ENDIF}

  fInitialWidth := Width;
  fInitialHeight := Height;

  fRenderer := TL3DRenderer.Create;
  pagesLevelInfo.ActivePage := tabGeneral;
  pagesLevelComponent.ActivePage := tabLand;
  fCornerMode := true;

  fLockedAxis := AxisY;
  fSelectedBlock := Make3DPoint(0, 0, 0);
  fMarkedBlock := Make3DPoint(-1, -1, -1);

  LoadOptions;

  fUndoHistory := TMemoryStreamList.Create;

  fLevel := TL3DLevel.Create;
  PrepareNewLevel;
end;

procedure TFL3DEdit.LevelPropertyChange(Sender: TObject);
begin
  LevelModified := true;
end;

procedure TFL3DEdit.btnRotateCCWClic(Sender: TObject);
begin
  fRenderer.ViewRotation := fRenderer.ViewRotation - 1;
  RedrawLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
end;

procedure TFL3DEdit.btnPerspectiveIncreaseClick(Sender: TObject);
begin
  fRenderer.ViewPerspective := fRenderer.ViewPerspective + 1;
  RedrawLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
end;

procedure TFL3DEdit.btnRenderRangeApplyClick(Sender: TObject);
begin
  fRenderer.RenderRangeStart := Make3DPoint(StrToIntDef(ebRangeXLow.Text, 0),
                                            StrToIntDef(ebRangeYLow.Text, 0),
                                            StrToIntDef(ebRangeZLow.Text, 0));
  fRenderer.RenderRangeEnd := Make3DPoint(StrToIntDef(ebRangeXHigh.Text, 31) + 1,
                                          StrToIntDef(ebRangeYHigh.Text, 15) + 1,
                                          StrToIntDef(ebRangeZHigh.Text, 31) + 1);
  RedrawLevel;
end;

procedure TFL3DEdit.btnRotateBlockAntiClockwiseClick(Sender: TObject);
begin
  fDrawBlock.Rotation := (fDrawBlock.Rotation + 3) mod 4;
  RedrawDrawBlock;
end;

procedure TFL3DEdit.btnRotateBlockClockwiseClick(Sender: TObject);
begin
  fDrawBlock.Rotation := (fDrawBlock.Rotation + 1) mod 4;
  RedrawDrawBlock;
end;

procedure TFL3DEdit.btnRotateCameraClockwiseClick(Sender: TObject);
begin
  if cbActiveCamera.ItemIndex = 4 then Exit; // preview pivot has no rotation

  fLevel.InitialCameraPositions[cbActiveCamera.ItemIndex].Orientation :=
    (fLevel.InitialCameraPositions[cbActiveCamera.ItemIndex].Orientation + 1) mod 4;

  LevelModified := true;
  RedrawLevel;
end;

procedure TFL3DEdit.btnRotateCameraAnticlockwiseClick(Sender: TObject);
begin
  if cbActiveCamera.ItemIndex = 4 then Exit; // preview pivot has no rotation

  fLevel.InitialCameraPositions[cbActiveCamera.ItemIndex].Orientation :=
    (fLevel.InitialCameraPositions[cbActiveCamera.ItemIndex].Orientation + 3) mod 4;

  LevelModified := true;
  RedrawLevel;
end;

procedure TFL3DEdit.btnPerspectiveDecreaseClick(Sender: TObject);
begin
  fRenderer.ViewPerspective := fRenderer.ViewPerspective - 1;
  RedrawLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
end;

procedure TFL3DEdit.btnDeleteLandClick(Sender: TObject);
begin
  if cbLand.ItemIndex < fLevel.LandPolygons.Count then
  begin
    fLevel.LandPolygons.Delete(cbLand.ItemIndex);
    RecreateLandList;
    RedrawLevel;
  end;
end;

procedure TFL3DEdit.btnRotateCWClick(Sender: TObject);
begin
  fRenderer.ViewRotation := fRenderer.ViewRotation + 1;
  RedrawLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
end;

procedure TFL3DEdit.btnRotateObjectAntiClockwiseClick(Sender: TObject);
begin
  DoRotateObject(1);
end;

procedure TFL3DEdit.btnRotateObjectClockwiseClick(Sender: TObject);
begin
  DoRotateObject(-1);
end;

procedure TFL3DEdit.btnSelectedSliceModifierClick(Sender: TObject);
begin
  if not (Sender is TControl) then Exit;
  SetLockedRank(LockedRank + TControl(Sender).Tag);
end;

procedure TFL3DEdit.btnSelectMetablockClick(Sender: TObject);
begin
  OpenBlockSelectionWindow;
end;

procedure TFL3DEdit.btnSkewLeftClick(Sender: TObject);
begin
  fRenderer.ViewSkew := fRenderer.ViewSkew - 1;
  RedrawLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
end;

procedure TFL3DEdit.btnSkewRightClick(Sender: TObject);
begin
  fRenderer.ViewSkew := fRenderer.ViewSkew + 1;
  RedrawLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
end;

procedure TFL3DEdit.cbLandChange(Sender: TObject);
begin
  RedrawLevel;
end;

procedure TFL3DEdit.cbObjectTypeChange(Sender: TObject);
var
  IndexValue: (ivGraphic, ivPairing, ivNone);
  RotateButtons: Boolean;
  ReceiverBox: Boolean;
begin
  if fIgnoreObjectEvents then Exit;

  SetObjectFromOptions;
  RedrawDrawObject;

  IndexValue := ivGraphic;
  RotateButtons := true;
  ReceiverBox := false;

  case cbObjectType.ItemIndex of
    0: begin // Nothing
         IndexValue := ivNone;
         RotateButtons := false;
       end;
    1: RotateButtons := false; // Standard
    4: begin // Interactive
         case fLevel.InteractiveObject of
           0, 7: IndexValue := ivNone;
           1, 2, 6: begin
                      IndexValue := ivNone;
                      RotateButtons := false;
                    end;
           4, 5, 8: begin
                      IndexValue := ivPairing;
                      RotateButtons := false;
                      ReceiverBox := fLevel.InteractiveObject <> 5;
                    end;
         end;
       end;
  end;

  case IndexValue of
    ivGraphic: begin
                 lblObjectGraphic.Caption := 'Graphic';
                 lblObjectGraphic.Visible := true;
                 ebObjectGraphic.Visible := true;
                 ebObjectGraphic.Enabled := true;
               end;
    ivPairing: begin
                 lblObjectGraphic.Caption := 'Pairing';
                 lblObjectGraphic.Visible := true;
                 ebObjectGraphic.Visible := true;
                 ebObjectGraphic.Enabled := true;
               end;
    ivNone: begin
              lblObjectGraphic.Visible := false;
              ebObjectGraphic.Visible := false;
              ebObjectGraphic.Enabled := false;
            end;
  end;

  if RotateButtons then
  begin
    btnRotateObjectClockwise.Visible := true;
    btnRotateObjectClockwise.Enabled := true;
    btnRotateObjectAntiClockwise.Visible := true;
    btnRotateObjectAntiClockwise.Enabled := true;
  end else begin
    btnRotateObjectClockwise.Visible := false;
    btnRotateObjectClockwise.Enabled := false;
    btnRotateObjectAntiClockwise.Visible := false;
    btnRotateObjectAntiClockwise.Enabled := false;
  end;

  if ReceiverBox then
  begin
    cbReceiver.Visible := true;
    cbReceiver.Enabled := true;
  end else begin
    cbReceiver.Visible := false;
    cbReceiver.Enabled := false;
  end;
end;

procedure TFL3DEdit.cbReceiverClick(Sender: TObject);
begin
  if fIgnoreObjectEvents then Exit;
  SetObjectFromOptions;
  RedrawDrawObject;
end;

procedure TFL3DEdit.cbRenderOptionChange(Sender: TObject);
const
  NEED_RERENDER_OPTIONS = [rfSolidSea, rfSolidSky, rfHideLand, rfLargeSeaTexture,
                           rfSingleLandTexture, rfSurroundSky];
var
  Flag: TL3DLevelRenderFlag;
begin
  if not (Sender is TCheckbox) then Exit;
  Flag := TL3DLevelRenderFlag(TCheckbox(Sender).Tag);
  if TCheckbox(Sender).Checked then
  begin
    if Flag in fLevel.RenderFlags then Exit;
    Include(fLevel.RenderFlags, Flag)
  end else begin
    if not (Flag in fLevel.RenderFlags) then Exit;
    Exclude(fLevel.RenderFlags, Flag);
  end;

  LevelModified := true;

  if Flag in NEED_RERENDER_OPTIONS then RedrawLevel;
end;

procedure TFL3DEdit.DrawBlockPropertyChange(Sender: TObject);
begin
  if fIgnoreBlockEvents then Exit;
  SetBlockFromOptions;
end;

procedure TFL3DEdit.cbDisableTransparencyChange(Sender: TObject);
begin
  if cbDisableTransparency.Checked then
    Include(fRenderer.RenderSettings, rsNoInvisibles)
  else
    Exclude(fRenderer.RenderSettings, rsNoInvisibles);

  fRenderer.PurgeCache;
  fLevel.PurgeBlockFaceGraphics;
  RedrawLevel;
end;

procedure TFL3DEdit.cbRenderTypeOptionChange(Sender: TObject);
var
  Checkbox: TCheckbox absolute Sender;
begin
  if not (Sender is TCheckbox) then Exit;
  if Checkbox.Checked then
    Include(fRenderer.RenderPieceTypes, TRenderPieceType(Checkbox.Tag))
  else
    Exclude(fRenderer.RenderPieceTypes, TRenderPieceType(Checkbox.Tag));

  RedrawLevel;
end;

procedure TFL3DEdit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in KEYBOARD_CONTROL_KEYS then
  begin
    FocusControl(levelImage);
    levelImage.OnKeyDown(Sender, Key, Shift);
    Key := 0;
  end;
end;

procedure TFL3DEdit.FormKeyPress(Sender: TObject; var Key: char);
var
  TestKey: Word;
begin
  case Key of
    '0'..'9': TestKey := VK_NUMPAD0 + Ord(Key) - Ord('0');
    '+': TestKey := VK_ADD;
    '-': TestKey := VK_SUBTRACT;
    '*': TestKey := VK_MULTIPLY;
    '/': TestKey := VK_DIVIDE;
    '.': TestKey := VK_DECIMAL;
    else Exit;
  end;

  if GetKeyState(TestKey) < 0 then Key := #0;
end;

procedure TFL3DEdit.LandGraphicPropertyChange(Sender: TObject);
begin
  MakeNewLandIfNeeded;
  fLevel.LandPolygons[cbLand.ItemIndex].FloorGraphic := StrToIntDef(ebLandGraphic.Text, 0);
  fLevel.LandPolygons[cbLand.ItemIndex].FloorShade := tbLandShade.Position;
  RedrawLevel;
end;

procedure TFL3DEdit.ebObjectGraphicEditingDone(Sender: TObject);
begin
  if fIgnoreObjectEvents then Exit;
  SetObjectFromOptions;
  RedrawDrawObject;
end;

procedure TFL3DEdit.ebObjectUnknownExit(Sender: TObject);
begin
  SetObjectFromOptions;
end;

procedure TFL3DEdit.ebObjectUnknownKeyPress(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, ['a'..'f']) then
    Key := Uppercase(Key)[1]
  else if not (CharInSet(Key, ['0'..'9', 'A'..'F']) or (Key < #20)) then
    Key := #0;
end;

procedure TFL3DEdit.ebNumbersOnlyAllowNegativeKeyPress(Sender: TObject; var Key: char);
begin
  if not (CharInSet(Key, ['0'..'9', '-']) or (Key < #20)) then Key := #0;
end;

procedure TFL3DEdit.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewWidth := Max(NewWidth, fInitialWidth);
  NewHeight := Max(NewHeight, fInitialHeight);
end;

procedure TFL3DEdit.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  SaveOptions;
end;

procedure TFL3DEdit.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  if LevelModified then
    CanClose := PerformSaveCheck
  else
    CanClose := true;
end;

procedure TFL3DEdit.levelImageKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

  procedure BlockCount;
  var
    x,y,z: Integer;
    bc, nb3c: Integer;
  begin
    bc := 0;
    nb3c := 0;
    for z := 0 to 31 do
      for y := 0 to 15 do
        for x := 0 to 31 do
          if not fLevel.Blocks[x,y,z].Empty then
          begin
            Inc(bc);
            if not (fLevel.Blocks[x,y,z].Graphic = 3) then
              Inc(nb3c);
          end;

    ShowMessage(IntToStr(bc) + ' / ' + IntToStr(nb3c));
  end;

  procedure KeyboardMove(dX, dY, dZ: Integer);
  var
    MaxHeight: Integer;
  begin
    fSelectedBlock.X := fSelectedBlock.X + dX;
    fSelectedBlock.Y := fSelectedBlock.Y + dY;
    fSelectedBlock.Z := fSelectedBlock.Z + dZ;

    if pagesLevelComponent.ActivePage = tabLand then
      MaxHeight := 0
    else
      MaxHeight := 15;

    while fSelectedBlock.X < -1 do
      fSelectedBlock.X := fSelectedBlock.X + 33;
    while fSelectedBlock.Y < -1 do
      fSelectedBlock.Y := fSelectedBlock.Y + (MaxHeight + 2);
    while fSelectedBlock.Z < -1 do
      fSelectedBlock.Z := fSelectedBlock.Z + 33;
    while fSelectedBlock.X > 31 do
      fSelectedBlock.X := fSelectedBlock.X - 33;
    while fSelectedBlock.Y > MaxHeight do
      fSelectedBlock.Y := fSelectedBlock.Y - (MaxHeight + 2);
    while fSelectedBlock.Z > 31 do
      fSelectedBlock.Z := fSelectedBlock.Z - 33;

    SetCoordInfo;
    RedrawLevel;
  end;

  procedure KeyboardRotate(dR: Integer);
  begin
    fRenderer.ViewRotation := fRenderer.ViewRotation + dR;
    SetCoordInfo;
    RedrawLevel;
    RedrawDrawBlock;
    RedrawDrawObject;
  end;

begin
  if ssCtrl in Shift then Exit;

  if Key in KEYBOARD_CONTROL_KEYS then
  begin
    try
      case Key of
        VK_NUMPAD4: KeyboardMove(-1, 0, 0);
        VK_NUMPAD5: KeyboardMove(0, 1, 0);
        VK_NUMPAD6: KeyboardMove(0, 0, -1);
        VK_NUMPAD1: KeyboardMove(0, 0, 1);
        VK_NUMPAD2: KeyboardMove(0, -1, 0);
        VK_NUMPAD3: KeyboardMove(1, 0, 0);

        VK_NUMPAD8: HandlePlacement(fSelectedBlock);
        VK_NUMPAD0: HandlePicker(fSelectedBlock);
        VK_DECIMAL: HandleMark(fSelectedBlock);

        VK_NUMPAD7: KeyboardRotate(-1);
        VK_NUMPAD9: KeyboardRotate(1);

        VK_DIVIDE: HandleRegion(fSelectedBlock);
        VK_MULTIPLY: HandleShift(fSelectedBlock);
        VK_SUBTRACT: cbDisableMouseInput.Checked := not cbDisableMouseInput.Checked;
      end;
    finally
      Key := 0;
    end;
    Exit;
  end;

  if not (ssShift in Shift) then Exit;

  try
    case Key of
      Ord('Z'): SetLockedRank(LockedRank - 1);
      Ord('C'): SetLockedRank(LockedRank + 1);
      Ord('X'): SetLockedAxis(TAxis((Integer(fLockedAxis) + 1) mod 3));
      Ord('Q'): begin pagesLevelComponent.ActivePage := tabLand; pagesLevelComponentChange(pagesLevelComponent); end;
      Ord('W'): begin pagesLevelComponent.ActivePage := tabBlocks; pagesLevelComponentChange(pagesLevelComponent); end;
      Ord('E'): begin pagesLevelComponent.ActivePage := tabObjects; pagesLevelComponentChange(pagesLevelComponent); end;
      Ord('R'): begin pagesLevelComponent.ActivePage := tabCameras; pagesLevelComponentChange(pagesLevelComponent); end;
      Ord('T'): begin pagesLevelComponent.ActivePage := tabBoundaries; pagesLevelComponentChange(pagesLevelComponent); end;

      Ord('F'): begin
                  if (fMarkedBlock.X >= 0) then
                    fMarkedBlock := Make3DPoint(-1, -1, -1)
                  else
                    fMarkedBlock := fSelectedBlock;
                  RedrawLevel;
                end;
      Ord('V'): MakeBlockCube;
      Ord('G'): ShiftLevelByMark;
      Ord('O'): BlockCount;
      Ord('B'): KeyboardRotate(1);
    end;

    if pagesLevelComponent.ActivePage = tabBlocks then
      case Key of
        Ord('A'): OpenBlockSelectionWindow;
        Ord('S'): begin Inc(fDrawBlock.Shape); if fDrawBlock.Shape > High(TL3DBlockShape) then fDrawBlock.Shape := Low(TL3DBlockShape); SetOptionsFromBlock; RedrawDrawBlock; end;
        Ord('D'): begin fDrawBlock.Rotation := (fDrawBlock.Rotation + 1) mod 4; RedrawDrawBlock; end;
        Ord('1'): begin fDrawBlock.Segments[0] := not fDrawBlock.Segments[0]; SetOptionsFromBlock; RedrawDrawBlock; end;
        Ord('2'): begin fDrawBlock.Segments[1] := not fDrawBlock.Segments[1]; SetOptionsFromBlock; RedrawDrawBlock; end;
        Ord('3'): begin fDrawBlock.Segments[2] := not fDrawBlock.Segments[2]; SetOptionsFromBlock; RedrawDrawBlock; end;
        Ord('4'): begin fDrawBlock.Segments[3] := not fDrawBlock.Segments[3]; SetOptionsFromBlock; RedrawDrawBlock; end;
      end;

    if pagesLevelComponent.ActivePage = tabCameras then
      case Key of
        Ord('1'): cbActiveCamera.ItemIndex := 0;
        Ord('2'): cbActiveCamera.ItemIndex := 1;
        Ord('3'): cbActiveCamera.ItemIndex := 2;
        Ord('4'): cbActiveCamera.ItemIndex := 3;
        Ord('A'): cbActiveCamera.ItemIndex := 4;
        Ord('D'): if cbActiveCamera.ItemIndex < 4 then
              begin
                fLevel.InitialCameraPositions[cbActiveCamera.ItemIndex].Orientation :=
                  (fLevel.InitialCameraPositions[cbActiveCamera.ItemIndex].Orientation + 1) mod 4;
                RedrawLevel;
              end;
      end;

    if pagesLevelComponent.ActivePage = tabObjects then
      case Key of
        Ord('A'): begin cbObjectType.ItemIndex := (cbObjectType.ItemIndex + 1) mod cbObjectType.Items.Count; cbObjectTypeChange(cbObjectType); end;
        Ord('S'): begin
                case fDrawObject.ObjectType of
                  otStandard: ebObjectGraphic.Text := IntToStr((StrToIntDef(ebObjectGraphic.Text, -1) + 1) mod 20);
                  otSign: ebObjectGraphic.Text := IntToStr((StrToIntDef(ebObjectGraphic.Text, -1) + 1) mod 8);
                  otAnimation: ebObjectGraphic.Text := IntToStr((StrToIntDef(ebObjectGraphic.Text, -1) + 1) mod 4);
                  otWall: ebObjectGraphic.Text := IntToStr((StrToIntDef(ebObjectGraphic.Text, -1) + 1) mod 16);
                  otInteractive: case fLevel.InteractiveObject of
                                   3: ebObjectGraphic.Text := IntToStr((StrToIntDef(ebObjectGraphic.Text, -1) + 1) mod 2);
                                   4, 8: ebObjectGraphic.Text := IntToStr((StrToIntDef(ebObjectGraphic.Text, -1) + 1) mod 4);
                                   5: ebObjectGraphic.Text := IntToStr((StrToIntDef(ebObjectGraphic.Text, -1) + 1) mod 8);
                                 end;
                end;
                ebObjectGraphicEditingDone(ebObjectGraphic);
              end;
        Ord('D'): if cbReceiver.Visible then
                cbReceiver.Checked := not cbReceiver.Checked
              else
                DoRotateObject(1);
      end;

    if pagesLevelComponent.ActivePage = tabBoundaries then
      if Key = Ord('D') then
        cbBoundary.ItemIndex := (cbBoundary.ItemIndex + 1) mod 5;

    if pagesLevelComponent.ActivePage = tabLand then
      case Key of
        Ord('A'): begin cbLand.ItemIndex := (cbLand.ItemIndex + 1) mod cbLand.Items.Count; cbLandChange(cbLand); end;
        Ord('S'): begin ebLandGraphic.Text := IntToStr(StrToIntDef(ebLandGraphic.Text, -1) + 1); LandGraphicPropertyChange(ebLandGraphic); end;
        Ord('D'): begin tbLandShade.Position := (tbLandShade.Position + 1) mod tbLandShade.Max; LandGraphicPropertyChange(tbLandShade); end;
      end;
  finally
    Key := 0;
  end;
end;

procedure TFL3DEdit.miGraphicComparisonClick(Sender: TObject);
var
  SearchRec: TSearchRec;
  MS1, MS2: TMemoryStream;
  SL: TStringList;
begin
  if FindFirst(AppPath + 'WLGFX' + PathDelim + '*', 0, SearchRec) = 0 then
  begin
    MS1 := TMemoryStream.Create;
    MS2 := TMemoryStream.Create;
    SL := TStringList.Create;
    try
      repeat
        if not FileExists(AppPath + 'GFX' + PathDelim + SearchRec.Name) then
        begin
          SL.Add('Unique file: ' + SearchRec.Name);
          Continue;
        end;

        MS1.LoadFromFile(AppPath + 'WLGFX' + PathDelim + SearchRec.Name);
        MS2.LoadFromFile(AppPath + 'GFX' + PathDelim + SearchRec.Name);

        if (MS1.Size <> MS2.Size) or not CompareMem(MS1.Memory, MS2.Memory, MS1.Size) then
          SL.Add('Non-identical: ' + SearchRec.Name)
        else
          SL.Add('Identical: ' + SearchRec.Name);
      until FindNext(SearchRec) <> 0;

      SL.SaveToFile(AppPath + 'gfxcomp.txt');
    finally
      MS1.Free;
      MS2.Free;
      SysUtils.FindClose(SearchRec); // otherwise it tries to use one from the Windows unit
    end;
  end;
end;

procedure TFL3DEdit.miImportBlockDataClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  TempLevel: TL3DLevel;
  i, f: Integer;
begin
  TempLevel := TL3DLevel.Create;
  try
    OpenDlg := TOpenDialog.Create(self);
    try
      OpenDlg.Title := 'Select level to import from';
      OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
      OpenDlg.Filter := 'Lemmings 3D Level Files|LEVEL.*;BLK.*';
      if OpenDlg.Execute then
        TempLevel.LoadFromFile(OpenDlg.FileName)
      else
        Exit;
    finally
      OpenDlg.Free;
    end;

    fLevel.TextureSet := TempLevel.TextureSet;
    for i := 0 to 63 do
    begin
      fLevel.MetaBlocks[i].Flags := TempLevel.MetaBlocks[i].Flags;
      for f := 0 to 5 do
      begin
        fLevel.MetaBlocks[i].Faces[f].TextureIndex := TempLevel.MetaBlocks[i].Faces[f].TextureIndex;
        fLevel.MetaBlocks[i].Faces[f].Modifiers := TempLevel.MetaBlocks[i].Faces[f].Modifiers;
        fLevel.MetaBlocks[i].Faces[f].Shade := TempLevel.MetaBlocks[i].Faces[f].Shade;
      end;
    end;
  finally
    TempLevel.Free;
  end;

  fLevel.PurgeBlockFaceGraphics;
  fRenderer.PurgeCache;
  LevelModified := true;

  ebTextureSet.Text := IntToStr(fLevel.TextureSet);

  RedrawLevel;
  RedrawDrawBlock;
end;

procedure TFL3DEdit.miImportGraphicFileClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  TempBMP: TBitmap32;
begin
  OpenDlg := TOpenDialog.Create(self);
  TempBMP := TBitmap32.Create;
  try
    OpenDlg.Title := 'Open bitmap graphic file';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDlg.Filter := 'Bitmap Image|*.bmp';
    if not OpenDlg.Execute then Exit;
    TempBMP.LoadFromFile(OpenDlg.Filename);
    SaveL3DGraphic(TempBMP, ChangeFileExt(OpenDlg.Filename, ''), $FFFF00FF);
  finally
    OpenDlg.Free;
    TempBMP.Free;
  end;
end;

procedure TFL3DEdit.miRedoClick(Sender: TObject);
begin
  if fUndoIndex >= fUndoHistory.Count - 2 then Exit;
  fUndoIndex := fUndoIndex + 2;
  fUndoHistory[fUndoIndex].Position := 0;
  fUndoHistory[fUndoIndex+1].Position := 0;
  fLevel.LoadFromStreams(fUndoHistory[fUndoIndex], fUndoHistory[fUndoIndex + 1]);
  fLevel.PurgeBlockFaceGraphics;
  fRenderer.PurgeCache;
  SetFieldsFromLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
  RedrawLevel;
end;

procedure TFL3DEdit.miReorderLevelsClick(Sender: TObject);
var
  F: TFReorderLevels;
begin
  F := TFReorderLevels.Create(self);
  try
    F.SetStyleList(cbTheme.Items);
    F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TFL3DEdit.miUndoClick(Sender: TObject);
begin
  if fUndoIndex <= 0 then Exit;
  fUndoIndex := fUndoIndex - 2;
  fUndoHistory[fUndoIndex].Position := 0;
  fUndoHistory[fUndoIndex+1].Position := 0;
  fLevel.LoadFromStreams(fUndoHistory[fUndoIndex], fUndoHistory[fUndoIndex + 1]);
  fLevel.PurgeBlockFaceGraphics;
  fRenderer.PurgeCache;
  SetFieldsFromLevel;
  RedrawDrawBlock;
  RedrawDrawObject;
  RedrawLevel;
end;

procedure TFL3DEdit.pagesLevelComponentChange(Sender: TObject);
var
  NeedRedraw: Boolean;
begin
  NeedRedraw := fCornerMode and (pagesLevelComponent.ActivePage <> tabLand);

  if pagesLevelComponent.ActivePage = tabLand then
  begin
    fCornerMode := true;

    SetLockedAxis(AxisY); // This will also trigger the redraw.
  end else
    fCornerMode := false;

  if NeedRedraw then
    RedrawLevel;
end;

procedure TFL3DEdit.MakeBlockCube;
var
  X, Y, Z: Integer;
  X1, X2, Y1, Y2, Z1, Z2: Integer;
  RealMark, RealSelect: T3DPoint;
begin
  if (pagesLevelComponent.ActivePage <> tabBlocks) and (pagesLevelComponent.ActivePage <> tabObjects) then Exit;
  if (fMarkedBlock.X < 0) or (fSelectedBlock.X < 0) then Exit;

  RealMark := fRenderer.ViewCoordsToDataCoords(fMarkedBlock);
  RealSelect := fRenderer.ViewCoordsToDataCoords(fSelectedBlock);

  X1 := Min(RealMark.X, RealSelect.X);
  X2 := Max(RealMark.X, RealSelect.X);
  Y1 := Min(RealMark.Y, RealSelect.Y);
  Y2 := Max(RealMark.Y, RealSelect.Y);
  Z1 := Min(RealMark.Z, RealSelect.Z);
  Z2 := Max(RealMark.Z, RealSelect.Z);

  for X := X1 to X2 do
    for Y := Y1 to Y2 do
      for Z := Z1 to Z2 do
        if pagesLevelComponent.ActivePage = tabBlocks then
          fLevel.Blocks[x, y, z] := fDrawBlock
        else if pagesLevelComponent.ActivePage = tabObjects then
          fLevel.Objects[x, y, z] := fDrawObject;

  fMarkedBlock := Make3DPoint(-1, -1, -1);
  LevelModified := true;

  RedrawLevel;
end;

procedure TFL3DEdit.DoRotateObject(aDelta: Integer; AnimObjOverride: Boolean = false);
var
  NewOrientation: Integer;
  Orientations: Integer;
begin
  NewOrientation := fDrawObject.Orientation + aDelta;
  if fDrawObject.ObjectType = otWall then
    Orientations := 6
  else
    Orientations := 4;

  NewOrientation := ((NewOrientation mod Orientations) + Orientations) mod Orientations;

  case fDrawObject.ObjectType of
    otSign: fDrawObject.RawByte1 := (fDrawObject.RawByte1 and $FC) or NewOrientation;
    otAnimation: if (fDrawObject.GraphicIndex = 1) and (not AnimObjOverride) then Exit
                 else begin
                   fDrawObject.RawByte1 := (fDrawObject.RawByte1 and $FC) or NewOrientation;
                   if fDrawObject.RawByte1 = $51 then
                     if aDelta < 0 then
                       Dec(fDrawObject.RawByte1)
                     else
                       Inc(fDrawObject.RawByte1);
                 end;
    otInteractive: case fLevel.InteractiveObject of
                     0, 7: fDrawObject.RawByte1 := (fDrawObject.RawByte1 and $FC) or NewOrientation;
                   end;
    otWall: fDrawObject.RawByte1 := ((NewOrientation shl 4) + $70) or (fDrawObject.RawByte1 and $0F);
  end;

  RedrawDrawObject;
end;

procedure TFL3DEdit.ApplyBoundary;
var
  TargetFace: Integer;
  TargetPos: T3DPoint;
  Rotations: Integer;
begin
  TargetPos := fRenderer.ViewCoordsToDataCoords(fSelectedBlock);
  if cbBoundary.ItemIndex = 2 then
    TargetFace := FACE_Y15
  else begin
    TargetFace := cbBoundary.ItemIndex;
    if TargetFace > 2 then TargetFace := TargetFace - 1;
    TargetFace := ((TargetFace + 2) mod 4) xor $01;
    Rotations := fRenderer.ViewRotation;
    while Rotations > 0 do
    begin
      case TargetFace of
        FACE_Z31: TargetFace := FACE_X31;
        FACE_X31: TargetFace := FACE_Z0;
        FACE_Z0: TargetFace := FACE_X0;
        FACE_X0: TargetFace := FACE_Z31;
      end;
      Dec(Rotations);
    end;
  end;

  case TargetFace of // we have the REAL face by this point, not the view face
    FACE_Z0: fLevel.BorderKills[FACE_Z0] := TargetPos.Z + 1;
    FACE_Z31: fLevel.BorderKills[FACE_Z31] := TargetPos.Z;
    FACE_Y15: fLevel.BorderKills[FACE_Y15] := TargetPos.Y;
    FACE_X0: fLevel.BorderKills[FACE_X0] := TargetPos.X + 1;
    FACE_X31: fLevel.BorderKills[FACE_X31] := TargetPos.X;
  end;

  RedrawLevel;
end;

procedure TFL3DEdit.ShiftLevelByMark;
var
  RealSrc, RealDst: T3DPoint;
begin
  if fMarkedBlock.X < 0 then Exit;

  RealSrc := fRenderer.ViewCoordsToDataCoords(fMarkedBlock);
  RealDst := fRenderer.ViewCoordsToDataCoords(fSelectedBlock);

  ShiftLevel(RealDst.X - RealSrc.X, RealDst.Y - RealSrc.Y, RealDst.Z - RealSrc.Z);

  fMarkedBlock := Make3DPoint(-1, -1, -1);
end;

procedure TFL3DEdit.ShiftLevel(dX, dY, dZ: Integer);
var
  BlockData: array[0..31, 0..15, 0..31] of TL3DBlock;
  ObjectData: array[0..31, 0..15, 0..31] of TL3DObject;
  X, Y, Z: Integer;
  i, n: Integer;
  Vertex: TLandVertexPoint;

  procedure AdjustPoint(var X, Y, Z: Integer);
  begin
    X := X - dX;
    Y := Y - dY;
    Z := Z - dZ;
  end;

  function GetSrcBlock(aX, aY, aZ: Integer): TL3DBlock;
  begin
    AdjustPoint(aX{%H-}, aY{%H-}, aZ{%H-}); // compiler is a dumb fuck
    if (aX >= 0) and (aX <= 31) and (aY >= 0) and (aY <= 15) and (aZ >= 0) and (aZ <= 31) then
      Result := BlockData[aX, aY, aZ]
    else
      Result.RawData := 0;
  end;

  function GetSrcObject(aX, aY, aZ: Integer): TL3DObject;
  begin
    AdjustPoint(aX{%H-}, aY{%H-}, aZ{%H-});
    if (aX >= 0) and (aX <= 31) and (aY >= 0) and (aY <= 15) and (aZ >= 0) and (aZ <= 31) then
      Result := ObjectData[aX, aY, aZ]
    else
      Result.RawData := 0;
  end;

begin
  for Z := 0 to 31 do
    for Y := 0 to 15 do
      for X := 0 to 31 do
      begin
        BlockData[X,Y,Z] := fLevel.Blocks[X, Y, Z];
        ObjectData[X,Y,Z] := fLevel.Objects[X, Y, Z];
      end;

  for Z := 0 to 31 do
    for Y := 0 to 15 do
      for X := 0 to 31 do
      begin
        fLevel.Blocks[X, Y, Z] := GetSrcBlock(X, Y, Z);
        fLevel.Objects[X, Y, Z] := GetSrcObject(X, Y, Z);
      end;

  for i := 0 to 3 do
  begin
    fLevel.InitialCameraPositions[i].Position.X := fLevel.InitialCameraPositions[i].Position.X + dX;
    fLevel.InitialCameraPositions[i].Position.Y := fLevel.InitialCameraPositions[i].Position.Y + dY;
    fLevel.InitialCameraPositions[i].Position.Z := fLevel.InitialCameraPositions[i].Position.Z + dZ;
  end;

  fLevel.PreviewCameraPivot.X := fLevel.PreviewCameraPivot.X + dX;
  fLevel.PreviewCameraPivot.Y := fLevel.PreviewCameraPivot.Y + dY;
  fLevel.PreviewCameraPivot.Z := fLevel.PreviewCameraPivot.Z + dZ;

  for i := 0 to fLevel.LandPolygons.Count-1 do
    for n := 0 to fLevel.LandPolygons[i].Count-1 do
    begin
      Vertex := fLevel.LandPolygons[i][n];
      Vertex.X := Vertex.X + dX;
      Vertex.Z := Vertex.Z + dZ;
      fLevel.LandPolygons[i][n] := Vertex;
    end;

  fLevel.BorderKills[FACE_X0] := Max(1, fLevel.BorderKills[FACE_X0] + dX);
  fLevel.BorderKills[FACE_X31] := Min(31, fLevel.BorderKills[FACE_X31] + dX);
  fLevel.BorderKills[FACE_Z0] := Max(1, fLevel.BorderKills[FACE_Z0] + dZ);
  fLevel.BorderKills[FACE_Z31] := Min(31, fLevel.BorderKills[FACE_Z31] + dZ);
  fLevel.BorderKills[FACE_Y15] := Min(15, fLevel.BorderKills[FACE_Y15] + dY);

  LevelModified := true;
  RedrawLevel;
end;

procedure TFL3DEdit.SetLevelModified(const aValue: Boolean);
begin
  fLevelModified := aValue;
  if aValue then SaveUndoHistoryEntry;
end;

procedure TFL3DEdit.SaveUndoHistoryEntry;
var
  LevelMS, BlockMS: TMemoryStream;
  LastLevelMS, LastBlockMS: TMemoryStream;

  procedure Cancel;
  begin
    fUndoHistory.Remove(LevelMS);
    fUndoHistory.Remove(BlockMS);
    // This will automatically free them too.
  end;

begin
  while fUndoIndex + 2 < fUndoHistory.Count do
    fUndoHistory.Delete(fUndoHistory.Count-1);

  try
    LevelMS := TMemoryStream.Create;
    BlockMS := TMemoryStream.Create;
    fUndoHistory.Add(LevelMS);
    fUndoHistory.Add(BlockMS);
    try
      fLevel.SaveToStreams(LevelMS, BlockMS);
      if fUndoHistory.Count >= 4 then
      begin
        LastLevelMS := fUndoHistory[fUndoHistory.Count - 4];
        LastBlockMS := fUndoHistory[fUndoHistory.Count - 3];
        if CompareMem(LevelMS.Memory, LastLevelMS.Memory, LevelMS.Size) and
           CompareMem(BlockMS.Memory, LastBlockMS.Memory, BlockMS.Size) then
        begin
          Cancel;
          Exit;
        end;
      end;

      while fUndoHistory.Count > MAX_UNDO_HISTORY_STREAMS do
        fUndoHistory.Delete(0);
    except
      {$IFDEF DEBUG_BUILD}
      ShowMessage('Exception during saving undo history.');
      {$ENDIF}
      Cancel;
    end;
  finally
    fUndoIndex := fUndoHistory.Count - 2;
  end;
end;

function TFL3DEdit.GetLockedRank: Integer;
begin
  case fLockedAxis of
    AxisX: Result := fSelectedBlock.X;
    AxisY: Result := fSelectedBlock.Y;
    AxisZ: Result := fSelectedBlock.Z;
    else Result := -1;
  end;
end;

procedure TFL3DEdit.SetCoordInfo;
begin
  if LockedRank < 0 then
    lblSelectedSliceIndex.Caption := '(None)'
  else
    lblSelectedSliceIndex.Caption := IntToStr(LockedRank);
  lblCoords.Caption := 'X: ' + IntToStr(fSelectedBlock.X) + ' | Y: ' + IntToStr(fSelectedBlock.Y) + ' | Z: ' + IntToStr(fSelectedBlock.Z);
end;

procedure TFL3DEdit.StylePresetChange(Sender: TObject);
var
  ComboBox: TComboBox absolute Sender;
  EditBox: TEdit;
  NewValueNumeric: Integer;
  NewValue: String;
begin
  if not (Sender is TComboBox) then Exit;
  if ComboBox.ItemIndex < 1 then Exit;
  NewValueNumeric := Integer(ComboBox.Items.Objects[ComboBox.ItemIndex]);
  ComboBox.ItemIndex := 0;
  EditBox := nil;

  case ComboBox.Tag of
    0: begin
         EditBox := ebTextureSet;
         fLevel.PurgeBlockFaceGraphics;
       end;
    1: begin
         EditBox := ebLand;
         cbLargeLandTexture.Checked := (NewValueNumeric >= 0) and (NewValueNumeric <> 255);
       end;
    2: EditBox := ebObjects;
    3: EditBox := ebSign;
    4: begin
         EditBox := ebSea;
         cbLargeSeaTexture.Checked := (NewValueNumeric >= 0) and (NewValueNumeric <> 255);
       end;
    5: EditBox := ebAnimation;
    6: EditBox := ebInteractive;
    7: begin
         EditBox := ebSky;
         cbFullHeightSkyTexture.Checked := NewValueNumeric < 0;
       end;
    8: EditBox := ebWalls;
  end;

  if NewValueNumeric < 0 then
    NewValue := IntToStr(-1 - NewValueNumeric)
  else
    NewValue := IntToStr(NewValueNumeric);

  EditBox.Text := NewValue;
  StylePropertyChange(EditBox);
end;

procedure TFL3DEdit.miImageBruteForceClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  TempBMP: TBitmap32;
  FS: TFileStream;
  i: Integer;
begin
  OpenDlg := TOpenDialog.Create(self);
  TempBMP := TBitmap32.Create;
  try
    OpenDlg.Title := 'Open graphic file';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDlg.Filter := 'Lemmings 3D Graphic File|*.0??';
    if OpenDlg.Execute then
    begin
      FS := TFileStream.Create(OpenDlg.Filename, fmOpenRead);
      try
        for i := 1 to Floor(Sqrt(FS.Size)) do
        begin
          if FS.Size mod i <> 0 then Continue;
          FS.Position := 0;
          LoadL3DGraphic(TempBMP, FS, i, FS.Size div i, false);
          TempBMP.SaveToFile(OpenDlg.Filename + '-' + LeadZeroStr(i, 4) + '.bmp');
        end;
      finally
        FS.Free;
      end;
    end;
  finally
    OpenDlg.Free;
    TempBMP.Free;
  end;
end;

procedure TFL3DEdit.StylePropertyChange(Sender: TObject);
begin
  SetLevelFromFields;

  fRenderer.PurgeCache;

  if Sender = ebTextureSet then
  begin
    fLevel.PurgeBlockFaceGraphics;
    RedrawDrawBlock;
  end;

  if Sender = ebInteractive then
    cbObjectTypeChange(cbObjectType);

  if (Sender = ebObjects) or (Sender = ebSign) or (Sender = ebAnimation) or
     (Sender = ebInteractive) or (Sender = ebWalls) then
    RedrawDrawObject;

  RedrawLevel;
end;

procedure TFL3DEdit.levelImageMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
begin
  FocusControl(levelImage);

  if (ssShift in Shift) then
    Exit;

  if cbDisableMouseInput.Checked then
    Button := mbMiddle; // since we still allow mouse scroll

  case Button of
    mbLeft: case rgDrawMode.ItemIndex of
              1: begin
                   rgDrawMode.ItemIndex := 0;
                   HandleMark(fSelectedBlock);
                 end;
              2: begin
                   rgDrawMode.ItemIndex := 0;
                   HandleRegion(fSelectedBlock);
                 end;
              3: begin
                   rgDrawMode.ItemIndex := 0;
                   HandleShift(fSelectedBlock);
                 end;
              else HandlePlacement(fSelectedBlock);
            end;
    mbMiddle: begin
                fScrollMode := true;
                Mouse.CursorPos := levelImage.ClientToScreen(Point(levelImage.Width div 2, levelImage.Height div 2));
              end;
    mbRight: HandlePicker(fSelectedBlock);
  end;
end;      

procedure TFL3DEdit.levelImageMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
begin
  if (Button = mbMiddle) or (cbDisableMouseInput.Checked) then fScrollMode := false;
end;

procedure TFL3DEdit.levelImageMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if WheelDelta < 0 then
    ChangeZoom(-1)
  else if WheelDelta > 0 then
    ChangeZoom(1);

  Handled := true;
end;

procedure TFL3DEdit.levelImageMouseLeave(Sender: TObject);
begin
  fScrollMode := false;
end;

procedure TFL3DEdit.levelImageMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer; Layer: TCustomLayer);
var
  NewSelectedBlock: T3DPoint;
  CenterPos: TPoint;

  HorzMin, HorzMax, VertMin, VertMax: Single;
begin
  if fScrollMode then
  begin
    CenterPos := levelImage.ClientToScreen(Point(levelImage.Width div 2, levelImage.Height div 2));

    if (levelImage.Bitmap.Width * levelImage.Scale) + (levelImage.Width / 2) > levelImage.Width then
    begin
      HorzMin := -(levelImage.Width / 4) - (levelImage.Bitmap.Width * levelImage.Scale) + levelImage.Width;
      HorzMax := levelImage.Width / 4;
      HorzMin := Min(HorzMin, levelImage.OffsetHorz);
      HorzMax := Max(HorzMax, levelImage.OffsetHorz);
    end else begin
      HorzMin := -levelImage.Width / 4;
      HorzMax := (levelImage.Width * 3 / 4) - (levelImage.Bitmap.Width * levelImage.Scale);
    end;

    if (levelImage.Bitmap.Height * levelImage.Scale) + (levelImage.Height / 2) > levelImage.Height then
    begin
      VertMin := -(levelImage.Height / 4) - (levelImage.Bitmap.Height * levelImage.Scale) + levelImage.Height;
      VertMax := levelImage.Height / 4;
      VertMin := Min(VertMin, levelImage.OffsetVert);
      VertMax := Max(VertMax, levelImage.OffsetVert);
    end else begin
      VertMin := -levelImage.Height / 4;
      VertMax := (levelImage.Height * 3 / 4) - (levelImage.Bitmap.Height * levelImage.Scale);
    end;

    levelImage.OffsetHorz := Min(Max(HorzMin, levelImage.OffsetHorz - (Mouse.CursorPos.X - CenterPos.X)), HorzMax);
    levelImage.OffsetVert := Min(Max(VertMin, levelImage.OffsetVert - (Mouse.CursorPos.Y - CenterPos.Y)), VertMax);

    Mouse.CursorPos := CenterPos;
  end else if not cbDisableMouseInput.Checked then begin
    NewSelectedBlock := fRenderer.CalculateBlockFromImagePos(levelImage.ControlToBitmap(Point(X, Y)),
                          fLockedAxis, LockedRank, false);
    if NewSelectedBlock = fSelectedBlock then Exit;
    if not (NewSelectedBlock.IsInside(0, 32, 0, 16, 0, 32)) then Exit;

    fSelectedBlock := NewSelectedBlock;

    SetCoordInfo;
    RedrawLevel;
  end;
end;

procedure TFL3DEdit.FormDestroy(Sender: TObject);
begin
  fLevel.Free;
  fRenderer.Free;
  fUndoHistory.Free;
end;

procedure TFL3DEdit.FormResize(Sender: TObject);
begin
  btnPerspectiveDecrease.Left := btnPerspectiveIncrease.Left + 85;
end;

procedure TFL3DEdit.miAboutClick(Sender: TObject);
begin
  ShowMessage('L3DEdit V' + VersionString + #10 +
              'Lemmings 3D Level Editor' + #10 +
              'by namida' + #10 +
              #10 +
              'https://www.lemmingsforums.net/');
end;

procedure TFL3DEdit.miAboutDebugOptionsClick(Sender: TObject);
begin
  ShowMessage('Debug features are mostly here for my own use while creating the ' +
              'editor and/or figuring out L3D file formats. They are likely of ' +
              'no use to an average user, and should not even be present in ' +
              'release builds, and very likely have 100% hardcoded behaviour.');
end;

procedure TFL3DEdit.miBackFacesClick(Sender: TObject);
begin
  // Debug menu option.
  if miBackFaces.Checked then
    Include(fRenderer.RenderSettings, rsBackFaces)
  else
    Exclude(fRenderer.RenderSettings, rsBackFaces);

  fRenderer.PurgeCache;
  RedrawLevel;
end;

procedure TFL3DEdit.miComFileClick(Sender: TObject);
begin
  HandleFileCompression(false);
end;

procedure TFL3DEdit.miComL3DClick(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_FULL, false);
end;

procedure TFL3DEdit.miComL3DD1Click(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_DEMO_1, false);
end;

procedure TFL3DEdit.miComL3DD2Click(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_DEMO_2, false);
end;

procedure TFL3DEdit.miComL3DWClick(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_WINTERLAND, false);
end;

procedure TFL3DEdit.miCreateTestTexturesetClick(Sender: TObject);
var
  i: Integer;

  BackBmp, FrontBmp: TBitmap32;
  TextureSetBmp: TBitmap32;

  procedure ShiftBack;
  var
    X, Y: Integer;
    LastPixelSrc, LastPixelDst: TColor32;
    H, S, L: Single;
  begin
    LastPixelSrc := $FF000000;
    LastPixelDst := $FF000000;
    for y := 0 to BackBmp.Height-1 do
      for x := 0 to BackBmp.Width-1 do
      begin
        if BackBmp[x, y] = LastPixelSrc then
          BackBmp[x, y] := LastPixelDst
        else begin
          LastPixelSrc := BackBmp[x, y];
          RGBtoHSL(LastPixelSrc, H, S, L);
          H := H + 0.01;
          if H >= 1 then H := H - 1;
          LastPixelDst := HSLtoRGB(H, S, L) or $FF000000; // unclear whether GR32 applies any alpha value here
          BackBmp[x, y] := LastPixelDst;
        end;
      end;
  end;

  procedure GenerateBinCode(aValue: Integer);
  var
    n: Integer;
    x: Integer;
  begin
    FrontBmp.Clear($00000000);
    x := 8;
    for n := 7 downto 0 do
    begin
      if (aValue and (1 shl n)) <> 0 then
        FrontBmp.FillRect(x, 26, x+4, 38, $FFFFFFFF)
      else
        FrontBmp.FillRect(x, 26, x+4, 38, $FF404040);

      x := x + 6;
    end;
  end;

begin
  BackBmp := TBitmap32.Create;
  FrontBmp := TBitmap32.Create;
  TextureSetBmp := TBitmap32.Create;
  try
    FrontBmp.DrawMode := dmTransparent;

    BackBmp.SetSize(64, 64);
    FrontBmp.SetSize(64, 64);
    TextureSetBmp.SetSize(64, 6400);

    // Generate back
    BackBmp.FillRect(0, 0, 64, 64, $00000000);
    BackBmp.FillRect(1, 1, 63, 63, $FF200000);
    BackBmp.FillRect(2, 2, 62, 62, $FF400000);
    BackBmp.FillRect(3, 3, 61, 61, $FF600000);
    BackBmp.FillRect(4, 4, 60, 60, $FF800000);
    BackBmp.FillRect(5, 5, 59, 59, $FFA00000);
    BackBmp.FillRect(6, 6, 58, 58, $FFC00000);

    BackBmp.FillRect(7, 50, 57, 57, $FF00C0C0);

    for i := 0 to 99 do
    begin
      GenerateBinCode(i);
      BackBmp.DrawTo(TextureSetBmp, 0, i * 64);
      FrontBmp.DrawTo(TextureSetBmp, 0, i * 64);
      ShiftBack;
    end;

    SaveL3DGraphic(TextureSetBmp, AppPath + 'GFX' + PathDelim + 'TEXTURE.099');
  finally
    BackBmp.Free;
    FrontBmp.Free;
    TextureSetBmp.Free;
  end;
end;

procedure TFL3DEdit.miDecFileClick(Sender: TObject);
begin
  HandleFileCompression(true);
end;

procedure TFL3DEdit.miDecL3DClick(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_FULL, true);
end;

procedure TFL3DEdit.miDecL3DD1Click(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_DEMO_1, true);
end;

procedure TFL3DEdit.miDecL3DD2Click(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_DEMO_2, true);
end;

procedure TFL3DEdit.miDecL3DWClick(Sender: TObject);
begin
  HandleMassCompression(FILE_SET_L3D_WINTERLAND, true);
end;

procedure TFL3DEdit.miEditUnknownsClick(Sender: TObject);
var
  F: TFEditUnknowns;
begin
  F := TFEditUnknowns.Create(self);
  try
    F.SetLevel(fLevel);
    if F.ShowModal = mrOk then
    begin
      LevelModified := true;
      RedrawLevel;
    end;
  finally
    F.Free;
  end;
end;

procedure TFL3DEdit.miExtractGraphicClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  ParamsForm: TFExtractGraphicWidth;
  TempBMP: TBitmap32;
begin
  OpenDlg := TOpenDialog.Create(self);
  ParamsForm := TFExtractGraphicWidth.Create(self);
  TempBMP := TBitmap32.Create;
  try
    OpenDlg.Title := 'Open graphic file';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDlg.Filter := 'Lemmings 3D Graphic File|*.0??';
    if OpenDlg.Execute then
    begin
      ParamsForm.SetWidthFromFilename(OpenDlg.Filename);
      if ParamsForm.ShowModal = mrOk then
      begin
        LoadL3DGraphic(TempBMP, OpenDlg.FileName, 0, ParamsForm.GraphicWidth, -1, ParamsForm.SideRLE);
        TempBMP.SaveToFile(OpenDlg.FileName + '.bmp');
        ShowMessage('Extracted to ' + OpenDlg.FileName + '.bmp!');
      end;
    end;
  finally
    OpenDlg.Free;
    TempBMP.Free;
    ParamsForm.Free;
  end;
end;

procedure TFL3DEdit.miLevelFloodClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;

  SrcIndex: Integer;
  i: Integer;
  BasePath: String;

  SrcBlockStream, SrcLevelStream: TMemoryStream;
  DstBlockStream, DstLevelStream: TFileStream;
begin
  OpenDlg := TOpenDialog.Create(self);
  SrcBlockStream := TMemoryStream.Create;
  SrcLevelStream := TMemoryStream.Create;
  try
    OpenDlg.Title := 'Open level';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDlg.Filter := 'Lemmings 3D Level Files|LEVEL.*;BLK.*';
    if OpenDlg.Execute then
    begin
      BasePath := ExtractFilePath(OpenDlg.FileName);
      SrcIndex := StrToIntDef(RightStr(OpenDlg.FileName, 3), -1);

      if SrcIndex = -1 then
      begin
        ShowMessage('Invalid file selected.');
        Exit;
      end;

      if MessageDlg('This will replace all levels with copies of the selected level. Continue?',
                    mtCustom, [mbYes, mbNo], 0) = mrNo then
        Exit;

      SrcBlockStream.LoadFromFile(BasePath + 'BLK.' + LeadZeroStr(SrcIndex, 3));
      SrcLevelStream.LoadFromFile(BasePath + 'LEVEL.' + LeadZeroStr(SrcIndex, 3));

      for i := 0 to 99 do
      begin
        LevelFloodDebug(SrcBlockStream, SrcLevelStream, i);

        DstBlockStream := TFileStream.Create(BasePath + 'BLK.' + LeadZeroStr(i, 3), fmCreate);
        try
          SrcBlockStream.Position := 0;
          DstBlockStream.CopyFrom(SrcBlockStream, SrcBlockStream.Size);
        finally
          DstBlockStream.Free;
        end;

        DstLevelStream := TFileStream.Create(BasePath + 'LEVEL.' + LeadZeroStr(i, 3), fmCreate);
        try
          SrcLevelStream.Position := 0;
          DstLevelStream.CopyFrom(SrcLevelStream, SrcLevelStream.Size);
        finally
          DstLevelStream.Free;
        end;
      end;
    end;
  finally
    OpenDlg.Free;
    SrcLevelStream.Free;
    SrcBlockStream.Free;
  end;
end;

procedure TFL3DEdit.miLevelScanClick(Sender: TObject);
var
  LevelRec: TL3DLevelFile;
  BlockRec: TL3DBlockFile;
  Level: TL3DLevel;

  procedure LoadLevel(aLevelIndex: Integer);
  var
    LevelStream, BlockStream: TFileStream;
  begin
    LevelStream := TFileStream.Create(AppPath + 'levels' + PathDelim + 'official' + PathDelim + 'level.' + LeadZeroStr(aLevelIndex, 3), fmOpenRead);
    try
      BlockStream := TFileStream.Create(AppPath + 'levels' + PathDelim + 'official' + PathDelim + 'blk.' + LeadZeroStr(aLevelIndex, 3), fmOpenRead);
      try
        LevelStream.Read(LevelRec, SizeOf(LevelRec));
        BlockStream.Read(BlockRec, SizeOf(BlockRec));
        LevelStream.Position := 0;
        BlockStream.Position := 0;
        Level.LoadFromStreams(LevelStream, BlockStream);
      finally
        BlockStream.Free;
      end;
    finally
      LevelStream.Free;
    end;
  end;

var
  i: Integer;
  SL, SubSL: TStringList;

  procedure DoTestStuff;
  begin
  end;
begin
  Level := TL3DLevel.Create;
  SL := TStringList.Create;
  SubSL := TStringList.Create;
  try
    for i := 0 to 99 do
    begin
      LoadLevel(i);
      DoTestStuff;
    end;

    SL.SaveToFile(AppPath + 'unknownflag-blocks.txt');
  finally
    Level.Free;
    SL.Free;
    SubSL.Free;
  end;
end;

procedure TFL3DEdit.miNewClick(Sender: TObject);
begin
  if not PerformSaveCheck then Exit;
  fLevel.Clear;
  fLevelFilename := '';
  PrepareNewLevel;
end;

procedure TFL3DEdit.miOpenClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
begin
  if not PerformSaveCheck then Exit;

  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Title := 'Open level';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDlg.Filter := 'Lemmings 3D Level Files|LEVEL.*;BLK.*';
    if OpenDlg.Execute then
    begin
      fLevel.LoadFromFile(OpenDlg.FileName);
      fLevelFilename := OpenDlg.FileName;
      PrepareNewLevel;
    end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFL3DEdit.miPaletteImageClick(Sender: TObject);
begin
  SavePaletteImage(AppPath + 'pal.bmp');
end;

procedure TFL3DEdit.miQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TFL3DEdit.miResolutionOptionClick(Sender: TObject);
var
  i: Integer;
begin
  if not (Sender is TMenuItem) then Exit;

  if fRenderer.ViewResolution = TMenuItem(Sender).Tag then Exit;

  fRenderer.ViewResolution := TMenuItem(Sender).Tag;
  for i := 0 to miResolution.Count-1 do
    miResolution[i].Checked := Sender = miResolution[i];
  RedrawLevel;
end;

procedure TFL3DEdit.miGridOptionClick(Sender: TObject);
var
  i: Integer;
begin
  if not (Sender is TMenuItem) then Exit;

  if fRenderer.GridStyle = TGridStyle(TMenuItem(Sender).Tag) then Exit;

  fRenderer.GridStyle := TGridStyle(TMenuItem(Sender).Tag);
  for i := 0 to miGridStyle.Count-1 do
    miGridStyle[i].Checked := Sender = miGridStyle[i];

  if LockedRank >= 0 then
    RedrawLevel;
end;

procedure TFL3DEdit.miSaveAsClick(Sender: TObject);
begin
  HandleSave(true);
end;

procedure TFL3DEdit.miSaveClick(Sender: TObject);
begin
  HandleSave(false);
end;

procedure TFL3DEdit.miZoomInClick(Sender: TObject);
begin
  ChangeZoom(4);
end;

procedure TFL3DEdit.miZoomOutClick(Sender: TObject);
begin
  ChangeZoom(-4);
end;

procedure TFL3DEdit.tbLockAxisChange(Sender: TObject);
begin
  if not (Sender is TRadioGroup) then Exit;
  if fLockedAxis = TAxis(TRadioGroup(Sender).ItemIndex) then Exit;

  SetLockedAxis(TAxis(TRadioGroup(Sender).ItemIndex));
end;

procedure TFL3DEdit.LoadOptions;
var
  SL: TStringList;
begin
  if not FileExists(AppPath + 'L3DEdit.ini') then
  begin
    if FileExists(AppPath + 'L3D.EXE') and not FileExists(AppPath + 'SOUND' + PathDelim + 'patched') then
      ShowMessage('Please note that L3DEdit''s music-related setting assumes you have used L3DUtils to apply the music patch. ' +
                  'It appears your copy of Lemmings 3D does not have the music patch applied. It is recommended that you download ' +
                  'L3DUtils and apply this patch.');
  end else begin
    SL := TStringList.Create;
    try
      SL.LoadFromFile(AppPath + 'L3DEdit.ini');

      // I haven't included the things under RenderSettings because they're mostly
      // debug stuff that I'll remove later, and at any rate generally would be
      // expected not to be saved.

      fRenderer.ViewPerspective := StrToIntDef(SL.Values['PERSPECTIVE'], fRenderer.ViewPerspective);
      fRenderer.ViewSkew := StrToIntDef(SL.Values['SKEW'], fRenderer.ViewSkew);
      fRenderer.ViewResolution := StrToIntDef(SL.Values['RESOLUTION'], fRenderer.ViewResolution);
      fRenderer.GridStyle := TGridStyle(StrToIntDef(SL.Values['GRID'], 0));

      levelImage.Scale := Min(Max(ZOOM_MIN, StrToFloatDef(SL.Values['ZOOM'], 1)), ZOOM_MAX);
      cbDisableMouseInput.Checked := StrToBoolDef(SL.Values['DISABLE_MOUSE'], false);
    finally
      SL.Free;
    end;
  end;

  miResolution.Items[fRenderer.ViewResolution].Checked := true;
  miGridStyle.Items[Integer(fRenderer.GridStyle)].Checked := true;
end;

procedure TFL3DEdit.SaveOptions;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SL.Values['PERSPECTIVE'] := IntToStr(fRenderer.ViewPerspective);
    SL.Values['SKEW'] := IntToStr(fRenderer.ViewSkew);
    SL.Values['RESOLUTION'] := IntToStr(fRenderer.ViewResolution);
    SL.Values['ZOOM'] := FloatToStr(levelImage.Scale);
    SL.Values['GRID'] := IntToStr(Integer(fRenderer.GridStyle));
    SL.Values['DISABLE_MOUSE'] := BoolToStr(cbDisableMouseInput.Checked);

    SL.SaveToFile(AppPath + 'L3DEdit.ini');
  finally
    SL.Free;
  end;
end;

procedure TFL3DEdit.SetFieldsFromLevel;
  procedure HandleRenderOptionCheckbox(aBox: TCheckbox);
  begin
    aBox.Checked := TL3DLevelRenderFlag(aBox.Tag) in fLevel.RenderFlags;
  end;

var
  i: Integer;
begin
  // General
  ebLevelTitle.Text := fLevel.Title;
  ebComment.Text := fLevel.Comment;
  ebLemmings.Text := IntToStr(fLevel.LemmingCount);
  ebSaveRequirement.Text := IntToStr(fLevel.SaveRequirement);
  ebTimeMinutes.Text := IntToStr(fLevel.TimeLimitMinutes);
  ebTimeSeconds.Text := IntToStr(fLevel.TimeLimitSeconds);
  ebReleaseRate.Text := IntToStr(fLevel.ReleaseRate);
  HandleRenderOptionCheckbox(cbFixedPreviewCamera);
  HandleRenderOptionCheckbox(cbDisableMinimap);
  HandleRenderOptionCheckbox(cbSolidBottom);
  HandleRenderOptionCheckbox(cbSlipperyBottom);
  HandleRenderOptionCheckbox(cbDisableQuickJump);
  HandleRenderOptionCheckbox(cbSlipperyLands);

  // Skillset
  ebBlockers.Text := IntToStr(fLevel.SkillCounts[skBlocker]);
  ebTurners.Text := IntToStr(fLevel.SkillCounts[skTurner]);
  ebBombers.Text := IntToStr(fLevel.SkillCounts[skBomber]);
  ebBuilders.Text := IntToStr(fLevel.SkillCounts[skBuilder]);
  ebBashers.Text := IntToStr(fLevel.SkillCounts[skBasher]);
  ebMiners.Text := IntToStr(fLevel.SkillCounts[skMiner]);
  ebDiggers.Text := IntToStr(fLevel.SkillCounts[skDigger]);
  ebClimbers.Text := IntToStr(fLevel.SkillCounts[skClimber]);
  ebFloaters.Text := IntToStr(fLevel.SkillCounts[skFloater]);

  // Styles
  for i := 0 to cbTheme.Items.Count do
    if i = cbTheme.Items.Count then
    begin
      cbTheme.AddItem('0x' + IntToHex(i, 2), TObject(fLevel.Style));
      cbTheme.ItemIndex := i;
    end else if Integer(cbTheme.Items.Objects[i]) = fLevel.Style then
    begin
      cbTheme.ItemIndex := i;
      Break;
    end;

  cbMusic.ItemIndex := fLevel.Music;

  ebTextureSet.Text := IntToStr(fLevel.TextureSet);
  ebLand.Text := IntToStr(fLevel.Land);
  ebObjects.Text := IntToStr(fLevel.ObjectSet);
  ebSign.Text := IntToStr(fLevel.Sign);
  ebSea.Text := IntToStr(fLevel.Sea);
  ebAnimation.Text := IntToStr(fLevel.AnimatedObject);
  ebInteractive.Text := IntToStr(fLevel.InteractiveObject);
  ebSky.Text := IntToStr(fLevel.Sky);
  ebWalls.Text := IntToStr(fLevel.Walls);
  HandleRenderOptionCheckbox(cbNonAnimatingSea);
  HandleRenderOptionCheckbox(cbLargeSeaTexture);
  HandleRenderOptionCheckbox(cbLargeLandTexture);
  HandleRenderOptionCheckbox(cbFullHeightSkyTexture);
  HandleRenderOptionCheckbox(cbZapLemmings);
  ebSeaMovementX.Text := IntToStr(fLevel.WaterSpeedX);
  ebSeaMovementZ.Text := IntToStr(fLevel.WaterSpeedZ);
end;

procedure TFL3DEdit.SetLevelFromFields;
begin
  LevelModified := true;

  // General
  fLevel.Title := Trim(ebLevelTitle.Text);
  fLevel.Comment := Trim(ebComment.Text);
  fLevel.LemmingCount := StrToIntDef(ebLemmings.Text, 0);
  fLevel.SaveRequirement := StrToIntDef(ebSaveRequirement.Text, 0);
  fLevel.TimeLimitMinutes := StrToIntDef(ebTimeMinutes.Text, 0);
  fLevel.TimeLimitSeconds := StrToIntDef(ebTimeSeconds.Text, 0);
  fLevel.ReleaseRate := StrToIntDef(ebReleaseRate.Text, 0);

  // Skillset
  fLevel.SkillCounts[skBlocker] := StrToIntDef(ebBlockers.Text, 0);
  fLevel.SkillCounts[skTurner] := StrToIntDef(ebTurners.Text, 0);
  fLevel.SkillCounts[skBomber] := StrToIntDef(ebBombers.Text, 0);
  fLevel.SkillCounts[skBuilder] := StrToIntDef(ebBuilders.Text, 0);
  fLevel.SkillCounts[skBasher] := StrToIntDef(ebBashers.Text, 0);
  fLevel.SkillCounts[skMiner] := StrToIntDef(ebMiners.Text, 0);
  fLevel.SkillCounts[skDigger] := StrToIntDef(ebDiggers.Text, 0);
  fLevel.SkillCounts[skClimber] := StrToIntDef(ebClimbers.Text, 0);
  fLevel.SkillCounts[skFloater] := StrToIntDef(ebFloaters.Text, 0);

  // Styles
  fLevel.Style := Integer(cbTheme.Items.Objects[cbTheme.ItemIndex]);
  fLevel.Music := cbMusic.ItemIndex;

  fLevel.TextureSet := StrToIntDef(ebTextureSet.Text, 0);
  fLevel.Land := StrToIntDef(ebLand.Text, 0);
  fLevel.ObjectSet := StrToIntDef(ebObjects.Text, 0);
  fLevel.Sign := StrToIntDef(ebSign.Text, 0);
  fLevel.Sea := StrToIntDef(ebSea.Text, 0);
  fLevel.AnimatedObject := StrToIntDef(ebAnimation.Text, 0);
  fLevel.InteractiveObject := StrToIntDef(ebInteractive.Text, 0);
  fLevel.Sky := StrToIntDef(ebSky.Text, 0);
  fLevel.Walls := StrToIntDef(ebWalls.Text, 0);

  fLevel.WaterSpeedX := StrToIntDef(ebSeaMovementX.Text, 0);
  fLevel.WaterSpeedZ := StrToIntDef(ebSeaMovementZ.Text, 0);
end;

procedure TFL3DEdit.HandlePlacement(aPoint: T3DPoint);
var
  NewVertex: TLandVertexPoint;
  RealBlockPos: T3DPoint;
begin
  RealBlockPos := fRenderer.ViewCoordsToDataCoords(aPoint);

  if pagesLevelComponent.ActivePage = tabBlocks then
  begin
    if not RealBlockPos.IsInside(0, 32, 0, 16, 0, 32) then Exit;
    fLevel.Blocks[RealBlockPos.X, RealBlockPos.Y, RealBlockPos.Z] := fDrawBlock;
    LevelModified := true;
    RedrawLevel;
  end else if pagesLevelComponent.ActivePage = tabObjects then
  begin
    if not RealBlockPos.IsInside(0, 32, 0, 16, 0, 32) then Exit;
    fLevel.Objects[RealBlockPos.X, RealBlockPos.Y, RealBlockPos.Z] := fDrawObject;
    LevelModified := true;
    RedrawLevel;
  end else if pagesLevelComponent.ActivePage = tabCameras then
  begin
    if not RealBlockPos.IsInside(-32, 64, 1, 16, -32, 64) then Exit;
    if cbActiveCamera.ItemIndex = 4 then
      fLevel.PreviewCameraPivot := RealBlockPos
    else
      fLevel.InitialCameraPositions[cbActiveCamera.ItemIndex].Position := RealBlockPos;
    LevelModified := true;
    RedrawLevel;
  end else if pagesLevelComponent.ActivePage = tabBoundaries then
  begin
    if not RealBlockPos.IsInside(0, 32, 0, 16, 0, 32) then Exit;
    ApplyBoundary;
  end else if pagesLevelComponent.ActivePage = tabLand then
  begin
    if not RealBlockPos.IsInside(0, 32, 0, 1, 1, 32) then Exit;
    MakeNewLandIfNeeded;

    if fLevel.LandPolygons[cbLand.ItemIndex].Count >= 8 then Exit;

    NewVertex.X := RealBlockPos.X;
    NewVertex.Z := RealBlockPos.Z;

    if fRenderer.ViewRotation in [1, 2] then NewVertex.Z := NewVertex.Z + 1;
    if fRenderer.ViewRotation in [2, 3] then NewVertex.X := NewVertex.X + 1;

    fLevel.LandPolygons[cbLand.ItemIndex].Add(NewVertex);
    LevelModified := true;
    if fLevel.LandPolygons[cbLand.ItemIndex].Count >= 3 then // if it's less than 3 corners it isn't renderable anyway
      RedrawLevel;
  end;
end;

procedure TFL3DEdit.HandlePicker(aPoint: T3DPoint);
var
  i: Integer;
  RealBlockPos: T3DPoint;
begin
  RealBlockPos := fRenderer.ViewCoordsToDataCoords(aPoint);

  if pagesLevelComponent.ActivePage = tabBlocks then
  begin
    if not RealBlockPos.IsInside(0, 32, 0, 16, 0, 32) then Exit;
    fDrawBlock := fLevel.Blocks[RealBlockPos.X, RealBlockPos.Y, RealBlockPos.Z];
    SetOptionsFromBlock;
    RedrawDrawBlock;
  end else if pagesLevelComponent.ActivePage = tabObjects then
  begin
    if not RealBlockPos.IsInside(0, 32, 0, 16, 0, 32) then Exit;
    fDrawObject := fLevel.Objects[RealBlockPos.X, RealBlockPos.Y, RealBlockPos.Z];
    SetOptionsFromObject;
    RedrawDrawObject;
  end else if pagesLevelComponent.ActivePage = tabCameras then
  begin
    if RealBlockPos = fLevel.PreviewCameraPivot then
      cbActiveCamera.ItemIndex := 4
    else
      for i := 0 to 3 do
        if RealBlockPos = fLevel.InitialCameraPositions[i].Position then
        begin
          cbActiveCamera.ItemIndex := i;
          Break;
        end;
  end else if pagesLevelComponent.ActivePage = tabLand then
  begin
    if cbLand.ItemIndex < fLevel.LandPolygons.Count then
      if fLevel.LandPolygons[cbLand.ItemIndex].Count > 0 then
      begin
        with fLevel.LandPolygons[cbLand.ItemIndex] do
          Delete(Count-1);

        LevelModified := true;

        if fLevel.LandPolygons[cbLand.ItemIndex].Count >= 2 then // 2, not 3, because if it's 2, we just changed from 3 and thus need to redraw
          RedrawLevel;
      end;
  end;
end;

procedure TFL3DEdit.HandleMark(aPoint: T3DPoint);
begin
  fMarkedBlock := aPoint; // We intentionally don't want to convert this to real position.
  RedrawLevel;
end;

procedure TFL3DEdit.HandleRegion(aPoint: T3DPoint);
begin
  MakeBlockCube;
end;

procedure TFL3DEdit.HandleShift(aPoint: T3DPoint);
begin
  ShiftLevelByMark;
end;

function TFL3DEdit.PerformSaveCheck: Boolean;
begin
  Result := true;
  if not LevelModified then Exit;

  case MessageDlg('Do you want to save the current level?', mtCustom, [mbYes, mbNo, mbCancel], 0) of
    mrYes: begin Result := HandleSave; Exit; end;
    mrNo: Exit;
    mrCancel: begin Result := false; Exit; end;
  end;
end;

function TFL3DEdit.HandleSave(SaveAs: Boolean = false): Boolean;
var
  SaveDlg: TSaveDialog;
begin
  Result := false;

  if (fLevelFilename = '') or SaveAs then
  begin
    SaveDlg := TSaveDialog.Create(self);
    try
      SaveDlg.Title := 'Save level';
      SaveDlg.Options := [ofOverwritePrompt];
      SaveDlg.Filter := 'Lemmings 3D Level Files|LEVEL.*;BLK.*';
      if SaveDlg.Execute then
      begin
        if ExtractFileExt(SaveDlg.FileName) = '' then
        begin
          ShowMessage('Error: Please make sure to choose a filename with a suitable extension.');
          Exit;
        end;

        fLevelFilename := SaveDlg.FileName;
      end else
        Exit;
    finally
      SaveDlg.Free;
    end;
  end;

  SetLevelFromFields;
  fLevel.SaveToFile(fLevelFilename);
  LevelModified := false;
  Result := true;
end;

procedure TFL3DEdit.PrepareNewLevel;
begin
  SetFieldsFromLevel;
  RecreateLandList;

  LevelModified := false;
  fUndoHistory.Clear;
  SaveUndoHistoryEntry;

  fRenderer.PurgeCache;
  RedrawDrawBlock;
  RedrawDrawObject;
  RedrawLevel;
end;

procedure TFL3DEdit.RecreateLandList;
var
  i: Integer;
  OldIndex: Integer;
begin
  OldIndex := Max(cbLand.ItemIndex, 0);
  cbLand.OnChange := nil;
  try
    cbLand.Items.Clear;
    for i := 0 to fLevel.LandPolygons.Count-1 do
      cbLand.Items.Add('Land ' + IntToStr(i));

    if cbLand.Items.Count < 8 then
      cbLand.Items.Add('New Land');
  finally
    cbLand.ItemIndex := OldIndex;
    cbLand.OnChange := cbLandChange;
  end;
end;

procedure TFL3DEdit.MakeNewLandIfNeeded;
var
  NewPolygon: TL3DLandPolygon;
  OldIndex: Integer;
begin
  if cbLand.ItemIndex >= fLevel.LandPolygons.Count then
  begin
    OldIndex := cbLand.ItemIndex;
    NewPolygon := TL3DLandPolygon.Create;
    NewPolygon.FloorGraphic := StrToIntDef(cbLand.Text, 0);
    NewPolygon.FloorShade := tbLandShade.Position;
    fLevel.LandPolygons.Add(NewPolygon);
    cbLand.Items[cbLand.Items.Count-1] := 'Land ' + IntToStr(cbLand.Items.Count - 1);
    cbLand.Items.Add('New Land');
    cbLand.ItemIndex := OldIndex;
  end;
end;

procedure TFL3DEdit.HandleFileCompression(Decompress: Boolean);
var
  OpenDlg: TOpenDialog;
begin
  OpenDlg := TOpenDialog.Create(self);
  try
    if Decompress then
      OpenDlg.Title := 'Select file to decompress'
    else
      OpenDlg.Title := 'Select file to compress';

    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDlg.Filter := 'All files|*';
    if OpenDlg.Execute then
    begin
      if Decompress then
      begin
        DecompressFile(OpenDlg.FileName);
        ShowMessage('Decompression complete.');
      end else begin
        CompressFile(OpenDlg.FileName);
        ShowMessage('Compression complete.');
      end;
    end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFL3DEdit.HandleMassCompression(Fileset: String; Decompress: Boolean);
var
  OpenDlg: TOpenDialog;
begin
  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Title := 'Locate L3D.EXE';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    OpenDlg.Filter := 'Lemmings 3D|L3D.EXE';
    if OpenDlg.Execute then
    begin
      if Decompress then
      begin
        DecompressFiles(Fileset, ExtractFilePath(OpenDlg.FileName));
        ShowMessage('Decompression complete.');
      end else begin
        CompressFiles(Fileset, ExtractFilePath(OpenDlg.FileName));
        ShowMessage('Compression complete.');
      end;
    end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFL3DEdit.RedrawLevel;
var
  HighlitLand: Integer;
begin
  levelImage.Bitmap.BeginUpdate;
  try
    if pagesLevelComponent.ActivePage = tabLand then
      HighlitLand := cbLand.ItemIndex
    else
      HighlitLand := -1;

    fRenderer.RenderLevel(levelImage.Bitmap, fLevel, fLockedAxis, LockedRank, fSelectedBlock, fMarkedBlock, fCornerMode, HighlitLand);
  finally
    levelImage.Bitmap.EndUpdate;
    levelImage.Bitmap.Changed;
  end;
end;

procedure TFL3DEdit.LevelFloodDebug(Block, Level: TMemoryStream; Index: Integer);
var
  {%H-}LevelRec: TL3DLevelFile;
  {%H-}DebugBlock: TL3DBlock;
begin
  Level.Position := 0;
  Level.Read(LevelRec{%H-}, SizeOf(LevelRec));

  // Do stuff here
  // End stuff

  Level.Clear;
  Level.Position := 0;
  Level.Write(LevelRec, SizeOf(LevelRec));
end;

procedure TFL3DEdit.ChangeZoom(delta: Integer);
begin
  levelImage.Scale := Min(ZOOM_MAX, Max(levelImage.Scale + (ZOOM_STEP * delta), ZOOM_MIN));
end;

procedure TFL3DEdit.SetBlockFromOptions;
begin
  fDrawBlock.Segments[0] := cbBlockSlices0.Checked;
  fDrawBlock.Segments[1] := cbBlockSlices1.Checked or (cbBlockSlices0.Checked and cbBlockSlices2.Checked);
  fDrawBlock.Segments[2] := cbBlockSlices2.Checked or (cbBlockSlices1.Checked and cbBlockSlices3.Checked);
  fDrawBlock.Segments[3] := cbBlockSlices3.Checked;
  fDrawBlock.Graphic := Min(Max(0, StrToIntDef(ebMetablock.Text, 0)), 63);
  fDrawBlock.Shape := TL3DBlockShape(cbBlockShape.ItemIndex);
  RedrawDrawBlock;
end;

procedure TFL3DEdit.SetOptionsFromBlock;
begin
  fIgnoreBlockEvents := true;
  try
    cbBlockSlices0.Checked := fDrawBlock.Segments[0];
    cbBlockSlices1.Checked := fDrawBlock.Segments[1];
    cbBlockSlices2.Checked := fDrawBlock.Segments[2];
    cbBlockSlices3.Checked := fDrawBlock.Segments[3];
    ebMetablock.Text := IntToStr(fDrawBlock.Graphic);
    cbBlockShape.ItemIndex := Integer(fDrawBlock.Shape);
  finally
    fIgnoreBlockEvents := false;
  end;
end;

procedure TFL3DEdit.RedrawDrawBlock;
begin
  imgBlock.Bitmap.BeginUpdate;
  try
    fRenderer.RenderBlock(imgBlock.Bitmap, fDrawBlock, fLevel, false);
    if (imgBlock.Bitmap.Width > 0) and (imgBlock.Bitmap.Height > 0) then
      imgBlock.Scale := Min(imgBlock.Width / imgBlock.Bitmap.Width, imgBlock.Height / imgBlock.Bitmap.Height);
  finally
    imgBlock.Bitmap.EndUpdate;
    imgBlock.Changed;
  end;
end;

procedure TFL3DEdit.SetObjectFromOptions;
var
  GraphicIndex: Integer;
  PairingIndex: Integer absolute GraphicIndex;

  procedure HandleSetStaticObject;
  begin
    GraphicIndex := Min(Max(0, GraphicIndex), 20);
    fDrawObject.RawByte1 := GraphicIndex + 1;
    if fDrawObject.RawByte1 > 20 then fDrawObject.RawByte1 := 1;
  end;

  procedure HandleSetSign;
  begin
    GraphicIndex := Min(Max(0, GraphicIndex), 7);
    if fDrawObject.ObjectType = otSign then
      fDrawObject.RawByte1 := ((GraphicIndex shl 2) or fDrawObject.Orientation) + $20
    else
      fDrawObject.RawByte1 := (GraphicIndex shl 2) + $20;
  end;

  procedure HandleSetAnimatedObject;
  begin
    GraphicIndex := Min(Max(0, GraphicIndex), 3);
    if GraphicIndex = 1 then
      fDrawObject.RawByte1 := $51
    else if fDrawObject.ObjectType = otAnimation then
    begin
      if GraphicIndex = 0 then
        fDrawObject.RawByte1 := $50 + fDrawObject.Orientation
      else
        fDrawObject.RawByte1 := $50 + ((GraphicIndex - 1) * 4) + fDrawObject.Orientation;

      if fDrawObject.RawByte1 = $51 then
        fDrawObject.RawByte1 := $50;
    end else begin
      if GraphicIndex = 0 then
        fDrawObject.RawByte1 := $50
      else
        fDrawObject.RawByte1 := $50 + ((GraphicIndex - 1) * 4);
    end;
  end;

  procedure HandleSetInteractiveObject;
  var
    Rotation: Integer;
  begin
    if fDrawObject.ObjectType = otInteractive then
      Rotation := fDrawObject.Orientation
    else
      Rotation := 0;

    case fLevel.InteractiveObject of
      0, 7: fDrawObject.RawByte1 := $60 or Rotation;
      1, 2, 6: fDrawObject.RawByte1 := $60;
      3: if GraphicIndex >= 1 then fDrawObject.RawByte1 := $64 else fDrawObject.RawByte1 := $60;
      4, 8: begin
              PairingIndex := Min(Max(0, PairingIndex), 3);
              fDrawObject.RawByte1 := $60 + (PairingIndex * 2);
              if cbReceiver.Checked then fDrawObject.RawByte1 := fDrawObject.RawByte1 + 1;
            end;
      5: begin
           PairingIndex := Min(Max(0, PairingIndex), 7);
           fDrawObject.RawByte1 := $60 + PairingIndex;
         end;
    end;
  end;

  procedure HandleSetWall;
  begin
    GraphicIndex := Min(Max(0, GraphicIndex), 15);
    if fDrawObject.ObjectType = otWall then
      fDrawObject.RawByte1 := ((fDrawObject.Orientation shl 4) + $70) + GraphicIndex
    else
      fDrawObject.RawByte1 := $70 + GraphicIndex;
  end;

begin
  if fIgnoreObjectEvents then Exit;

  GraphicIndex := StrToIntDef(ebObjectGraphic.Text, 0);

  case cbObjectType.ItemIndex of
    0: fDrawObject.RawByte1 := 0; // None
    1: HandleSetStaticObject; // Static
    2: HandleSetSign; // Sign
    3: HandleSetAnimatedObject; // Animated
    4: HandleSetInteractiveObject; // Interactive
    5: HandleSetWall; // Wall
  end;

  if Length(ebObjectUnknown.Text) = 2 then
    fDrawObject.RawByte2 := StrToIntDef('x' + ebObjectUnknown.Text, 0) and $FF
  else begin
    ebObjectUnknown.Text := '00';
    fDrawObject.RawByte2 := 0;
  end;
end;

procedure TFL3DEdit.SetOptionsFromObject;
  procedure HandleSimpleObject;
  begin
    case fDrawObject.ObjectType of
      otStandard: cbObjectType.ItemIndex := 1;
      otSign: cbObjectType.ItemIndex := 2;
      otWall: cbObjectType.ItemIndex := 5;
      else raise Exception.Create('HandleSimpleObject called for an object type it cannot handle');
    end;

    if fDrawObject.ObjectType = otSign then
      ebObjectGraphic.Text := IntToStr((fDrawObject.GraphicIndex + 4) mod 8) // Need to look into WHY this works and if something is wrong elsewhere.
    else
      ebObjectGraphic.Text := IntToStr(fDrawObject.GraphicIndex);
  end;

  procedure HandleAnimatedObject;
  begin
    cbObjectType.ItemIndex := 3;
    if fDrawObject.GraphicIndex = 1 then
      ebObjectGraphic.Text := '1'
    else
      case fDrawObject.Split of
        osWhole: ebObjectGraphic.Text := '0';
        osLeft: ebObjectGraphic.Text := '2';
        osRight: ebObjectGraphic.Text := '3';
      end;
  end;

  procedure HandleInteractiveObject;
  begin
    cbObjectType.ItemIndex := 4;
    case fLevel.InteractiveObject of
      3: if fDrawObject.IsBlueTrampoline then ebObjectGraphic.Text := '1' else ebObjectGraphic.Text := '0';
      4, 8: begin ebObjectGraphic.Text := IntToStr(fDrawObject.SinglePairing); cbReceiver.Checked := fDrawObject.IsReceiver; end;
      5: ebObjectGraphic.Text := IntToStr(fDrawObject.BidirectionalPairing);
    end;
  end;

begin
  fIgnoreObjectEvents := true;
  try
    case fDrawObject.ObjectType of
      otNothing: cbObjectType.itemIndex := 0;
      otStandard, otSign, otWall: HandleSimpleObject;
      otAnimation: HandleAnimatedObject;
      otInteractive: HandleInteractiveObject;
    end;

    ebObjectUnknown.Text := IntToHex(fDrawObject.RawByte2, 2);
  finally
    fIgnoreObjectEvents := false;
    cbObjectTypeChange(cbObjectType);
  end;
end;

procedure TFL3DEdit.RedrawDrawObject;
begin
  imgObject.Bitmap.BeginUpdate;
  try
    imgObject.Bitmap.DrawMode := dmTransparent;
    fRenderer.DrawObjectForInterface(imgObject.Bitmap, fLevel, fDrawObject);
    if (imgObject.Bitmap.Width > 0) and (imgObject.Bitmap.Height > 0) then
      imgObject.Scale := Min(imgObject.Width / imgObject.Bitmap.Width, imgObject.Height / imgObject.Bitmap.Height);
  finally
    imgObject.Bitmap.EndUpdate;
    imgObject.Changed;
  end;
end;

procedure TFL3DEdit.SetLockedRank(aValue: Integer);
var
  MaxRank: Integer;
  LocalRank: Integer;
begin
  LocalRank := aValue;

  if pagesLevelComponent.ActivePage = tabLand then
    MaxRank := 0
  else if fLockedAxis = AxisY then
    MaxRank := 15
  else
    MaxRank := 31;

  if LocalRank < -1 then
    LocalRank := MaxRank
  else if LocalRank > MaxRank then
    LocalRank := -1;

  case fLockedAxis of
    AxisX: fSelectedBlock.X := LocalRank;
    AxisY: fSelectedBlock.Y := LocalRank;
    AxisZ: fSelectedBlock.Z := LocalRank;
  end;

  SetCoordInfo;
  RedrawLevel;
end;

procedure TFL3DEdit.SetLockedAxis(aAxis: TAxis);
begin
  if pagesLevelComponent.ActivePage = tabLand then
    aAxis := AxisY;

  fLockedAxis := aAxis;

  rgSelectedSlice.ItemIndex := Integer(aAxis);

  if pagesLevelComponent.ActivePage = tabLand then
    SetLockedRank(Min(LockedRank, 0))
  else
    SetLockedRank(LockedRank);
end;

procedure TFL3DEdit.OpenBlockSelectionWindow;
var
  F: TFSelectBlock;
  i: Integer;
begin
  F := TFSelectBlock.Create(self);
  try
    F.Prepare(fRenderer, fLevel, fDrawBlock.Graphic);
    F.ShowModal;

    if (fDrawBlock.Graphic <> F.NewBlockIndex) or (F.RequirePurge) then
    begin
      fDrawBlock.Graphic := F.NewBlockIndex;
      if fDrawBlock.Empty then
        for i := 0 to 3 do
          fDrawBlock.Segments[i] := true;
      SetOptionsFromBlock;
      RedrawDrawBlock;
    end;

    if F.RequirePurge then
      LevelModified := true;
  finally
    F.Free;
  end;
end;

procedure TFL3DEdit.PrepareStylePresets;
var
  i: Integer;
  SL: TStringList;

  CurrentTarget: TComboBox;
  CurrentFilenameBase: String;

  NewIndex: Integer;
  NewName: String;

  AltMode: Boolean;

  RS: TResourceStream;

  procedure Prepare(aBox: TComboBox; NoneValue: Integer = 255; SelectText: Boolean = true);
  begin
    aBox.Items.Clear;
    if SelectText then
      aBox.AddItem('(Select Preset)', nil);
    aBox.AddItem('(None)', TObject(NoneValue));
  end;

  procedure SetIfApplicable(aPattern: String; aTargetPattern: String; aComboBox: TComboBox; aFilenameBase: String);
  begin
    if CompareText(aPattern, aTargetPattern) = 0 then
    begin
      CurrentTarget := aComboBox;
      CurrentFilenameBase := aFilenameBase;
    end;
  end;

begin
  CurrentTarget := nil;
  CurrentFilenameBase := '';

  if not FileExists(AppPath + 'L3DEditPresets.ini') then
  begin
    RS := TResourceStream.Create(HInstance, 'UI', 'PRESETS');
    try
      RS.SaveToFile(AppPath + 'L3DEditPresets.ini');
    finally
      RS.Free;
    end;
  end;

  SL := TStringList.Create;
  try
    SL.LoadFromFile(AppPath + 'L3DEditPresets.ini');

    Prepare(cbTheme, 0, false);
    Prepare(cbTextureSetPreset);
    Prepare(cbLandPreset);
    Prepare(cbObjectPreset);
    Prepare(cbSignPreset);
    Prepare(cbSeaPreset);
    Prepare(cbAnimationPreset);
    Prepare(cbTrapPreset);
    Prepare(cbSkyPreset);
    Prepare(cbWallsPreset);

    for i := 0 to SL.Count-1 do
    begin
      SL[i] := Trim(SL[i]);

      if (Length(SL[i]) = 0) or (SL[i][1] = '#') then Continue;

      if SL[i][1] = '[' then
      begin
        SetIfApplicable(SL[i], '[THEME]', cbTheme, '');
        SetIfApplicable(SL[i], '[TEXTURE]', cbTextureSetPreset, 'TEXTURE');
        SetIfApplicable(SL[i], '[LAND]', cbLandPreset, 'LAND');
        SetIfApplicable(SL[i], '[OBJECT]', cbObjectPreset, 'OBJ');
        SetIfApplicable(SL[i], '[SIGN]', cbSignPreset, 'SIGNS');
        SetIfApplicable(SL[i], '[SEA]', cbSeaPreset, 'SEA');
        SetIfApplicable(SL[i], '[ANIMOBJ]', cbAnimationPreset, 'ANIMOBJ');
        SetIfApplicable(SL[i], '[TRAP]', cbTrapPreset, 'TRAPS');
        SetIfApplicable(SL[i], '[SKY]', cbSkyPreset, 'SKY');
        SetIfApplicable(SL[i], '[WALL]', cbWallsPreset, 'WALLS');
      end else if CurrentTarget <> nil then begin
        if LeftStr(SL[i], 1) = '!' then
        begin
          AltMode := true;
          SL[i] := RightStr(SL[i], Length(SL[i]) - 1);
        end else
          AltMode := false;

        NewIndex := StrToIntDef(Trim(SL.Names[i]), -1);
        if (NewIndex < 0) or (NewIndex > 99) then Continue;
        NewName := Trim(SL.ValueFromIndex[i]);

        if (CurrentFilenameBase = '') or FileExists(AppPath + 'GFX' + PathDelim + CurrentFileNameBase + '.' + LeadZeroStr(NewIndex, 3)) then
        begin
          if AltMode then NewIndex := -1 - NewIndex;
          CurrentTarget.AddItem(NewName, TObject(NewIndex));
        end;
      end;
    end;
  finally
    SL.Free;
  end;
end;

end.

