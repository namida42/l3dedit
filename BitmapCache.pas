unit BitmapCache;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  GR32,
  Generics.Collections,
  Classes, SysUtils;

const
  CACHE_MAX_SIZE = 1500;
  CACHE_TARGET_SIZE = 1000;

type

  { TBitmapCacheEntry }

  TBitmapCacheEntry = class
    private
      fBitmap: TBitmap32;
    public
      Identifier: String;

      constructor Create;
      destructor Destroy; override;

      property Bitmap: TBitmap32 read fBitmap;
  end;

  TCacheEntryList = TObjectList<TBitmapCacheEntry>;

  TBitmapCache = class
    private
      fCacheEntries: TCacheEntryList;

      function GetItem(Identifier: String): TBitmap32;
    public
      constructor Create;
      destructor Destroy; override;

      procedure CacheImage(Identifier: String; Bitmap: TBitmap32);
      procedure Purge;

      property Image[Identifier: String]: TBitmap32 read GetItem; default;
  end;

implementation

{ TBitmapCache }

function TBitmapCache.GetItem(Identifier: String): TBitmap32;
var
  i: Integer;
begin
  for i := 0 to fCacheEntries.Count-1 do
    if fCacheEntries[i].Identifier = Identifier then
    begin
      fCacheEntries.Move(i, 0);
      Result := fCacheEntries[0].Bitmap;
      Exit;
    end;

  Result := nil;
end;

constructor TBitmapCache.Create;
begin
  inherited;
  fCacheEntries := TCacheEntryList.Create;
end;

destructor TBitmapCache.Destroy;
begin
  fCacheEntries.Free;
  inherited Destroy;
end;

procedure TBitmapCache.CacheImage(Identifier: String; Bitmap: TBitmap32);
var
  i: Integer;
  NewEntry: TBitmapCacheEntry;
begin
  if fCacheEntries.Count >= CACHE_MAX_SIZE then
    for i := fCacheEntries.Count-1 downto CACHE_TARGET_SIZE-1 do
      fCacheEntries.Delete(i);

  NewEntry := TBitmapCacheEntry.Create;
  fCacheEntries.Insert(0, NewEntry);

  NewEntry.Identifier := Identifier;
  NewEntry.Bitmap.Assign(Bitmap);
end;

procedure TBitmapCache.Purge;
begin
  fCacheEntries.Clear;
end;

{ TBitmapCacheEntry }

constructor TBitmapCacheEntry.Create;
begin
  inherited;
  fBitmap := TBitmap32.Create;
end;

destructor TBitmapCacheEntry.Destroy;
begin
  fBitmap.Free;
  inherited Destroy;
end;

end.

