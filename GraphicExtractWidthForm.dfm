object FExtractGraphicWidth: TFExtractGraphicWidth
  Left = 418
  Top = 441
  BorderIcons = []
  BorderStyle = bsDialog
  ClientHeight = 93
  ClientWidth = 164
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblGraphicWidth: TLabel
    Left = 8
    Top = 8
    Width = 71
    Height = 13
    Caption = 'Graphic Width:'
    Color = clBtnFace
    ParentColor = False
  end
  object ebGraphicWidth: TEdit
    Left = 96
    Top = 4
    Width = 57
    Height = 24
    NumbersOnly = True
    TabOrder = 0
    OnChange = ebGraphicWidthChange
  end
  object btnOk: TButton
    Left = 8
    Top = 56
    Width = 64
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btnOkClick
  end
  object btnCancel: TButton
    Left = 88
    Top = 56
    Width = 64
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object cbRLE: TCheckBox
    Left = 8
    Top = 32
    Width = 138
    Height = 20
    Caption = 'Side RLE Compression'
    TabOrder = 3
  end
end
