unit GraphicExtractWidthForm;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Math;

type

  { TFExtractGraphicWidth }

  TFExtractGraphicWidth = class(TForm)
    btnOk: TButton;
    btnCancel: TButton;
    cbRLE: TCheckBox;
    ebGraphicWidth: TEdit;
    lblGraphicWidth: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure cbRLEChange(Sender: TObject);
    procedure ebGraphicWidthChange(Sender: TObject);
  private class var
    ShownSkyWarning: Boolean;
  private
    NeedSkyWarning: Boolean;
  public
    GraphicWidth: Integer;
    SideRLE: Boolean;
    procedure SetWidthFromFilename(Filename: String);
  end;

implementation

{$R *.dfm}

{ TFExtractGraphicWidth }

procedure TFExtractGraphicWidth.ebGraphicWidthChange(Sender: TObject);
begin
  GraphicWidth := Max(StrToIntDef(ebGraphicWidth.Text, 1), 1);
end;

procedure TFExtractGraphicWidth.cbRLEChange(Sender: TObject);
begin
  SideRLE := cbRLE.Checked;
end;

procedure TFExtractGraphicWidth.btnOkClick(Sender: TObject);
begin
  if NeedSkyWarning and (GraphicWidth = 1024) then
  begin
    ShownSkyWarning := true;
    ShowMessage('If width 1024 does not work correctly for a sky graphic file, try 200 instead.');
  end;
end;

procedure TFExtractGraphicWidth.SetWidthFromFilename(Filename: String);
const
  PRESET_COUNT = 10;
  PRESET_NAMES: array[0..PRESET_COUNT-1] of String =
    ('ANIMOBJ', 'BGRD', 'LAND', 'OBJ', 'SEA', 'SIGNS', 'SKY', 'TEXTURE', 'TRAP', 'WALLS');
  PRESET_WIDTHS: array[0..PRESET_COUNT-1] of Integer =
    (64, 320, 128, 64, 64, 64, 1024, 64, 64, 64);
  PRESET_RLE: array[0..PRESET_COUNT-1] of Boolean =
    (false, false, false, false, false, false, false, false, false, false);

  FALLBACK_WIDTH = 64;
  FALLBACK_RLE = false;
var
  i: Integer;
begin
  {%H-}Filename := Uppercase(ChangeFileExt(ExtractFileName(Filename), ''));
  for i := 0 to PRESET_COUNT-1 do
    if Filename = PRESET_NAMES[i] then
    begin
      ebGraphicWidth.Text := IntToStr(PRESET_WIDTHS[i]);
      cbRLE.Checked := PRESET_RLE[i];

      if (Filename = 'SKY') and not ShownSkyWarning then
        NeedSkyWarning := true;

      Exit;
    end;

  ebGraphicWidth.Text := IntToStr(FALLBACK_WIDTH);
  cbRLE.Checked := FALLBACK_RLE;
end;

end.

