object FL3DEdit: TFL3DEdit
  Left = 469
  Top = 186
  Caption = 'L3DEdit'
  ClientHeight = 574
  ClientWidth = 899
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = True
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  DesignSize = (
    899
    574)
  PixelsPerInch = 96
  TextHeight = 13
  object panelLevelInfo: TPanel
    Left = 0
    Top = 0
    Width = 280
    Height = 445
    TabOrder = 7
    DesignSize = (
      280
      445)
    object pagesLevelInfo: TPageControl
      Left = 0
      Top = 0
      Width = 282
      Height = 445
      ActivePage = tabStyles
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      object tabGeneral: TTabSheet
        Caption = 'General'
        object lblTitle: TLabel
          Left = 8
          Top = 16
          Width = 20
          Height = 13
          Caption = 'Title'
          Color = clBtnFace
          FocusControl = ebLevelTitle
          ParentColor = False
        end
        object lblComment: TLabel
          Left = 8
          Top = 44
          Width = 48
          Height = 13
          Caption = 'Comment '
          Color = clBtnFace
          FocusControl = ebComment
          ParentColor = False
        end
        object lblLemmings: TLabel
          Left = 8
          Top = 80
          Width = 49
          Height = 13
          Caption = 'Lemmings '
          Color = clBtnFace
          FocusControl = ebLemmings
          ParentColor = False
        end
        object lblSaveRequirement: TLabel
          Left = 8
          Top = 108
          Width = 42
          Height = 13
          Caption = 'To Save '
          Color = clBtnFace
          FocusControl = ebSaveRequirement
          ParentColor = False
        end
        object lblTimeLimit: TLabel
          Left = 8
          Top = 136
          Width = 49
          Height = 13
          Caption = 'Time Limit '
          Color = clBtnFace
          FocusControl = ebTimeMinutes
          ParentColor = False
        end
        object lblReleaseRate: TLabel
          Left = 8
          Top = 164
          Width = 64
          Height = 13
          Caption = 'Release Rate'
          Color = clBtnFace
          FocusControl = ebReleaseRate
          ParentColor = False
        end
        object ebLevelTitle: TEdit
          Left = 72
          Top = 8
          Width = 192
          Height = 21
          MaxLength = 31
          TabOrder = 0
          OnChange = LevelPropertyChange
        end
        object ebComment: TEdit
          Left = 72
          Top = 36
          Width = 192
          Height = 21
          MaxLength = 31
          TabOrder = 1
          OnChange = LevelPropertyChange
        end
        object ebLemmings: TEdit
          Left = 80
          Top = 72
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 2
          OnChange = LevelPropertyChange
        end
        object ebSaveRequirement: TEdit
          Left = 80
          Top = 100
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 3
          OnChange = LevelPropertyChange
        end
        object ebTimeMinutes: TEdit
          Left = 80
          Top = 128
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 4
          OnChange = LevelPropertyChange
        end
        object ebReleaseRate: TEdit
          Left = 80
          Top = 156
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 5
          OnChange = LevelPropertyChange
        end
        object ebTimeSeconds: TEdit
          Left = 152
          Top = 128
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 6
          OnChange = LevelPropertyChange
        end
        object cbFixedPreviewCamera: TCheckBox
          Tag = 6
          Left = 8
          Top = 192
          Width = 137
          Height = 20
          Caption = 'Fixed Preview Camera'
          TabOrder = 7
          OnClick = cbRenderOptionChange
        end
        object cbDisableMinimap: TCheckBox
          Tag = 12
          Left = 8
          Top = 210
          Width = 110
          Height = 20
          Caption = 'Disable Minimap'
          TabOrder = 8
          OnClick = cbRenderOptionChange
        end
        object cbSolidBottom: TCheckBox
          Tag = 7
          Left = 8
          Top = 246
          Width = 121
          Height = 20
          Caption = 'Solid Level Bottom'
          TabOrder = 10
          OnClick = cbRenderOptionChange
        end
        object cbSlipperyBottom: TCheckBox
          Tag = 16
          Left = 8
          Top = 264
          Width = 137
          Height = 20
          Caption = 'Slippery Level Bottom'
          TabOrder = 11
          OnClick = cbRenderOptionChange
        end
        object cbDisableQuickJump: TCheckBox
          Tag = 3
          Left = 8
          Top = 228
          Width = 126
          Height = 20
          Caption = 'Disable Quick Jump'
          TabOrder = 9
          OnClick = cbRenderOptionChange
        end
        object cbSlipperyLands: TCheckBox
          Tag = 17
          Left = 8
          Top = 282
          Width = 97
          Height = 20
          Caption = 'Slippery Lands'
          TabOrder = 12
          OnClick = cbRenderOptionChange
        end
      end
      object tabSkillset: TTabSheet
        Caption = 'Skillset'
        object lblBlockers: TLabel
          Left = 8
          Top = 16
          Width = 39
          Height = 13
          Caption = 'Blockers'
          Color = clBtnFace
          FocusControl = ebBlockers
          ParentColor = False
        end
        object lblTurners: TLabel
          Left = 8
          Top = 48
          Width = 37
          Height = 13
          Caption = 'Turners'
          Color = clBtnFace
          FocusControl = ebTurners
          ParentColor = False
        end
        object lblBombers: TLabel
          Left = 8
          Top = 80
          Width = 41
          Height = 13
          Caption = 'Bombers'
          Color = clBtnFace
          FocusControl = ebBombers
          ParentColor = False
        end
        object lblBuilders: TLabel
          Left = 8
          Top = 112
          Width = 37
          Height = 13
          Caption = 'Builders'
          Color = clBtnFace
          FocusControl = ebBuilders
          ParentColor = False
        end
        object lblBashers: TLabel
          Left = 8
          Top = 144
          Width = 38
          Height = 13
          Caption = 'Bashers'
          Color = clBtnFace
          FocusControl = ebBashers
          ParentColor = False
        end
        object lblMiners: TLabel
          Left = 8
          Top = 176
          Width = 31
          Height = 13
          Caption = 'Miners'
          Color = clBtnFace
          FocusControl = ebMiners
          ParentColor = False
        end
        object lblDiggers: TLabel
          Left = 8
          Top = 208
          Width = 36
          Height = 13
          Caption = 'Diggers'
          Color = clBtnFace
          FocusControl = ebDiggers
          ParentColor = False
        end
        object lblClimbers: TLabel
          Left = 8
          Top = 240
          Width = 40
          Height = 13
          Caption = 'Climbers'
          Color = clBtnFace
          FocusControl = ebClimbers
          ParentColor = False
        end
        object lblFloaters: TLabel
          Left = 8
          Top = 272
          Width = 39
          Height = 13
          Caption = 'Floaters'
          Color = clBtnFace
          FocusControl = ebFloaters
          ParentColor = False
        end
        object ebBlockers: TEdit
          Left = 72
          Top = 8
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 0
          OnChange = LevelPropertyChange
        end
        object ebTurners: TEdit
          Left = 72
          Top = 40
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 1
          OnChange = LevelPropertyChange
        end
        object ebBombers: TEdit
          Left = 72
          Top = 72
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 2
          OnChange = LevelPropertyChange
        end
        object ebBuilders: TEdit
          Left = 72
          Top = 104
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 3
          OnChange = LevelPropertyChange
        end
        object ebBashers: TEdit
          Left = 72
          Top = 136
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 4
          OnChange = LevelPropertyChange
        end
        object ebMiners: TEdit
          Left = 72
          Top = 168
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 5
          OnChange = LevelPropertyChange
        end
        object ebDiggers: TEdit
          Left = 72
          Top = 200
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 6
          OnChange = LevelPropertyChange
        end
        object ebClimbers: TEdit
          Left = 72
          Top = 232
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 7
          OnChange = LevelPropertyChange
        end
        object ebFloaters: TEdit
          Left = 72
          Top = 264
          Width = 56
          Height = 21
          MaxLength = 2
          NumbersOnly = True
          TabOrder = 8
          OnChange = LevelPropertyChange
        end
      end
      object tabStyles: TTabSheet
        Caption = 'Styles'
        object lblTheme: TLabel
          Left = 8
          Top = 16
          Width = 32
          Height = 13
          Caption = 'Theme'
          Color = clBtnFace
          FocusControl = cbTheme
          ParentColor = False
        end
        object lblMusic: TLabel
          Left = 8
          Top = 41
          Width = 26
          Height = 13
          Caption = 'Music'
          Color = clBtnFace
          FocusControl = cbMusic
          ParentColor = False
        end
        object lblTextureSet: TLabel
          Left = 8
          Top = 65
          Width = 57
          Height = 13
          Caption = 'Texture Set'
          Color = clBtnFace
          FocusControl = ebTextureSet
          ParentColor = False
        end
        object lblLand: TLabel
          Left = 8
          Top = 90
          Width = 23
          Height = 13
          Caption = 'Land'
          Color = clBtnFace
          FocusControl = ebLand
          ParentColor = False
        end
        object lblObjects: TLabel
          Left = 8
          Top = 113
          Width = 37
          Height = 13
          Caption = 'Objects'
          Color = clBtnFace
          FocusControl = ebObjects
          ParentColor = False
        end
        object lblSign: TLabel
          Left = 8
          Top = 140
          Width = 25
          Height = 13
          Caption = 'Signs'
          Color = clBtnFace
          FocusControl = ebSign
          ParentColor = False
        end
        object lblSea: TLabel
          Left = 8
          Top = 163
          Width = 18
          Height = 13
          Caption = 'Sea'
          Color = clBtnFace
          FocusControl = ebSea
          ParentColor = False
        end
        object lblAnimation: TLabel
          Left = 8
          Top = 188
          Width = 47
          Height = 13
          Caption = 'Animation'
          Color = clBtnFace
          FocusControl = ebAnimation
          ParentColor = False
        end
        object lblInteractive: TLabel
          Left = 8
          Top = 213
          Width = 22
          Height = 13
          Caption = 'Trap'
          Color = clBtnFace
          FocusControl = ebInteractive
          ParentColor = False
        end
        object lblSky: TLabel
          Left = 8
          Top = 239
          Width = 17
          Height = 13
          Caption = 'Sky'
          Color = clBtnFace
          FocusControl = ebSky
          ParentColor = False
        end
        object lblWalls: TLabel
          Left = 8
          Top = 264
          Width = 25
          Height = 13
          Caption = 'Walls'
          Color = clBtnFace
          FocusControl = ebWalls
          ParentColor = False
        end
        object lblSeaMovement: TLabel
          Left = 8
          Top = 392
          Width = 71
          Height = 13
          Caption = 'Sea Movement'
          Color = clBtnFace
          ParentColor = False
        end
        object lblSeaMovementX: TLabel
          Left = 100
          Top = 392
          Width = 6
          Height = 13
          Caption = 'X'
          Color = clBtnFace
          FocusControl = ebSeaMovementX
          ParentColor = False
        end
        object lblSeaMovementZ: TLabel
          Left = 164
          Top = 392
          Width = 6
          Height = 13
          Caption = 'Z'
          Color = clBtnFace
          FocusControl = ebSeaMovementZ
          ParentColor = False
        end
        object cbTheme: TComboBox
          Left = 72
          Top = 8
          Width = 192
          Height = 21
          Style = csDropDownList
          ItemIndex = 1
          TabOrder = 0
          Text = 'Castle'
          OnChange = LevelPropertyChange
          Items.Strings = (
            'Null'
            'Castle'
            'Egypt'
            'Space'
            'Sweets'
            'Golf'
            'Computer'
            'Army'
            'Maze'
            'Circus'
            'Lemgo')
        end
        object cbMusic: TComboBox
          Left = 72
          Top = 33
          Width = 192
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = 'Standard'
          OnChange = LevelPropertyChange
          Items.Strings = (
            'Standard'
            'Alternate'
            'Standard (Remix)')
        end
        object ebTextureSet: TEdit
          Left = 72
          Top = 58
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 2
          OnExit = StylePropertyChange
        end
        object ebLand: TEdit
          Left = 72
          Top = 83
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 4
          OnExit = StylePropertyChange
        end
        object ebObjects: TEdit
          Left = 72
          Top = 108
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 6
          OnExit = StylePropertyChange
        end
        object ebSign: TEdit
          Left = 72
          Top = 133
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 8
          OnExit = StylePropertyChange
        end
        object ebSea: TEdit
          Left = 72
          Top = 158
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 10
          OnExit = StylePropertyChange
        end
        object ebAnimation: TEdit
          Left = 72
          Top = 183
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 12
          OnExit = StylePropertyChange
        end
        object ebInteractive: TEdit
          Left = 72
          Top = 208
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 14
          OnExit = StylePropertyChange
        end
        object ebSky: TEdit
          Left = 72
          Top = 233
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 16
          OnExit = StylePropertyChange
        end
        object ebWalls: TEdit
          Left = 72
          Top = 258
          Width = 40
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 18
          OnExit = StylePropertyChange
        end
        object cbTextureSetPreset: TComboBox
          Left = 120
          Top = 58
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 3
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbLandPreset: TComboBox
          Tag = 1
          Left = 120
          Top = 83
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 5
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbObjectPreset: TComboBox
          Tag = 2
          Left = 120
          Top = 108
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 7
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbSignPreset: TComboBox
          Tag = 3
          Left = 120
          Top = 133
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 9
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbSeaPreset: TComboBox
          Tag = 4
          Left = 120
          Top = 158
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 11
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbAnimationPreset: TComboBox
          Tag = 5
          Left = 120
          Top = 183
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 13
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbTrapPreset: TComboBox
          Tag = 6
          Left = 120
          Top = 208
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 15
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbSkyPreset: TComboBox
          Tag = 7
          Left = 120
          Top = 233
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 17
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbWallsPreset: TComboBox
          Tag = 8
          Left = 120
          Top = 258
          Width = 144
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemIndex = 0
          TabOrder = 19
          Text = '(Select preset)'
          OnSelect = StylePresetChange
          Items.Strings = (
            '(Select preset)')
        end
        object cbNonAnimatingSea: TCheckBox
          Tag = 4
          Left = 8
          Top = 288
          Width = 126
          Height = 20
          Caption = 'Non-Animating Sea'
          TabOrder = 20
          OnClick = cbRenderOptionChange
        end
        object cbLargeSeaTexture: TCheckBox
          Tag = 5
          Left = 8
          Top = 306
          Width = 115
          Height = 20
          Caption = 'Large Sea Texture'
          TabOrder = 21
          OnClick = cbRenderOptionChange
        end
        object ebSeaMovementX: TEdit
          Left = 112
          Top = 384
          Width = 40
          Height = 21
          MaxLength = 4
          TabOrder = 25
          OnChange = LevelPropertyChange
          OnKeyPress = ebNumbersOnlyAllowNegativeKeyPress
        end
        object ebSeaMovementZ: TEdit
          Left = 176
          Top = 384
          Width = 40
          Height = 21
          MaxLength = 4
          TabOrder = 26
          OnChange = LevelPropertyChange
          OnKeyPress = ebNumbersOnlyAllowNegativeKeyPress
        end
        object cbLargeLandTexture: TCheckBox
          Tag = 8
          Left = 8
          Top = 324
          Width = 171
          Height = 20
          Caption = 'Single-Graphic Land Texture'
          TabOrder = 22
          OnClick = cbRenderOptionChange
        end
        object cbFullHeightSkyTexture: TCheckBox
          Tag = 9
          Left = 8
          Top = 342
          Width = 146
          Height = 20
          Caption = 'Full-Height Sky Texture'
          TabOrder = 23
          OnClick = cbRenderOptionChange
        end
        object cbZapLemmings: TCheckBox
          Tag = 15
          Left = 8
          Top = 360
          Width = 170
          Height = 20
          Caption = 'Zapping Replaces Drowning'
          TabOrder = 24
          OnClick = cbRenderOptionChange
        end
      end
    end
  end
  object levelImage: TImage32
    Left = 288
    Top = 40
    Width = 605
    Height = 405
    Anchors = [akLeft, akTop, akRight, akBottom]
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCustom
    Color = clFuchsia
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 6
    OnKeyDown = levelImageKeyDown
    OnMouseDown = levelImageMouseDown
    OnMouseMove = levelImageMouseMove
    OnMouseUp = levelImageMouseUp
    OnMouseWheel = levelImageMouseWheel
    OnMouseLeave = levelImageMouseLeave
  end
  object btnRotateCCW: TButton
    Left = 288
    Top = 8
    Width = 75
    Height = 24
    Caption = '<-- Rotate'
    TabOrder = 0
    OnClick = btnRotateCCWClic
  end
  object btnRotateCW: TButton
    Left = 368
    Top = 8
    Width = 75
    Height = 24
    Caption = 'Rotate -->'
    TabOrder = 1
    OnClick = btnRotateCWClick
  end
  object btnPerspectiveIncrease: TButton
    Left = 527
    Top = 8
    Width = 75
    Height = 24
    Anchors = [akTop]
    Caption = 'Persp. +'
    TabOrder = 2
    OnClick = btnPerspectiveIncreaseClick
  end
  object btnPerspectiveDecrease: TButton
    Left = 612
    Top = 8
    Width = 75
    Height = 24
    Caption = 'Persp. -'
    TabOrder = 3
    OnClick = btnPerspectiveDecreaseClick
  end
  object btnSkewLeft: TButton
    Left = 738
    Top = 8
    Width = 75
    Height = 24
    Anchors = [akTop, akRight]
    Caption = '<-- Skew'
    TabOrder = 4
    OnClick = btnSkewLeftClick
  end
  object btnSkewRight: TButton
    Left = 818
    Top = 8
    Width = 75
    Height = 24
    Anchors = [akTop, akRight]
    Caption = 'Skew -->'
    TabOrder = 5
    OnClick = btnSkewRightClick
  end
  object pagesLevelComponent: TPageControl
    Left = 0
    Top = 453
    Width = 280
    Height = 121
    ActivePage = tabObjects
    TabOrder = 8
    OnChange = pagesLevelComponentChange
    object tabLand: TTabSheet
      Caption = 'Land'
      object lblLandInstance: TLabel
        Left = 8
        Top = 12
        Width = 23
        Height = 13
        Caption = 'Land'
        Color = clBtnFace
        FocusControl = cbLand
        ParentColor = False
      end
      object lblLandGraphic: TLabel
        Left = 8
        Top = 59
        Width = 36
        Height = 13
        Caption = 'Graphic'
        Color = clBtnFace
        FocusControl = ebLandGraphic
        ParentColor = False
      end
      object lblLandShade: TLabel
        Left = 120
        Top = 59
        Width = 30
        Height = 13
        Caption = 'Shade'
        Color = clBtnFace
        ParentColor = False
      end
      object cbLand: TComboBox
        Left = 56
        Top = 8
        Width = 136
        Height = 21
        Style = csDropDownList
        TabOrder = 0
        OnChange = cbLandChange
      end
      object ebLandGraphic: TEdit
        Left = 56
        Top = 56
        Width = 44
        Height = 19
        AutoSize = False
        MaxLength = 1
        NumbersOnly = True
        TabOrder = 2
        Text = '0'
        OnChange = LandGraphicPropertyChange
      end
      object tbLandShade: TTrackBar
        Left = 160
        Top = 56
        Width = 100
        Height = 25
        Max = 7
        TabOrder = 3
        OnChange = LandGraphicPropertyChange
      end
      object btnDeleteLand: TButton
        Left = 200
        Top = 7
        Width = 61
        Height = 25
        Caption = 'Delete'
        TabOrder = 1
        OnClick = btnDeleteLandClick
      end
    end
    object tabBlocks: TTabSheet
      Caption = 'Blocks'
      object lblMetablock: TLabel
        Left = 96
        Top = 16
        Width = 52
        Height = 13
        Caption = 'Metablock:'
        Color = clBtnFace
        ParentColor = False
      end
      object imgBlock: TImage32
        Left = 24
        Top = 6
        Width = 64
        Height = 84
        Bitmap.ResamplerClassName = 'TNearestResampler'
        BitmapAlign = baCenter
        Color = clFuchsia
        ParentColor = False
        Scale = 1.000000000000000000
        ScaleMode = smNormal
        TabOrder = 2
      end
      object cbBlockSlices0: TCheckBox
        Left = 0
        Top = 14
        Width = 20
        Height = 19
        TabOrder = 3
        OnClick = DrawBlockPropertyChange
      end
      object cbBlockSlices1: TCheckBox
        Tag = 1
        Left = 0
        Top = 32
        Width = 20
        Height = 19
        TabOrder = 4
        OnClick = DrawBlockPropertyChange
      end
      object cbBlockSlices2: TCheckBox
        Tag = 2
        Left = 0
        Top = 48
        Width = 20
        Height = 19
        TabOrder = 5
        OnClick = DrawBlockPropertyChange
      end
      object cbBlockSlices3: TCheckBox
        Tag = 3
        Left = 0
        Top = 66
        Width = 20
        Height = 19
        TabOrder = 6
        OnClick = DrawBlockPropertyChange
      end
      object ebMetablock: TEdit
        Left = 156
        Top = 13
        Width = 44
        Height = 19
        AutoSize = False
        MaxLength = 2
        NumbersOnly = True
        TabOrder = 0
        Text = '0'
        OnExit = DrawBlockPropertyChange
      end
      object btnSelectMetablock: TButton
        Left = 208
        Top = 15
        Width = 20
        Height = 17
        Caption = '...'
        TabOrder = 1
        OnClick = btnSelectMetablockClick
      end
      object cbBlockShape: TComboBox
        Left = 96
        Top = 40
        Width = 132
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 7
        Text = 'Standard'
        OnChange = DrawBlockPropertyChange
        Items.Strings = (
          'Standard'
          'Pyramid A Up'
          'Pyramid A Down'
          'Pyramid B Up '
          'Pyramid B Down'
          '45'#194#176' Above'
          '45'#194#176' Below'
          'Deflector'
          '22.5'#194#176' Above'
          '22.5'#194#176' Below'
          'Corner A Up'
          'Corner A Down'
          'Corner B Up'
          'Corner B Down')
      end
      object btnRotateBlockClockwise: TButton
        Left = 97
        Top = 69
        Width = 63
        Height = 17
        Caption = '<-- Rotate'
        TabOrder = 8
        OnClick = btnRotateBlockAntiClockwiseClick
      end
      object btnRotateBlockAntiClockwise: TButton
        Left = 165
        Top = 69
        Width = 63
        Height = 17
        Caption = 'Rotate -->'
        TabOrder = 9
        OnClick = btnRotateBlockClockwiseClick
      end
    end
    object tabObjects: TTabSheet
      Caption = 'Objects'
      object lblObjectGraphic: TLabel
        Left = 88
        Top = 40
        Width = 36
        Height = 13
        Caption = 'Graphic'
        Color = clBtnFace
        ParentColor = False
        Visible = False
      end
      object lblObjectUnknown: TLabel
        Left = 198
        Top = 40
        Width = 20
        Height = 13
        Caption = '????'
        Color = clBtnFace
        ParentColor = False
      end
      object imgObject: TImage32
        Left = 8
        Top = 6
        Width = 64
        Height = 84
        Bitmap.ResamplerClassName = 'TNearestResampler'
        BitmapAlign = baCenter
        Color = clFuchsia
        ParentColor = False
        Scale = 1.000000000000000000
        ScaleMode = smNormal
        TabOrder = 2
      end
      object btnRotateObjectClockwise: TButton
        Left = 86
        Top = 69
        Width = 63
        Height = 17
        Caption = '<-- Rotate'
        Enabled = False
        TabOrder = 4
        Visible = False
        OnClick = btnRotateObjectClockwiseClick
      end
      object btnRotateObjectAntiClockwise: TButton
        Left = 154
        Top = 69
        Width = 63
        Height = 17
        Caption = 'Rotate -->'
        Enabled = False
        TabOrder = 5
        Visible = False
        OnClick = btnRotateObjectAntiClockwiseClick
      end
      object cbObjectType: TComboBox
        Left = 86
        Top = 8
        Width = 132
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 0
        Text = 'None'
        OnChange = cbObjectTypeChange
        Items.Strings = (
          'None'
          'Static Object'
          'Sign Graphic'
          'Animated Object'
          'Interactive Object'
          'Wall Graphic')
      end
      object ebObjectGraphic: TEdit
        Left = 136
        Top = 37
        Width = 44
        Height = 19
        AutoSize = False
        Enabled = False
        MaxLength = 2
        NumbersOnly = True
        TabOrder = 1
        Text = '0'
        Visible = False
        OnExit = ebObjectGraphicEditingDone
      end
      object cbReceiver: TCheckBox
        Left = 88
        Top = 64
        Width = 65
        Height = 20
        Caption = 'Receiver'
        Enabled = False
        TabOrder = 3
        Visible = False
        OnClick = cbReceiverClick
      end
      object ebObjectUnknown: TEdit
        Left = 224
        Top = 37
        Width = 44
        Height = 19
        AutoSize = False
        MaxLength = 2
        TabOrder = 6
        Text = '00'
        OnExit = ebObjectUnknownExit
        OnKeyPress = ebObjectUnknownKeyPress
      end
    end
    object tabCameras: TTabSheet
      Caption = 'Cameras'
      object lblCamera: TLabel
        Left = 4
        Top = 12
        Width = 40
        Height = 13
        Caption = 'Camera '
        Color = clBtnFace
        ParentColor = False
      end
      object cbActiveCamera: TComboBox
        Left = 72
        Top = 8
        Width = 152
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 0
        Text = 'Camera 1'
        Items.Strings = (
          'Camera 1'
          'Camera 2'
          'Camera 3'
          'Camera 4'
          'Preview Pivot')
      end
      object btnRotateCameraAnticlockwise: TButton
        Left = 120
        Top = 56
        Width = 75
        Height = 25
        Caption = 'Rotate -->'
        TabOrder = 2
        OnClick = btnRotateCameraAnticlockwiseClick
      end
      object btnRotateCameraClockwise: TButton
        Left = 32
        Top = 56
        Width = 75
        Height = 25
        Caption = '<-- Rotate'
        TabOrder = 1
        OnClick = btnRotateCameraClockwiseClick
      end
    end
    object tabBoundaries: TTabSheet
      Caption = 'Boundaries'
      object lblBoundary: TLabel
        Left = 8
        Top = 16
        Width = 46
        Height = 13
        Caption = 'Boundary'
        Color = clBtnFace
        ParentColor = False
      end
      object cbBoundary: TComboBox
        Left = 80
        Top = 12
        Width = 140
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 0
        Text = 'Min X'
        Items.Strings = (
          'Min X'
          'Max X'
          'Max Y'
          'Min Z'
          'Max Z')
      end
    end
  end
  object pnBottomArea: TPanel
    Left = 288
    Top = 453
    Width = 611
    Height = 121
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 9
    DesignSize = (
      611
      121)
    object lblCoords: TLabel
      Left = 489
      Top = 98
      Width = 116
      Height = 16
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Color = clBtnFace
      ParentColor = False
    end
    object lblRenderRange: TLabel
      Left = 8
      Top = 4
      Width = 69
      Height = 13
      Caption = 'Render Range'
      Color = clBtnFace
      ParentColor = False
    end
    object lblRenderRangeX: TLabel
      Left = 24
      Top = 22
      Width = 6
      Height = 13
      Caption = 'X'
      Color = clBtnFace
      FocusControl = ebRangeXLow
      ParentColor = False
    end
    object lblRenderRangeXTo: TLabel
      Left = 80
      Top = 22
      Width = 10
      Height = 13
      Caption = 'to'
      Color = clBtnFace
      FocusControl = ebRangeXHigh
      ParentColor = False
    end
    object lblRenderRangeYTo: TLabel
      Left = 80
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Color = clBtnFace
      FocusControl = ebRangeYHigh
      ParentColor = False
    end
    object lblRenderRangeY: TLabel
      Left = 24
      Top = 44
      Width = 6
      Height = 13
      Caption = 'Y'
      Color = clBtnFace
      FocusControl = ebRangeYLow
      ParentColor = False
    end
    object lblRenderRangeZTo: TLabel
      Left = 80
      Top = 66
      Width = 10
      Height = 13
      Caption = 'to'
      Color = clBtnFace
      FocusControl = ebRangeZHigh
      ParentColor = False
    end
    object lblRenderRangeZ: TLabel
      Left = 24
      Top = 66
      Width = 6
      Height = 13
      Caption = 'Z'
      Color = clBtnFace
      FocusControl = ebRangeZLow
      ParentColor = False
    end
    object lblSelectedSliceIndex: TLabel
      Left = 184
      Top = 56
      Width = 33
      Height = 16
      Alignment = taCenter
      AutoSize = False
      Caption = '0'
      Color = clBtnFace
      ParentColor = False
    end
    object lblRenderPieces: TLabel
      Left = 272
      Top = 4
      Width = 71
      Height = 13
      Caption = 'Render Pieces '
      Color = clBtnFace
      ParentColor = False
    end
    object ebRangeXLow: TEdit
      Left = 40
      Top = 20
      Width = 32
      Height = 20
      AutoSize = False
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 3
      Text = '0'
    end
    object ebRangeXHigh: TEdit
      Left = 100
      Top = 20
      Width = 32
      Height = 20
      AutoSize = False
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 4
      Text = '31'
    end
    object ebRangeYHigh: TEdit
      Left = 100
      Top = 42
      Width = 32
      Height = 20
      AutoSize = False
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 8
      Text = '15'
    end
    object ebRangeYLow: TEdit
      Left = 40
      Top = 42
      Width = 32
      Height = 20
      AutoSize = False
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 7
      Text = '0'
    end
    object ebRangeZHigh: TEdit
      Left = 100
      Top = 64
      Width = 32
      Height = 20
      AutoSize = False
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 13
      Text = '31'
    end
    object ebRangeZLow: TEdit
      Left = 40
      Top = 64
      Width = 32
      Height = 20
      AutoSize = False
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 12
      Text = '0'
    end
    object btnRenderRangeApply: TButton
      Left = 48
      Top = 88
      Width = 75
      Height = 25
      Caption = 'Apply'
      TabOrder = 17
      OnClick = btnRenderRangeApplyClick
    end
    object btnSelectedSlicePlus: TButton
      Tag = 1
      Left = 216
      Top = 50
      Width = 24
      Height = 25
      Caption = '+'
      TabOrder = 10
      OnClick = btnSelectedSliceModifierClick
    end
    object btnSelectedSliceMinus: TButton
      Tag = -1
      Left = 160
      Top = 50
      Width = 24
      Height = 25
      Caption = '-'
      TabOrder = 9
      OnClick = btnSelectedSliceModifierClick
    end
    object cbDisableTransparency: TCheckBox
      Left = 156
      Top = 88
      Width = 88
      Height = 20
      Caption = 'No Invisibles'
      TabOrder = 15
      OnClick = cbDisableTransparencyChange
    end
    object cbRenderSkySea: TCheckBox
      Left = 280
      Top = 18
      Width = 69
      Height = 20
      Caption = 'Sea / Sky'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = cbRenderTypeOptionChange
    end
    object cbRenderLand: TCheckBox
      Tag = 1
      Left = 280
      Top = 36
      Width = 46
      Height = 20
      Caption = 'Land'
      Checked = True
      State = cbChecked
      TabOrder = 5
      OnClick = cbRenderTypeOptionChange
    end
    object cbRenderBlocks: TCheckBox
      Tag = 2
      Left = 280
      Top = 54
      Width = 54
      Height = 20
      Caption = 'Blocks'
      Checked = True
      State = cbChecked
      TabOrder = 11
      OnClick = cbRenderTypeOptionChange
    end
    object cbRenderObjects: TCheckBox
      Tag = 3
      Left = 280
      Top = 72
      Width = 60
      Height = 20
      Caption = 'Objects'
      Checked = True
      State = cbChecked
      TabOrder = 14
      OnClick = cbRenderTypeOptionChange
    end
    object cbRenderCameras: TCheckBox
      Tag = 4
      Left = 280
      Top = 89
      Width = 66
      Height = 20
      Caption = 'Cameras'
      Checked = True
      State = cbChecked
      TabOrder = 16
      OnClick = cbRenderTypeOptionChange
    end
    object rgSelectedSlice: TRadioGroup
      Left = 156
      Top = 7
      Width = 88
      Height = 37
      Caption = 'Selected Slice'
      Columns = 3
      ItemIndex = 1
      Items.Strings = (
        'X'
        'Y'
        'Z')
      TabOrder = 1
      OnClick = tbLockAxisChange
    end
    object rgDrawMode: TRadioGroup
      Left = 371
      Top = 9
      Width = 83
      Height = 75
      Caption = 'Mode'
      ItemIndex = 0
      Items.Strings = (
        'Standard'
        'Mark Block'
        'Fill Region'
        'Shift Level')
      TabOrder = 6
    end
    object cbDisableMouseInput: TCheckBox
      Left = 472
      Top = 13
      Width = 131
      Height = 20
      Caption = 'Disable Mouse Input'
      TabOrder = 0
    end
  end
  object MainMenu1: TMainMenu
    Left = 256
    Top = 48
    object miFile: TMenuItem
      Caption = 'File'
      object miNew: TMenuItem
        Caption = 'New'
        ShortCut = 16462
        OnClick = miNewClick
      end
      object miOpen: TMenuItem
        Caption = 'Open...'
        ShortCut = 16463
        OnClick = miOpenClick
      end
      object miSave: TMenuItem
        Caption = 'Save'
        ShortCut = 16467
        OnClick = miSaveClick
      end
      object miSaveAs: TMenuItem
        Caption = 'Save As...'
        ShortCut = 24659
        OnClick = miSaveAsClick
      end
      object miFileSeperator1: TMenuItem
        Caption = '-'
      end
      object miImportBlockData: TMenuItem
        Caption = 'Import Block Data...'
        ShortCut = 16457
        OnClick = miImportBlockDataClick
      end
      object miFileSeperator2: TMenuItem
        Caption = '-'
      end
      object miQuit: TMenuItem
        Caption = 'Quit'
        ShortCut = 32883
        OnClick = miQuitClick
      end
    end
    object miEdit: TMenuItem
      Caption = 'Edit'
      object miUndo: TMenuItem
        Caption = 'Undo'
        ShortCut = 16474
        OnClick = miUndoClick
      end
      object miRedo: TMenuItem
        Caption = 'Redo'
        ShortCut = 24666
        OnClick = miRedoClick
      end
      object miEditSeperator: TMenuItem
        Caption = '-'
      end
      object miEditUnknowns: TMenuItem
        Caption = 'Unknown / Obscure'
        OnClick = miEditUnknownsClick
      end
    end
    object miView: TMenuItem
      Caption = 'View'
      object miZoomIn: TMenuItem
        Caption = 'Zoom In'
        ShortCut = 16571
        OnClick = miZoomInClick
      end
      object miZoomOut: TMenuItem
        Caption = 'Zoom Out'
        ShortCut = 16573
        OnClick = miZoomOutClick
      end
      object miViewSeperator: TMenuItem
        Caption = '-'
      end
      object miGridStyle: TMenuItem
        Caption = 'Grid Style'
        object miGridStandard: TMenuItem
          Caption = 'Standard'
          OnClick = miGridOptionClick
        end
        object miGridCorners: TMenuItem
          Tag = 1
          Caption = 'Corners'
          OnClick = miGridOptionClick
        end
        object miGridNone: TMenuItem
          Tag = 2
          Caption = 'None'
          OnClick = miGridOptionClick
        end
      end
      object miResolution: TMenuItem
        Caption = 'Resolution'
        object miResolutionLowest: TMenuItem
          Caption = 'Lowest'
          OnClick = miResolutionOptionClick
        end
        object miResolutionLow: TMenuItem
          Tag = 1
          Caption = 'Low'
          OnClick = miResolutionOptionClick
        end
        object miResolutionMedium: TMenuItem
          Tag = 2
          Caption = 'Medium'
          OnClick = miResolutionOptionClick
        end
        object miResolutionHigh: TMenuItem
          Tag = 3
          Caption = 'High'
          OnClick = miResolutionOptionClick
        end
        object miResolutionHighest: TMenuItem
          Tag = 4
          Caption = 'Highest'
          OnClick = miResolutionOptionClick
        end
      end
    end
    object miUtilities: TMenuItem
      Caption = 'Utilities'
      object miCompress: TMenuItem
        Caption = 'RNC Compress'
        object miComL3D: TMenuItem
          Caption = 'Lemmings 3D (Full)'
          OnClick = miComL3DClick
        end
        object miComL3DD1: TMenuItem
          Caption = 'Lemmings 3D (8 Level Demo)'
          OnClick = miComL3DD1Click
        end
        object miComL3DD2: TMenuItem
          Caption = 'Lemmings 3D (10 Level Demo)'
          OnClick = miComL3DD2Click
        end
        object miComL3DW: TMenuItem
          Caption = 'Lemmings 3D Winterland'
          OnClick = miComL3DWClick
        end
        object miComFile: TMenuItem
          Caption = 'Single File...'
          OnClick = miComFileClick
        end
      end
      object miDecompress: TMenuItem
        Caption = 'RNC Decompress'
        object miDecL3D: TMenuItem
          Caption = 'Lemmings 3D (Full)'
          OnClick = miDecL3DClick
        end
        object miDecL3DD1: TMenuItem
          Caption = 'Lemmings 3D (8 Level Demo)'
          OnClick = miDecL3DD1Click
        end
        object miDecL3DD2: TMenuItem
          Caption = 'Lemmings 3D (10 Level Demo)'
          OnClick = miDecL3DD2Click
        end
        object miDecL3DW: TMenuItem
          Caption = 'Lemmings 3D Winterland'
          OnClick = miDecL3DWClick
        end
        object miDecFile: TMenuItem
          Caption = 'Single File...'
          OnClick = miDecFileClick
        end
      end
      object miGraphicFiles: TMenuItem
        Caption = 'Graphic Files'
        object miExtractGraphic: TMenuItem
          Caption = 'Extract Graphic File'
          OnClick = miExtractGraphicClick
        end
        object miImportGraphicFile: TMenuItem
          Caption = 'Import Graphic File'
          OnClick = miImportGraphicFileClick
        end
      end
      object miReorderLevels: TMenuItem
        Caption = 'Reorder Levels'
        OnClick = miReorderLevelsClick
      end
    end
    object miHelp: TMenuItem
      Caption = 'Help'
      object miAbout: TMenuItem
        Caption = 'About'
        OnClick = miAboutClick
      end
    end
    object miDebug: TMenuItem
      Caption = 'Debug'
      object miAboutDebugOptions: TMenuItem
        Caption = 'About Debug Options'
        OnClick = miAboutDebugOptionsClick
      end
      object miBackFaces: TMenuItem
        AutoCheck = True
        Caption = 'Render Back Faces'
        OnClick = miBackFacesClick
      end
      object miLevelScan: TMenuItem
        Caption = 'Level Scan Test'
        OnClick = miLevelScanClick
      end
      object miLevelFlood: TMenuItem
        Caption = 'Level Flood'
        OnClick = miLevelFloodClick
      end
      object miCreateTestTextureset: TMenuItem
        Caption = 'Create Test Textureset'
        OnClick = miCreateTestTexturesetClick
      end
      object miImageBruteForce: TMenuItem
        Caption = 'Brute Force Image Size'
        OnClick = miImageBruteForceClick
      end
      object miGraphicComparison: TMenuItem
        Caption = 'Graphic Comparison'
        OnClick = miGraphicComparisonClick
      end
      object miPaletteImage: TMenuItem
        Caption = 'Palette Image'
        OnClick = miPaletteImageClick
      end
    end
  end
end
