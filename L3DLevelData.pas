unit L3DLevelData;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

// This unit has the Records used to load / save a Lemmings 3D level.
// Check the unit L3DLevel for how a level is handled in-memory.

interface

type
  TL3DLevelFileSkillsetEntry = packed record
    SkillID: Byte;
    SkillCount: Byte;
  end;

  TL3DLevelFileLandVertexEntry = packed record
    LandVertexZ: Byte;
    LandVertexX: Byte;
    FloorGraphic: Byte; // Also maybe contains other stuff
  end;

  TL3DLevelFileLandPolygon = packed array[0..7] of TL3DLevelFileLandVertexEntry;

  TL3DLevelFileCameraPosition = packed record
    _Always0xC0_0000: Byte;
    Y: ShortInt;
    _Always0x80_0002: Byte;
    Z: ShortInt;
    _Always0x80_0004: Byte;
    X: ShortInt;
    Rotation: Byte;
    _Always0x00_0007: Byte;
  end;

  // No record for Object or Block, as these have to be read bitwise.

  TL3DLevelFile = packed record
    TimeSeconds: Byte;                                             // 0x000000
    TimeMinutes: Byte;                                             // 0x000001
    _UnknownCamera0002: TL3DLevelFileCameraPosition;               // 0x000002 - 0x000009
    Skillset: array[0..8] of TL3DLevelFileSkillsetEntry;           // 0x00000A - 0x00001B
    _Always0x00_001C: array[0..5] of Byte;                         // 0x00001C - 0x000021
    LandVertexSets: array[0..7] of TL3DLevelFileLandPolygon;       // 0x000022 - 0x0000E1
    ReleaseRate: SmallInt;                                         // 0x0000E2 - 0x0000E3
    LemmingCount: SmallInt;                                        // 0x0000E4 - 0x0000E5
    SaveRequirement: SmallInt;                                     // 0x0000E6 - 0x0000E7
    Textures: Byte;                                                // 0x0000E8
    Land: Byte;                                                    // 0x0000E9
    _Unknown00EA: array[0..3] of Byte;                             // 0x0000EA - 0x0000ED
    _Always0x00_00EE: Byte;                                        // 0x0000EE
    _Unknown00EF: Byte;                                            // 0x0000EF
    LevelName: array[0..31] of AnsiChar;                           // 0x0000F0
    Objects: Byte;                                                 // 0x000110
    Signs: Byte;                                                   // 0x000111
    Sea: Byte;                                                     // 0x000112
    AnimatedObject: Byte;                                          // 0x000113
    InteractiveObject: Byte;                                       // 0x000114
    Sky: Byte;                                                     // 0x000115
    _Unknown0116: Byte;                                            // 0x000116
    Walls: Byte;                                                   // 0x000117
    WaterSpeedZ: ShortInt;                                         // 0x000118
    WaterSpeedX: ShortInt;                                         // 0x000119
    _Unknown011A: Byte;                                            // 0x00011A
    RenderFlags: Word;                                             // 0x00011B - 0x00011C
    Comment: array[0..31] of AnsiChar;                             // 0x00011D - 0x00013C
    _Unknown013D: array[0..1] of Byte;                             // 0x00013D - 0x00013D
    FaceRenderLimit: Word;                                         // 0x00013F - 0x000140
    _Unknown0141: array[0..1] of Byte;                             // 0x000141 - 0x000142
    Style: Byte;                                                   // 0x000143
    _Unknown0144: array[0..2] of Byte;                             // 0x000144 - 0x000146
    _Always0x00_0147: Byte;                                        // 0x000147
    _Unknown0148: array[0..7] of Byte;                             // 0x000148 - 0x00014F
    _Always0x00_0150: Byte;                                        // 0x000150
    Music: Byte;                                                   // 0x000151
    _Always0x00_0152: Byte;                                        // 0x000152
    BorderKill: array[0..3] of Byte;                               // 0x000153 - 0x000156
    SlipperyFlag: Byte;                                            // 0x000157
    _Always0x00_0158: Byte;                                        // 0x000158
    PreviewCameraPivotY: Byte;                                     // 0x000159
    PreviewCameraPivotZ: Byte;                                     // 0x00015A
    PreviewCameraPivotX: Byte;                                     // 0x00015B
    CeilingKill: Byte;                                             // 0x00015C
    CameraPositions: array[0..3] of TL3DLevelFileCameraPosition;   // 0x00015D - 0x00017C
    _Unknown017D: array[0..9] of Byte;                             // 0x00017D - 0x000186
    _Always0x00_0187: array[0..6] of Byte;                         // 0x000187 - 0x00018D
    _Unknown018E: Byte;                                            // 0x00018E
    _Always0x00_018F: Byte;                                        // 0x00018F
    _Unknown0190: Byte;                                            // 0x000190
    _Always0x00_0191: array[0..110] of Byte;                       // 0x000191 - 0x0001FF
    BlockData: array[0..16383] of Word;                            // 0x000200 - 0x0081FF
    ObjectData: array[0..16383] of Word;                           // 0x008200 - 0x0101FF
  end;

  // It's been speculated that there may be a single unknown byte at 0x00015C, with
  // camera positions starting at 0x00015D and their unknown byte being the final
  // one (and thus, Unknown10 being one byte shorter).

  TL3DBlockFileFace = packed record
    Index: Byte;
    Modifier: Byte; // 0x80 = allow transparency, 0x40 = special anim*, 0x20 = four-panel anim starting from Index
    Shade: Byte;
  end;

  TL3DBlockFileEntry = packed record
    Unknown1: array[0..1] of Byte;
    Flags: Byte;
    Unknown2: Byte;
    Faces: array[0..5] of TL3DBlockFileFace;
  end;

  TL3DBlockFile = packed array[0..63] of TL3DBlockFileEntry;

implementation

end.
