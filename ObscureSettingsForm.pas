unit ObscureSettingsForm;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  L3DLevel,
  Generics.Collections,
  Windows, Messages, SysUtils, StrUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls;

type
  TEditList = TObjectList<TEdit>;

  TFEditUnknowns = class(TForm)
    lblFaceRenderLimit: TLabel;
    ebFaceRenderLimit: TEdit;
    gbUncommonFlags: TGroupBox;
    cbSolidSea: TCheckBox;
    cbSolidSky: TCheckBox;
    cbInvisibleLand: TCheckBox;
    cbFun8Effect: TCheckBox;
    cbShrinkLand: TCheckBox;
    gbUnknownFlags: TGroupBox;
    cbUnknown011Cx20: TCheckBox;
    cbUnknown011Cx40: TCheckBox;
    cbUnknown0157x04: TCheckBox;
    cbUnknown0157x08: TCheckBox;
    cbUnknown0157x10: TCheckBox;
    cbUnknown0157x20: TCheckBox;
    cbUnknown0157x40: TCheckBox;
    cbUnknown0157x80: TCheckBox;
    btnCancel: TButton;
    btnOK: TButton;
    procedure ebFaceRenderLimitExit(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    fLevel: TL3DLevel;
    fUnknownEdits: TEditList;

    procedure CreateUnknownEditboxes;
    procedure VerifyHexKey(Sender: TObject; var Key: char);
  public
    procedure SetLevel(aLevel: TL3DLevel);
  end;

implementation

{$R *.dfm}

const
  FIRST_UNKNOWNS_X = 184;
  FIRST_UNKNOWNS_Y = 16;
  UNKNOWN_LABEL_SPACING_X = 86;
  UNKNOWN_SPACING_X = 248;
  UNKNOWN_SPACING_Y = 23;

procedure TFEditUnknowns.btnOKClick(Sender: TObject);
  procedure ApplyChecks(aGroupBox: TGroupBox);
  var
    i: Integer;
  begin
    for i := 0 to aGroupBox.ControlCount-1 do
      if aGroupBox.Controls[i] is TCheckbox then
      begin
        if TCheckbox(aGroupBox.Controls[i]).Checked then
          Include(fLevel.RenderFlags, TL3DLevelRenderFlag(aGroupBox.Controls[i].Tag))
        else
          Exclude(fLevel.RenderFlags, TL3DLevelRenderFlag(aGroupBox.Controls[i].Tag));
      end;
  end;

  procedure ApplyUnknowns;
  var
    i, n: Integer;
    ThisEdit: TEdit;
    BytePtr: ^Byte;
  begin
    for i := 0 to fUnknownEdits.Count-1 do
    begin
      ThisEdit := fUnknownEdits[i];
      BytePtr := Pointer(ThisEdit.Tag);
      for n := 1 to (ThisEdit.MaxLength div 2) do
      begin
        while Length(ThisEdit.Text) < ThisEdit.MaxLength do
          ThisEdit.Text := ThisEdit.Text + '0';
        BytePtr^ := StrToIntDef('x' + MidStr(ThisEdit.Text, (n * 2) - 1, 2), 0);
        Inc(BytePtr);
      end;
    end;
  end;
begin
  fLevel.FaceRenderLimit := StrToIntDef(ebFaceRenderLimit.Text, 1081);
  ApplyChecks(gbUncommonFlags);
  ApplyChecks(gbUnknownFlags);
  ApplyUnknowns;

  ModalResult := mrOK;
end;

procedure TFEditUnknowns.CreateUnknownEditboxes;
var
  BoxIndex: Integer;

  procedure CreateBox(DataOffset: Integer; DataByteLength: Integer; DataPointer: Pointer);
  var
    NewLabel: TLabel;
    NewEdit: TEdit;

    NewText: String;
    BytePtr: ^Byte;

    i: Integer;
  begin
    NewLabel := TLabel.Create(self);
    NewEdit := TEdit.Create(self);

    NewLabel.Caption := '0x' + IntToHex(DataOffset, 4);
    if DataByteLength > 1 then
      NewLabel.Caption := NewLabel.Caption + ' - 0x' + IntToHex(DataOffset + DataByteLength - 1, 4);

    NewEdit.OnKeyPress := VerifyHexKey;
    NewEdit.MaxLength := DataByteLength * 2;

    NewText := '';
    BytePtr := DataPointer;

    for i := 0 to DataByteLength-1 do
    begin
      NewText := NewText + IntToHex(BytePtr^, 2);
      Inc(BytePtr);
    end;

    NewEdit.Text := NewText;

    NewLabel.Parent := self;
    NewEdit.Parent := self;
    fUnknownEdits.Add(NewEdit);

    NewLabel.Left := FIRST_UNKNOWNS_X + ((BoxIndex mod 2) * UNKNOWN_SPACING_X);
    NewLabel.Top := FIRST_UNKNOWNS_Y + ((BoxIndex div 2) * UNKNOWN_SPACING_Y);

    NewEdit.Left := NewLabel.Left + UNKNOWN_LABEL_SPACING_X;
    NewEdit.Top := NewLabel.Top - 2;

    NewEdit.Tag := Integer(DataPointer);

    Inc(BoxIndex);
  end;
begin
  BoxIndex := 0;

  CreateBox($0002, 8, @fLevel.HeaderUnknowns._UnknownCamera0002);
  CreateBox($001C, 6, @fLevel.HeaderUnknowns._Always0x00_001C);
  CreateBox($00EA, 4, @fLevel.HeaderUnknowns._Unknown00EA);
  CreateBox($00EE, 2, @fLevel.HeaderUnknowns._Always0x00_00EE); // spills over into the unknown at 0x00EF, to save space
  CreateBox($0116, 1, @fLevel.HeaderUnknowns._Unknown0116);
  CreateBox($011A, 1, @fLevel.HeaderUnknowns._Unknown011A);
  CreateBox($013D, 2, @fLevel.HeaderUnknowns._Unknown013D);
  CreateBox($0141, 2, @fLevel.HeaderUnknowns._Unknown0141);
  CreateBox($0144, 3, @fLevel.HeaderUnknowns._Unknown0144);
  CreateBox($0147, 10, @fLevel.HeaderUnknowns._Always0x00_0147);
  CreateBox($0152, 1, @fLevel.HeaderUnknowns._Always0x00_0152);
  CreateBox($0158, 1, @fLevel.HeaderUnknowns._Always0x00_0158);
  CreateBox($017D, 3, @fLevel.HeaderUnknowns._Unknown017D);
  CreateBox($0180, 32, @fLevel.HeaderUnknowns._Unknown017D[3]); // now we're getting really cheeky, but it looks nicer
  CreateBox($01A0, 32, @fLevel.HeaderUnknowns._Always0x00_0191[15]);
  CreateBox($01C0, 32, @fLevel.HeaderUnknowns._Always0x00_0191[47]);
  CreateBox($01E0, 32, @fLevel.HeaderUnknowns._Always0x00_0191[79]);
end;

procedure TFEditUnknowns.ebFaceRenderLimitExit(Sender: TObject);
var
  FaceCount: Integer;
begin
  FaceCount := StrToIntDef(ebFaceRenderLimit.Text, $0439);
  if (FaceCount < 15) then
    ebFaceRenderLimit.Text := '15'
  else if (FaceCount > 1100) then
    ebFaceRenderLimit.Text := '1100';   
end;

procedure TFEditUnknowns.FormCreate(Sender: TObject);
begin
  fUnknownEdits := TEditList.Create(false);
end;

procedure TFEditUnknowns.FormDestroy(Sender: TObject);
begin
  fUnknownEdits.Free;
end;

procedure TFEditUnknowns.SetLevel(aLevel: TL3DLevel);

  procedure SetChecks(aGroupBox: TGroupBox);
  var
    i: Integer;
  begin
    for i := 0 to aGroupBox.ControlCount-1 do
      if aGroupBox.Controls[i] is TCheckbox then
        TCheckbox(aGroupBox.Controls[i]).Checked := TL3DLevelRenderFlag(aGroupBox.Controls[i].Tag) in aLevel.RenderFlags;
  end;
begin
  fLevel := aLevel;

  ebFaceRenderLimit.Text := IntToStr(aLevel.FaceRenderLimit);

  SetChecks(gbUncommonFlags);
  SetChecks(gbUnknownFlags);

  CreateUnknownEditboxes; // also sets them
end;

procedure TFEditUnknowns.VerifyHexKey(Sender: TObject; var Key: char);
begin
  if CharInSet(Key, ['a'..'f']) then
    Key := Uppercase(Key)[1]
  else if not (CharInSet(Key, ['A'..'F', '0'..'9']) or (Key < #20)) then
    Key := #0;
end;

end.
